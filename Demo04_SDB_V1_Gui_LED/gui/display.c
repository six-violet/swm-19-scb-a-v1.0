/* www.6violet.com
 * FileName display.c
 * creat time: 2023-8-27 by dawoo.Zheng
 *    The last modifier: dawoo.Zheng
 * The last change time: 2023-12-11
 * commit:
 */

#include "display.h"

extern uint8_t OledGRAM[SV_SCREEN_H/8][SV_SCREEN_W];

#if (USE_SV_SIN_COS_TABLE_ == 1)
#include "cSinCosVal.h"
#endif  //if USE_SV_SIN_COS_TABLE_

////如果使用汉字与ASCII的查表功能， USE_SV_FONTLIB_TABLE_ == 1
////在显示汉字与ASCII字符时要使用。
#define USE_SV_FONTLIB_TABLE_ 1
#if (USE_SV_FONTLIB_TABLE_ == 1)
#include "fontLib.h"            //
#endif //USE_SV_FONTLIB_TABLE_ 

#define _MAX_RANK_ 6
SV_COORD centreX=(SV_SCREEN_W/2);
SV_COORD centreY=(SV_SCREEN_H/2);

unsigned char dispRam[SV_FONT_RAM_BUF_];
uint16_t indexRank[_MAX_RANK_][2];
uint16_t indexNum;

screenSys sv_sInfo;

uint8_t autoBeginEndArea;
void setreOutAreaVal(void)
{
    autoBeginEndArea = 1;
}

void clrreOutAreaVal(void)
{
    autoBeginEndArea = 0;
}

SV_COLOR reRGB(SV_COLOR color)
{
    SV_COLOR temp;
    temp = color>>8;
    return (SV_COLOR)((color<<8)+temp);
}

//------------------------------------o
//  设置默认背景色   |     COLOR      |
void setDefBKColorVal(SV_COLOR bkcolor)
{
    sv_sInfo.bkColorVal = bkcolor;
}

//------------------------------------o
//  设置默认前景色   |     COLOR      |
void setDefFGColorVal(SV_COLOR fgcolor)
{
    sv_sInfo.fgColorVal = fgcolor;
}

uint8_t reValInArea(uint16_t a,uint16_t b,uint16_t c)
{
    uint8_t reA;
    if(autoBeginEndArea == 1)
    {
        reA = 0;
    }
    else
    {
        reA = 1;
    }
    if(c < a || c > b)
        return reA;
    else
        return autoBeginEndArea;
}

void plotRegion(uint16_t a,uint16_t b)
{
    const uint16_t cRAngle[]= {89,179,269,359};
    if(a < cRAngle[0])
    {
        if( b <= cRAngle[0])
        {
            indexRank[indexNum][0] = a;
            indexRank[indexNum][1] = b;
            indexNum ++;
        }
        else if(b <= cRAngle[1])
        {
            indexRank[indexNum][0] = a;
            indexRank[indexNum][1] = cRAngle[0];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[0]+1;
            indexRank[indexNum][1] = b;
            indexNum ++;
        }
        else if(b <= cRAngle[2])
        {
            indexRank[indexNum][0] = a;
            indexRank[indexNum][1] = cRAngle[0];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[0]+1;
            indexRank[indexNum][1] = cRAngle[1];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[1]+1;
            indexRank[indexNum][1] = b;
            indexNum ++;
        }
        else
        {
            indexRank[indexNum][0] = a;
            indexRank[indexNum][1] = cRAngle[0];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[0]+1;
            indexRank[indexNum][1] = cRAngle[1];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[1]+1;
            indexRank[indexNum][1] = cRAngle[2];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[2]+1;
            indexRank[indexNum][1] = b;
            indexNum ++;
        }
    }
    else if(a<cRAngle[1])
    {
        if(b <= cRAngle[1])
        {
            indexRank[indexNum][0] = a;
            indexRank[indexNum][1] = b;
            indexNum ++;
        }
        else if(b <= cRAngle[2])
        {
            indexRank[indexNum][0] = a;
            indexRank[indexNum][1] = cRAngle[1];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[1]+1;
            indexRank[indexNum][1] = b;
            indexNum ++;
        }
        else
        {
            indexRank[indexNum][0] = a;
            indexRank[indexNum][1] = cRAngle[1];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[1]+1;
            indexRank[indexNum][1] = cRAngle[2];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[2]+1;
            indexRank[indexNum][1] = b;
            indexNum ++;
        }
    }
    else if(a<cRAngle[2])
    {
        if(b <= cRAngle[2])
        {
            indexRank[indexNum][0] = a;
            indexRank[indexNum][1] = b;
            indexNum ++;
        }
        else
        {
            indexRank[indexNum][0] = a;
            indexRank[indexNum][1] = cRAngle[2];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[2]+1;
            indexRank[indexNum][1] = b;
            indexNum ++;
        }
    }
    else if(a<cRAngle[3])
    {
        indexRank[indexNum][0] = a;
        indexRank[indexNum][1] = b;
        indexNum ++;
    }
}

uint8_t computeA2BRegion(uint16_t a, uint16_t b)
{
    uint16_t i;
    if( a == b)
        return 0;
    for(i=0; i<_MAX_RANK_; i++)
    {
        indexRank[i][0] = 0;
        indexRank[i][1] = 0;
    }
    indexNum = 0;
    if(a > b)
    {
        //begin b>a
        plotRegion(0,b);
        plotRegion(a,359);
    }
    else
    {
        //begin a>b
        plotRegion(a,b);
    }
    return 1;
}

uint8_t compute4Quadrant(int16_t x, int16_t y, int16_t indexRA0, int16_t indexRA1, int16_t indexRB0, int16_t indexRB1)
{
    if((indexRA0 > 0) && (indexRA1 > 0) && x > 0 && y > 0 && (indexRB0 > 0) && (indexRB1 > 0))
    {
        return 1;
    }
    if((indexRA0 < 0) && (indexRA1 > 0) && x < 0 && y > 0 && (indexRB0 < 0) && (indexRB1 > 0))
    {
        return 2;
    }
    if((indexRA0 < 0) && (indexRA1 < 0) && x < 0 && y < 0 && (indexRB0 < 0) && (indexRB1 < 0))
    {
        return 3;
    }
    if((indexRA0 > 0) && (indexRA1 < 0) && x > 0 && y < 0 && (indexRB0 >= 0) && (indexRB1 < 0))
    {
        return 4;
    }
    return 0;
}

uint8_t computeABC(int16_t x, int16_t y, int16_t indexRA0, int16_t indexRA1, int16_t indexRB0, int16_t indexRB1)
{
    uint8_t quadrant;
    int16_t tX,tY;
    double dA,dB,dC;
    tX = x - centreX;
    tY = centreY - y;
    if( tX == 0)
        tX = 1;
    quadrant = compute4Quadrant(tX, tY, indexRA0, indexRA1, indexRB0, indexRB1);
    if(quadrant == 0)
        return 0;
    dA = ((double)tY/tX);
    dB = ((double)indexRA0/indexRA1);
    dC = ((double)indexRB0/indexRB1);
    if(dA > dB && dA < dC)
        return 1;
    if(dA < dB && dA > dC)
        return 1;
    else
        return 0;
}

void displayClean(void)
{
}

void displayReverse(void)
{
}

void dispRamCopy(uint8_t AddOffsetX,uint8_t AddOffsetY,unsigned char * data, uint16_t sizeByte)
{
    uint16_t i;
    for(i=0; i<sizeByte; i++)
    {
        OledGRAM[AddOffsetY][AddOffsetX+i] = *data++;
    }
}

void dispRam2S(SV_COORD x, SV_COORD y,SV_COORD xEnd,SV_COORD yEnd, uint16_t sizeByte)
{
//    screenSetViewArea(x,y,xEnd,yEnd);
//    screenWrite_NByteData(dispRam,sizeByte*SV_COLOR_BYTE_NUM);
}

void dispRam2SData(SV_COORD x, SV_COORD y, SV_COORD xEnd, SV_COORD yEnd, unsigned char * dataRam, uint16_t sizeByte)
{
//    screenSetViewArea(x,y,xEnd,yEnd);
//    screenWrite_NByteData(dataRam,sizeByte*SV_COLOR_BYTE_NUM);
}

uint16_t findHzIndex(uint16_t hzCode)
{
    uint16_t i,a,b,re = 0;
    uint16_t fdHzCode;
//    uint8_t * tmpData;
    a=0;
    b=_HZ_NUM_;
    i=b/2;
    while(a != b)
    {
        if((a+1) == b)
        {
            if(i == a)
            {
                i=b;
                a=b;
            }
            else
            {
                i=a;
                b=a;
            }
        }
        fdHzCode = (uint16_t)((hzIndex[i][0])<<8) + (hzIndex[i][1]);
        if(fdHzCode == hzCode)
        {
            re = i;
            break;
        }
        else
        {
            if(fdHzCode > hzCode)
            {
                b = i;
            }
            else
            {
                a = i;
            }
            i = (a+b)/2;
        }
    }
    return re;
}

//------------------------------------------------------o
//      画点     |   X坐    |    Y坐    |    COLOR      |
//      函数     |   标点   |    标点   |    颜色       |
uint8_t drawPoint(SV_COORD x, SV_COORD y, SV_COLOR color)
{
    screenCSClr();
    screenDrvPixel( x,y,color);
    screenCSSet();
    return 0;
}

//---------------------------------------------------------------------------------o
//      画线    |    X1坐   |    Y1坐    |    X2坐    |    Y2坐    |    COLOR      |
//      函数    |    标点   |    标点    |    标点    |    标点    |    颜色       |
uint8_t drawLine(SV_COORD x1, SV_COORD y1, SV_COORD x2, SV_COORD y2, SV_COLOR color)
{
    uint16_t t;
    int16_t xerr=0,yerr=0,delta_x,delta_y,distance;
    int16_t incx,incy,uRow,uCol;
    screenCSClr();
    delta_x=x2-x1;                      //计算坐标增量
    delta_y=y2-y1;
    uRow=x1;                            //画线起点坐标
    uCol=y1;
    if(delta_x>0)
        incx=1;                         //设置单步方向
    else if (delta_x==0)
        incx=0;                         //垂直线
    else
    {
        incx=-1;
        delta_x=-delta_x;
    }
    if(delta_y>0)
        incy=1;
    else if(delta_y==0)
        incy=0;                         //水平线
    else
    {
        incy=-1;
        delta_y=-delta_y;
    }
    if(delta_x>delta_y)
        distance=delta_x;               //选取基本增量坐标轴
    else
        distance=delta_y;
    for(t=0; t < (uint16_t)(distance+1); t++)
    {
        screenDrvPixel(uRow,uCol,color);    //画点
        xerr+=delta_x;
        yerr+=delta_y;
        if(xerr>distance)
        {
            xerr-=distance;
            uRow+=incx;
        }
        if(yerr>distance)
        {
            yerr-=distance;
            uCol+=incy;
        }
    }
    screenCSSet();
    return 0;
}

//-----------------------------------------------------------------------------------o
//       画方形      |   X1坐    |    Y1坐   |   X2坐    |    Y2坐   |     COLOR     |
//       线宽1       |   标点    |    标点   |   标点    |    标点   |     颜色      |
uint8_t drawRectangle(SV_COORD x1,SV_COORD y1,SV_COORD x2,SV_COORD y2, SV_COLOR color)
{
    drawLine(x1,y1,x2,y1,color);
    drawLine(x1,y1,x1,y2,color);
    drawLine(x2,y1,x2,y2,color);
    drawLine(x1,y2,x2,y2,color);
    return 0;
}

//----------------------------------------------------------------------------------o
//      画方形       |   X1坐   |    Y1坐   |   X方向   |   Y方向   |     COLOR     |
//      全填充       |   标点   |    标点   |   宽度    |   宽度    |     颜色      |
uint8_t fillRectangle(SV_COORD x, SV_COORD y, uint16_t w, uint16_t h, SV_COLOR color)
{
    uint16_t x0_,y0_,i,j;
    if(x<0)
        x0_=0;
    else
        x0_=x;
    if(y<0)
        y0_=0;
    else
        y0_=y;
    for(i=0; i<h; i++)
    {
        for(j=0; j<w; j++)
        {
            if(color != 0)
                OledGRAM[(y0_+i)/8][x0_ +j] |= (0x01<<((y0_+i)%8));
            else
                OledGRAM[(y0_+i)/8][x0_ +j] &= (~(0x01<<((y0_+i)%8)));
        }
    }
    return 0;
}

//--------------------------------------------------------------------------------------------------o
//       画方形          |   X1坐   |    Y1坐   |   X方向   |   Y方向   |    X==Y   |    COLOR      |
//       线宽z点         |   标点   |    标点   |   宽度    |   宽度    |    宽度Z  |    颜色       |
uint8_t drawRectangleEdge(SV_COORD x, SV_COORD y, uint16_t w, uint16_t h, uint16_t z, SV_COLOR color)
{
    if((z >(w/2)) || (z>(h/2)))
        return 1;
    fillRectangle(x, y, z, h, color);
    fillRectangle(x+z, y, w-z-z, z, color);
    fillRectangle(x+z, y+h-z, w-z-z, z, color);
    fillRectangle(x+w-z, y, z, h, color);
    return 0;
}


int reOutArea(uint16_t a,uint16_t b,uint16_t c)
{
    if(c < a || c > b)
        return 1;
    else
        return 0;
}

int16_t findInCircle(uint16_t ox, uint16_t oy, uint16_t radiusP, uint8_t w, uint16_t findx, uint16_t findy)
{
    uint16_t i,re,maxZ,minZ;
    uint16_t aa,bb;
    minZ = radiusP-w;
    maxZ = radiusP;
    re = SV_SCREEN_W-1;
    for(i= findx; i< SV_SCREEN_W; i++)
    {
        aa = (ox > i) ? (ox-i) : (i-ox);
        bb = (oy > findy) ? (oy-findy) : (findy-oy);
        if(reValInArea(minZ*minZ,maxZ*maxZ,aa*aa+bb*bb))
        {
            re = i;
            break;
        }
    }
    return re;
}

//--------------------------------------------------------------------------------------o
//     画环形     |  圆心坐   |   圆心坐   |     半径长      |  圆环的  |     COLOR     |
//     线宽w点    |  标点X    |   标点Y    |     r像素       |   宽度   |     颜色      |
uint8_t drawCircle(uint16_t ox, uint16_t oy, uint16_t radiusP, uint8_t w, SV_COLOR color)
{
    uint16_t ix,iy;
    int16_t flagA,flagB;
    if(w<1 || w>99 || radiusP<w)
        return 1;
    for(iy = 0; iy < SV_SCREEN_H; iy++)
    {
        dispRam[iy*2] = (unsigned char )(color>>8);
        dispRam[iy*2+1] = (unsigned char )(color&0x00ff);
    }
    for(iy = 0; iy < SV_SCREEN_H; iy++)
    {
        flagA = 0;
        flagB = 0;
        for(ix = 0; ix < SV_SCREEN_W; ix++)
        {
            setreOutAreaVal();
            flagA = findInCircle(ox, oy, radiusP, w, ix, iy);
//            flagA = findInCircleBegin(ox, oy, radiusP, w, ix, iy);
            if(flagA == (SV_SCREEN_W-1))
                continue;
            ix =flagA;
            clrreOutAreaVal();
            flagB = findInCircle(ox, oy, radiusP, w, ix, iy);
//            flagB = findInCircleEnd(ox, oy, radiusP, w, ix, iy);
            ix =flagB;
            dispRam2S(flagA,iy,flagB,iy,flagB-flagA+1);
        }
    }
    return 0;
}

#if (USE_SV_SIN_COS_TABLE_ == 1)
uint16_t analyseInRegion( uint16_t a, uint16_t b, uint16_t radiusP, uint8_t w, uint16_t ax, uint16_t ay, SV_COLOR color)
{
    uint16_t i,j,tmp,maxZ,minZ;
    uint16_t re,aa,bb;
    SV_COORD xBeg,xEnd;
    int16_t tBx,tBy,tEx,tEy;
    minZ = radiusP-w;
    maxZ = radiusP;
    re = SV_SCREEN_W;
    for(i = 0; i < SV_SCREEN_H*2; i++)
    {
        dispRam[i*2] = (unsigned char )(color>>8);
        dispRam[i*2+1] = (unsigned char )(color&0x00ff);
    }
    setreOutAreaVal();
    for(i= ax; i< SV_SCREEN_W; i++)
    {
        aa = (centreX > i) ? (centreX-i) : (i-centreX);
        bb = (centreY > ay) ? (centreY-ay) : (ay-centreY);
        if(reValInArea(minZ*minZ,maxZ*maxZ,aa*aa+bb*bb))
        {
            re = i;
            break;
        }
    }
    if( re == SV_SCREEN_W)
        return re;
    else
    {
        clrreOutAreaVal();
        xBeg = re;
        re = SV_SCREEN_W;
        for(i= xBeg; i< SV_SCREEN_W; i++)
        {
            aa = (centreX > i) ? (centreX-i) : (i-centreX);
            bb = (centreY > ay) ? (centreY-ay) : (ay-centreY);
            if(reValInArea(minZ*minZ,maxZ*maxZ,aa*aa+bb*bb))
            {
                re = i;
                break;
            }
        }
        xEnd = re;

    }
    if(( xBeg > xEnd) || (xEnd > SV_SCREEN_W))
        return SV_SCREEN_W;
    tmp = computeA2BRegion( a, b);
    if(tmp == 0)
        return SV_SCREEN_W;
    for(re = xEnd,i = xBeg; i < (uint16_t)xEnd; i++)
    {
        for(j = 0; j< _MAX_RANK_; j++)
        {
            if((indexRank[j][0] == 0) && (indexRank[j][1] == 0))
                break;
            tBx = cSinCosList[indexRank[j][0]][0];
            tBy = cSinCosList[indexRank[j][0]][1];
            tEx = cSinCosList[indexRank[j][1]][0];
            tEy = cSinCosList[indexRank[j][1]][1];
            tmp = computeABC( i, ay, tBx, tBy, tEx, tEy);
            if((ay == 60) && (i == 125))
                aa = 0;
            if(tmp == 0)
                continue;
            else
            {
                re = i;
                i = xEnd;
                break;
            }
        }
    }
    xBeg = re;
    if(re >= xEnd)
        return xEnd;
    else
    {
        for(re = xEnd,i= xBeg; i< (uint16_t)xEnd; i++)
        {
            for(j = 0; j< _MAX_RANK_; j++)
            {
                if((indexRank[j][0] == 0) && (indexRank[j][1] == 0))
                    break;
                tBx = cSinCosList[indexRank[j][0]][0];
                tBy = cSinCosList[indexRank[j][0]][1];
                tEx = cSinCosList[indexRank[j][1]][0];
                tEy = cSinCosList[indexRank[j][1]][1];
                tmp = computeABC( i, ay, tBx, tBy, tEx, tEy);
                if((ay == 37) && (i > 125))
                    aa = 0;
                if(tmp == 1)
                    continue;
                else
                {
                    re = i;
                    i = xEnd;
                    break;
                }
            }
        }
    }
    if(re >= SV_SCREEN_W)
        return SV_SCREEN_W;
    else
    {
        xEnd = re;
        dispRam2S(xBeg,ay,xEnd,ay,xEnd-xBeg+1);
    }
    return re;
}

//------------扇形的圆心是常量，const uint16_t centreX,centreY;-----------------------------o
//       画扇形         |  起始角  |   终止角  |     半径长      |  扇形的  |     COLOR     |
//       线宽w点        |  [1,360] |  [1,360]  |     r像素       |   宽度   |     颜色      |
uint8_t drawCircleSector(uint16_t a, uint16_t b, uint16_t radiusP, uint8_t w, SV_COLOR color)
{
    uint16_t ix,iy;
    int16_t flagC;
    if(w<1 || w>99 || radiusP<w)
        return 1;
    for(iy = 0; iy < SV_SCREEN_H; iy++)
    {
        for(ix = 0; ix < SV_SCREEN_W; ix++)
        {
            if(iy == 61)
                flagC = 0;
            flagC = analyseInRegion( a, b, radiusP, w, ix, iy, color);
            if(flagC >= SV_SCREEN_W )
            {
                break;
            }
            else
            {
                ix = flagC;
            }
            flagC = analyseInRegion( a, b, radiusP, w, ix, iy, color);
            if(flagC < SV_SCREEN_W)
                continue;
            ix = flagC;
        }
    }
    return 0;
}
#endif

//-----------------------------------------------------------------------------------o
//     画单      |  左上坐  |   左上坐  |  图像    |   图像    |      图标对应       |
//     色图      |  标点X   |   标点Y   |  宽度    |   高度    |      数据指针       |
uint8_t drawImage(SV_COORD x, SV_COORD y,uint16_t w, uint16_t h, unsigned char * data)
{
//    uint8_t x0_,y0_;
//    for(y0_=y; y0_<y+(h/8); y0_++)
//    {
//        for(x0_=x; x0_<x+(h/8); x0_++)
//        {
//            dispRamCopy(x0_, y0_, data+(y0_*w), w);
//        }
//    }
    OledShowPicture(x,y,w,h,data);
    return 0;
}

//------------------------------------------------------------------------------------------------------------------o
//     使用图标     |   X1坐    |    Y1坐    |    X2坐    |    Y2坐    |  图像    |   图像    |      图标对应       |
//     填充区域     |   标点    |    标点    |    标点    |    标点    |  宽度    |   高度    |      数据指针       |
uint8_t drawPadImage(SV_COORD x1, SV_COORD y1, SV_COORD x2, SV_COORD y2,uint16_t w, uint16_t h, unsigned char * data)
{
    uint16_t i,j;
//    uint16_t col,row,line;
    screenCSClr();
    for(j= 0; j<y2; j++)
    {
        screenSetBeginCoord(x1,y1+j);
        for(i=0; i<x2; i++)
        {
            screenWriteByteData(*(data+(j%h)*w+(i%w)));
        }
    }
    /*
    screenSetViewArea(x1,y1,x2-1,y2-1);
    col = (x2-x1)/w;
    row = (x2-x1)%w;
    line = (y2-y1);
    for(i=0; i<line; i++)
    {
        for(j=0; j<col; j++)
        {
            screenWrite_NByteData((unsigned char *)(data+(w*(i%h)*2)), w*SV_COLOR_BYTE_NUM);
        }
        if(row > 0)
            screenWrite_NByteData((unsigned char *)(data+(w*(i%h)*2)), row*SV_COLOR_BYTE_NUM);
    }
    */
    screenCSSet();
    return 0;
}

//--------------------------------------------------------------------------------------------o
//      显示单       |      字体       |   左上坐  |   左上坐  |    文字编   |     显示的     |
//      个半字       |      高度       |   标点X   |   标点Y   |    码数据   |     模式       |
uint8_t drawAsciiChar(uint16_t fontSize, SV_COORD x, SV_COORD y, uint8_t data, uint8_t dispMod)
{
    uint8_t i,m,temp,size2,chr1;
    uint8_t y0=y;
    size2=(fontSize/8+((fontSize%8)?1:0))*(fontSize/2);  //得到字体一个字符对应点阵集所占的字节数
    chr1 = data - ' ';                  //计算偏移后的值
    for(i=0; i<size2; i++)
    {
        if(fontSize==12)
        {
            temp=asc2_1206[chr1][i];   //调用1206字体
        }
        else if(fontSize==16)
        {
            temp=asc2_1608[chr1][i];   //调用1608字体
        }
        else if(fontSize==24)
        {
            temp=asc2_2412[chr1][i];   //调用2412字体
        }
        else
            return 0;
        for(m=0; m<8; m++)         //写入数据
        {
            if(temp&0x80)
            {
                if(dispMod == 0)
                    OledGRAM[y/8][x] |= (1<<(y%8));
                else
                    OledGRAM[y/8][x] &= ~(1<<(y%8));
            }       //OledDrawPoint(x,y);
            else
            {
                if(dispMod == 0)
                    OledGRAM[y/8][x] &= ~(1<<(y%8));
                else
                    OledGRAM[y/8][x] |= (1<<(y%8));
            }       //OledClearPoint(x,y);
            temp<<=1;
            y++;
            if((y-y0)==fontSize)
            {
                y=y0;
                x++;
                break;
            }
        }
    }
    screenAllRefresh();
    return 0;
}

//-------------------------------------------------------------------------------------------o
//    显示En      |      字体       |   左上坐  |   左上坐  |     文字编    |     显示的     |
//    字符串      |      高度       |   标点X   |   标点Y   |     码数据    |     模式       |
uint8_t drawString(uint16_t fontSize, SV_COORD x, SV_COORD y, uint8_t * data, uint8_t dispMod)
{
//    uint8_t i,j;
    while(*data != 0x00)
    {
        drawAsciiChar(fontSize, x, y, *data, dispMod);
        data++;
        x += (fontSize/2);
    }
    return 0;
}

//-----------------------------------------------------------------------------------------o
//    显示En     |      字体       |   左上坐  |   左上坐  |    文字编    |     显示的     |
//    字符串     |      高度       |   标点X   |   标点Y   |    码数据    |     模式       |
uint8_t drawHex1B(uint16_t fontSize, SV_COORD x, SV_COORD y, uint8_t  data, uint8_t dispMod)
{
    if((data >>4)>9)
        drawAsciiChar(fontSize, x, y, (data>>4)-10+'A', dispMod);
    else
        drawAsciiChar(fontSize, x, y, (data>>4)+'0', dispMod);
    if((data&0x0F)>9)
        drawAsciiChar(fontSize, x+(fontSize/2), y, (data&0x0F)-10+'A', dispMod);
    else
        drawAsciiChar(fontSize, x+(fontSize/2), y, (data&0x0F)+'0', dispMod);
    return 0;
}

//------------------------------------------------------------------------------------------o
//    显示En     |      字体       |   左上坐  |   左上坐  |    文字编     |     显示的     |
//    字符串     |      高度       |   标点X   |   标点Y   |    码数据     |     模式       |
uint8_t drawHex2B(uint16_t fontSize, SV_COORD x, SV_COORD y, uint16_t  data, uint8_t dispMod)
{
    drawHex1B(fontSize,x,y,(uint8_t)(data>>8),dispMod);
    drawHex1B(fontSize,x+fontSize,y,(uint8_t)(data&0xff),dispMod);
    return 0;
}

//------------------------------------------------------------------------------------------o
//    显示En     |      字体       |   左上坐  |   左上坐  |    文字编     |     显示的     |
//    字符串     |      高度       |   标点X   |   标点Y   |    码数据     |     模式       |
uint8_t drawHex4B(uint16_t fontSize, SV_COORD x, SV_COORD y, uint32_t  data, uint8_t dispMod)
{
    drawHex1B(fontSize,x,y,(uint8_t)(data>>24),dispMod);
    drawHex1B(fontSize,x+fontSize,y,(uint8_t)(data>>16),dispMod);
    drawHex1B(fontSize,x+(fontSize<<1),y,(uint8_t)(data>>8),dispMod);
    drawHex1B(fontSize,x+(fontSize*3),y,(uint8_t)(data&0xff),dispMod);
    return 0;
}

//------------------------------------------------------------------------------------------------------------------o
//       显示单        |      字体       |   左上坐  |   左上坐  |     文字编     |     COLOR前    |     COLOR背    |
//       个汉字        |      高度       |   标点X   |   标点Y   |     码数据     |     景颜色     |     景颜色     |
uint8_t drawChineseChar(uint16_t fontSize, SV_COORD x, SV_COORD y, uint16_t hzCode, SV_COORD tColor, SV_COORD bColor)
{
    int8_t j;
    const unsigned char cTBit[]= {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};
    uint16_t i = 0,index;
    uint8_t tCols[2],bCols[2];
    unsigned char * libIndex;
    uint8_t libB;      //libA,
    uint32_t libStep;   //,libSize;
    tCols[0] = (uint8_t)(tColor >>8)& 0xFF;
    tCols[1] = (uint8_t)(tColor & 0xFF);
    bCols[0] = (uint8_t)(bColor >>8)& 0xFF;
    bCols[1] = (uint8_t)(bColor & 0xFF);
    index = findHzIndex(hzCode);
    switch(fontSize)
    {
        case 16:
            libIndex = (unsigned char *)&hz16Data[0][0];
            libB = 16;
            libStep = 16*16>>3;
            break;
        case 24:
            libIndex = (unsigned char *)&hz24Data[0][0];
            libB = 24;
            libStep = 24*24>>3;
            break;
        case 32:
            libIndex = (unsigned char *)&hz32Data[0][0];
            libB = 32;
            libStep = 32*32>>3;
            break;
        default :
            return 1;
    }
    screenCSClr();
    screenSetViewArea(x,y,x+libB-1,y+libB-1);
    for(i = 0; i< libStep; i++)
    {
        for(j = 0 ; j<8; j++)
        {
            if( (*(unsigned char *)(libIndex + libStep*index + i) & cTBit[j]) !=0)
            {
                dispRam[j*2] = tCols[0];
                dispRam[j*2+1] = tCols[1];
            }
            else
            {
                dispRam[j*2] = bCols[0];
                dispRam[j*2+1] = bCols[1];
            }
        }
        screenWrite_NByteData((unsigned char *)dispRam,8*SV_COLOR_BYTE_NUM);
    }
    screenCSSet();
    return 0;
}

//---------------------------------------------------------------------------------------------------------o
//     显示     |     字体       |   左上坐  |   左上坐  |   文字编      |     COLOR前    |     COLOR背    |
//     文字     |     高度       |   标点X   |   标点Y   |   码指针      |     景颜色     |     景颜色     |
uint8_t drawText(uint8_t fontSize, SV_COORD x, SV_COORD y, uint8_t * data, SV_COORD tColor, SV_COORD bColor)
{
    SV_COORD xt,yt,hzCode;
    uint8_t *tmpData;
    uint8_t libA,libB;
    xt = x;
    yt = y;
    tmpData = (uint8_t *)data;
    switch(fontSize)
    {
        case 16:
            libA = 8;
            libB = 16;
            break;
        case 24:
            libA = 12;
            libB = 24;
            break;
        case 32:
            libA = 16;
            libB = 32;
            break;
        default :
            return 1;
    }
    while((*tmpData) != 0)
    {
        if((*tmpData) < 0x80)
        {
#ifdef  __AUTO_RETURN_DISPLAY_
            if((xt + libA ) > SV_SCREEN_W)
            {
                xt = x;
                yt += libB;
                if(yt > SV_SCREEN_H)
                    yt = y;
            }
#endif
            drawAsciiChar(fontSize, xt, yt, *tmpData++, 0);
            xt += libA;
        }
        else
        {
#ifdef  __AUTO_RETURN_DISPLAY_
            if((xt + libB) > SV_SCREEN_W)
            {
                xt = x;
                yt += libB;
                if(yt > SV_SCREEN_H)
                    yt = y;
            }
#endif
            hzCode = ((*tmpData)<<8) + (*(tmpData+1));
            tmpData += 2;
            drawChineseChar(fontSize, xt, yt, hzCode, tColor, bColor);
            xt += libB;
        }
    }
    return 0;
}

//------------------------------------------------------------------------------------------------------o
//    显示有符  |     字体       |   左上坐  |   左上坐  |   8bit     |     COLOR前    |     COLOR背    |
//    号8bit数  |     高度       |   标点X   |   标点Y   |   数据     |     景颜色     |     景颜色     |
uint8_t drawInt8(uint8_t fontSize, SV_COORD x, SV_COORD y,int8_t data,SV_COORD tColor, SV_COORD bColor)
{
    uint8_t str[5]= {"    "};
    uint8_t num;
    if(data < 0)
    {
        str[0]='-';
        num = (-1)*data;
    }
    else
        num = data;
    if((num/100) >0)
    {
        str[1] = '0'+(num/100);
        str[2] = '0'+(num/10%10);
        str[3] = '0'+(num%10);
    }
    else if((num/10) > 0)
    {
        str[1] = '0'+(num/10);
        str[2] = '0'+(num%10);
    }
    else
        str[1] = '0'+num;
    drawText(fontSize, x, y, str,tColor, bColor);
    return 0;
}

//---------------------------------------------------------------------------------------------------------o
//    显示无符    |     字体       |   左上坐  |   左上坐  |   16bit     |     COLOR前    |     COLOR背    |
//    号16bit数   |     高度       |   标点X   |   标点Y   |   数据      |     景颜色     |     景颜色     |
uint8_t drawUint16(uint8_t fontSize, SV_COORD x, SV_COORD y,uint16_t data, SV_COORD tColor, SV_COORD bColor)
{
    uint8_t str[7]= {"     "};
    uint16_t num;
    num = data;
    str[5] = 0;
    if((num/10000) >0)
    {
        str[0] = '0'+(num/10000);
        str[1] = '0'+(num/1000%10);
        str[2] = '0'+(num/100%10);
        str[3] = '0'+(num/10%10);
        str[4] = '0'+(num%10);
    }
    else if((num/1000) > 0)
    {
        str[1] = '0'+(num/1000%10);
        str[2] = '0'+(num/100%10);
        str[3] = '0'+(num/10%10);
        str[4] = '0'+(num%10);
    }
    else if((num/100) > 0)
    {
        str[2] = '0'+(num/100%10);
        str[3] = '0'+(num/10%10);
        str[4] = '0'+(num%10);
    }
    else if((num/10) > 0)
    {
        str[3] = '0'+(num/10%10);
        str[4] = '0'+(num%10);
    }
    else
        str[4] = '0'+num;
    drawText(fontSize, x, y, str,tColor, bColor);
    return 0;
}

