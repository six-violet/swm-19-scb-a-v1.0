/* www.6violet.com
 * FileName displayRes.h
 * creat time: 2023-12-07 by dawoo.Zheng
 *    The last modifier: dawoo.Zheng
 * The last change time: 2023-12-07
 * commit:
 */

#ifndef _SV_DISPLAYRES_H_
#define _SV_DISPLAYRES_H_

//#include "displayConfig.h"

//  define 148 Color
#define SV_AliceBlue            0xF0F8FF
#define SV_AntiqueWhite         0xFAEBD7
#define SV_Aqua                 0x00FFFF
#define SV_Aquamarine           0x7FFFD4
#define SV_Azure                0xF0FFFF
#define SV_Beige                0xF5F5DC
#define SV_Bisque               0xFFE4C4
#define SV_Black                0x000000
#define SV_BlanchedAlmond       0xFFEBCD
#define SV_Blue                 0x0000FF
#define SV_BlueViolet           0x8A2BE2
#define SV_Brown                0xA52A2A
#define SV_BurlyWood            0xDEB887
#define SV_CadetBlue            0x5F9EA0
#define SV_Chartreuse           0x7FFF00
#define SV_Chocolate            0xD2691E
#define SV_Coral                0xFF7F50
#define SV_CornflowerBlue       0x6495ED
#define SV_Cornsilk             0xFFF8DC
#define SV_Crimson              0xDC143C
#define SV_Cyan                 0x00FFFF
#define SV_DarkBlue             0x00008B
#define SV_DarkCyan             0x008B8B
#define SV_DarkGoldenRod        0xB8860B
#define SV_DarkGray             0xA9A9A9
#define SV_DarkGrey             0xA9A9A9
#define SV_DarkGreen            0x006400
#define SV_DarkKhaki            0xBDB76B
#define SV_DarkMagenta          0x8B008B
#define SV_DarkOliveGreen       0x556B2F
#define SV_DarkOrange           0xFF8C00
#define SV_DarkOrchid           0x9932CC
#define SV_DarkRed              0x8B0000
#define SV_DarkSalmon           0xE9967A
#define SV_DarkSeaGreen         0x8FBC8F
#define SV_DarkSlateBlue        0x483D8B
#define SV_DarkSlateGray        0x2F4F4F
#define SV_DarkSlateGrey        0x2F4F4F
#define SV_DarkTurquoise        0x00CED1
#define SV_DarkViolet           0x9400D3
#define SV_DeepPink             0xFF1493
#define SV_DeepSkyBlue          0x00BFFF
#define SV_DimGray              0x696969
#define SV_DimGrey              0x696969
#define SV_DodgerBlue           0x1E90FF
#define SV_FireBrick            0xB22222
#define SV_FloralWhite          0xFFFAF0
#define SV_ForestGreen          0x228B22
#define SV_Fuchsia              0xFF00FF
#define SV_Gainsboro            0xDCDCDC
#define SV_GhostWhite           0xF8F8FF
#define SV_Gold                 0xFFD700
#define SV_GoldenRod            0xDAA520
#define SV_Gray                 0x808080
#define SV_Grey                 0x808080
#define SV_Green                0x008000
#define SV_GreenYellow          0xADFF2F
#define SV_HoneyDew             0xF0FFF0
#define SV_HotPink              0xFF69B4
#define SV_IndianRed            0xCD5C5C
#define SV_Indigo               0x4B0082
#define SV_Ivory                0xFFFFF0
#define SV_Khaki                0xF0E68C
#define SV_Lavender             0xE6E6FA
#define SV_LavenderBlush        0xFFF0F5
#define SV_LawnGreen            0x7CFC00
#define SV_LemonChiffon         0xFFFACD
#define SV_LightBlue            0xADD8E6
#define SV_LightCoral           0xF08080
#define SV_LightCyan            0xE0FFFF
#define SV_LightGoldenRodYellow 0xFAFAD2
#define SV_LightGray            0xD3D3D3
#define SV_LightGrey            0xD3D3D3
#define SV_LightGreen           0x90EE90
#define SV_LightPink            0xFFB6C1
#define SV_LightSalmon          0xFFA07A
#define SV_LightSeaGreen        0x20B2AA
#define SV_LightSkyBlue         0x87CEFA
#define SV_LightSlateGray       0x778899
#define SV_LightSlateGrey       0x778899
#define SV_LightSteelBlue       0xB0C4DE
#define SV_LightYellow          0xFFFFE0
#define SV_Lime                 0x00FF00
#define SV_LimeGreen            0x32CD32
#define SV_Linen                0xFAF0E6
#define SV_Magenta              0xFF00FF
#define SV_Maroon               0x800000
#define SV_MediumAquaMarine     0x66CDAA
#define SV_MediumBlue           0x0000CD
#define SV_MediumOrchid         0xBA55D3
#define SV_MediumPurple         0x9370DB
#define SV_MediumSeaGreen       0x3CB371
#define SV_MediumSlateBlue      0x7B68EE
#define SV_MediumSpringGreen    0x00FA9A
#define SV_MediumTurquoise      0x48D1CC
#define SV_MediumVioletRed      0xC71585
#define SV_MidnightBlue         0x191970
#define SV_MintCream            0xF5FFFA
#define SV_MistyRose            0xFFE4E1
#define SV_Moccasin             0xFFE4B5
#define SV_NavajoWhite          0xFFDEAD
#define SV_Navy                 0x000080
#define SV_OldLace              0xFDF5E6
#define SV_Olive                0x808000
#define SV_OliveDrab            0x6B8E23
#define SV_Orange               0xFFA500
#define SV_OrangeRed            0xFF4500
#define SV_Orchid               0xDA70D6
#define SV_PaleGoldenRod        0xEEE8AA
#define SV_PaleGreen            0x98FB98
#define SV_PaleTurquoise        0xAFEEEE
#define SV_PaleVioletRed        0xDB7093
#define SV_PapayaWhip           0xFFEFD5
#define SV_PeachPuff            0xFFDAB9
#define SV_Peru                 0xCD853F
#define SV_Pink                 0xFFC0CB
#define SV_Plum                 0xDDA0DD
#define SV_PowderBlue           0xB0E0E6
#define SV_Purple               0x800080
#define SV_RebeccaPurple        0x663399
#define SV_Red                  0xFF0000
#define SV_RosyBrown            0xBC8F8F
#define SV_RoyalBlue            0x4169E1
#define SV_SaddleBrown          0x8B4513
#define SV_Salmon               0xFA8072
#define SV_SandyBrown           0xF4A460
#define SV_SeaGreen             0x2E8B57
#define SV_SeaShell             0xFFF5EE
#define SV_Sienna               0xA0522D
#define SV_Silver               0xC0C0C0
#define SV_SkyBlue              0x87CEEB
#define SV_SlateBlue            0x6A5ACD
#define SV_SlateGray            0x708090
#define SV_SlateGrey            0x708090
#define SV_Snow                 0xFFFAFA
#define SV_SpringGreen          0x00FF7F
#define SV_SteelBlue            0x4682B4
#define SV_Tan                  0xD2B48C
#define SV_Teal                 0x008080
#define SV_Thistle              0xD8BFD8
#define SV_Tomato               0xFF6347
#define SV_Turquoise            0x40E0D0
#define SV_Violet               0xEE82EE
#define SV_Wheat                0xF5DEB3
#define SV_White                0xFFFFFF
#define SV_WhiteSmoke           0xF5F5F5
#define SV_Yellow               0xFFFF00
#define SV_YellowGreen          0x9ACD32

#endif      //_SV_DISPLAYRES_H_

