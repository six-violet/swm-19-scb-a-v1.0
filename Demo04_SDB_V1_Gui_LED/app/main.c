/*  杭州六堇科技有限公司
 *  2023-03-27
 *  GPIO Demo
 *  By Dawoo.Zheng
 *
 */

#include "main.h"
//#include "drvMcu.h"
#include "bmp.h"

const uint32_t colLedList[]=
{
    0x00FF0000,
    0x0000FF00,
    0x000000FF,
    0x00BF0040,
    0x00BF4000,
    0x00BF0000,
    0x0000BF40,
    0x0040BF00,
    0x0000BF00,
    0x000040BF,
    0x004000BF,
    0x000000BF,
    0x007F007F,
    0x007F7F00,
    0x007F3F3F,
    0x007F0000,
    0x007F003F,
    0x007F3F00,
    0x003F3FFF,
    0x003F6F3F,
    0x003F0F3F,
    0x003F3F3F,
    0x00BF0F0F,
    0x000FBF0F,
    0x000F0FBF,
    0x00BFBF0F,
    0x000FBFBF,
    0x00BF0FBF,
    0x007F0F0F,
    0x000F7F0F,
    0x000F0F7F,
    0x007F7F0F,
    0x000F7F7F,
    0x007F0F7F,
    0x003F0F0F,
    0x000F3F0F,
    0x000F0F3F,
    0x003F3F0F,
    0x000F3F3F,
    0x003F0F3F,
    0x00000000
};

extern void delayUs(volatile uint32_t u32DelayNum);
extern void delayMs(volatile uint32_t u32DelayNum);
//void delayMs(uint32_t t)
//{
//    uint16_t i,j;
//    for(i=0; i<t; i++)
//        for(j=0; j<1400; j++)
//            ;
//}

int main(void)
{
    uint16_t i = 0,j=0,k=0;
    uint16_t f1;
    SystemInit();

    delayMs(10);
    BSPInit();

    SetDrvP(pCOut[5]);
    for(i=0; i<16; i++)
    {
        awLEDPmA[0][i] = 0x0;
        awLEDPmA[1][i] = 0x0;
        awLEDPmA[2][i] = 0x0;
        awLEDPmA[3][i] = 0x0;
    }
    i = drvSoftReset9523(0); //初始化0???AW9523B
    i = drvSoftReset9523(2); //初始化0???AW9523B
    drvInitAW9523B();    //初始化AW9523B芯片
    drvSetLEDUpdata(0);
    drvSetLEDUpdata(2);
    screenDisplayInverOff();//
    screenTurn(0);//
    screenAllRefresh();
    /*
            for(i=0; i<10; i++)
            {
                drawRectangle(  1,  1,30,30,1);
                drawRectangle( 33,  1,63,30,1);
                drawRectangle( 65,  1,95,30,1);
                drawRectangle( 97,  1,127,30,1);
                screenAllRefresh();
                fillRectangle(  1, 33,30,30,1);
                fillRectangle( 33, 33,30,30,1);
                fillRectangle( 65, 33,30,30,1);
                fillRectangle( 97, 33,30,30,1);
                screenAllRefresh();
                delayMs(300);
                screenFillRam(0xFF);
                drawString(16, 0, 0,(uint8_t *)"ABCDEF0123456789", 0);
                drawString(16, 0,16,(uint8_t *)"EveryCourseHasRe", 0);
                drawString(16, 0,32,(uint8_t *)"alProjectsDesign", 0);
                drawString(16, 0,48,(uint8_t *)"fedcba9876543210", 0);
                screenAllRefresh();
                delayMs(300);
                screenFillRam(0xFF);
                drawString(16, 0, 0,(uint8_t *)"ABCDEF0123456789", 1);
                drawString(16, 0,16,(uint8_t *)"EveryCourseHasRe", 1);
                drawString(16, 0,32,(uint8_t *)"alProjectsDesign", 1);
                drawString(16, 0,48,(uint8_t *)"fedcba9876543210", 1);
                screenAllRefresh();
                delayMs(300);
                drawPadImage(  0,  0,127,8,32,4,(unsigned char *)gImage_04);
                delayMs(300);
                screenFill_5A();
                drawAsciiChar(16, 66,  7, 'A', 1);
                drawAsciiChar(16, 66, 37, 'B', 1);
                drawAsciiChar(16, 86,  7, 'C', 0);
                drawAsciiChar(16, 86, 37, 'D', 0);
                drawHex1B(16,32,32,i,0);
                screenAllRefresh();
                delayMs(800);
            }*/
    screenFill_5A();
    drawString(16, 0, 0,(uint8_t *)"  Red:", 0);
    drawString(16, 0,16,(uint8_t *)"Green:", 0);
    drawString(16, 0,32,(uint8_t *)" Blue:", 0);
    drawString(16,32,48,(uint8_t *)"Led:", 0);
    while(1==1)
    {
        if( k < 41)
            k++;
        else
        {
            k = 0;
            awLEDPmA[svLED[j].ledRedIC][svLED[j].ledRedmA] = 0x00;
            awLEDPmA[svLED[j].ledGreenIC][svLED[j].ledGreenmA] = 0x00;
            awLEDPmA[svLED[j].ledBlueIC][svLED[j].ledBluemA] = 0x00;
            if (j < 7)
                j++;
            else
                j = 0;
        }
        awLEDPmA[svLED[j].ledRedIC][svLED[j].ledRedmA] = (uint8_t)(colLedList[k]>>16);
        awLEDPmA[svLED[j].ledGreenIC][svLED[j].ledGreenmA] = (uint8_t)(colLedList[k]>>8);
        awLEDPmA[svLED[j].ledBlueIC][svLED[j].ledBluemA] = (uint8_t)(colLedList[k]);
        drvSetLEDUpdata(0);
        drvSetLEDUpdata(2);
//        i= drvRead9523_ID(2, 0x10);
        i= drvRead9523_ID(2, 0x01);
        drawHex1B(16,99,48,i+1,0);
        drawHex1B(16,64,48,j+1,0);

        fillRectangle( 48,  1,80,14,0);
        f1 = (uint8_t)(awLEDPmA[svLED[j].ledRedIC][svLED[j].ledRedmA]*80L/255);
        fillRectangle( 48,  1,f1,14,1);
        fillRectangle( 48, 17,80,14,0);
        f1 = (uint8_t)(awLEDPmA[svLED[j].ledGreenIC][svLED[j].ledGreenmA]*80L/255);
        fillRectangle( 48, 17,f1,14,1);
        fillRectangle( 48, 33,80,14,0);
        f1 = (uint8_t)(awLEDPmA[svLED[j].ledBlueIC][svLED[j].ledBluemA]*80L/255);
        fillRectangle( 48, 33,f1,14,1);
        screenAllRefresh();
        delayMs(300);

//        OledShowString(0, 2,"1234567890abcdefghij",12);
//        OledShowString(0,14,"klmnopqrstuvwxyz!@#$",12);
//        OledShowString(0,26,"ABCDEFGHIJKLMNOPQRST",12);
//        OledShowString(0,38,"UVWXYZ%^&*()_+-=~`[]",12);
//        OledShowString(0,50,"{}:;',><?/|\\\"",12);
//        OledRefresh();
//        GPIO_SetBit(GPIOC, PIN6);
//        delayMs(1000);

//        OledClear();
//        OledShowString(0, 0,"0123456789!@#$%^",16);
//        OledShowString(0,16,"ABCDEFGHIJKLMNOP",16);
//        OledShowString(0,32,"QRSTUVWXYZ_abcde",16);
//        OledShowString(0,48,"fghijklmnopqrstu",16);
//        OledRefresh();
//        GPIO_ClrBit(GPIOC, PIN6);
//        delayMs(1000);

//        OledClear();
//        OledShowString(0, 0,"0123456789",24);
//        OledShowString(0,24,"ABCDEFGHIJ",24);
//        OledRefresh();
//        GPIO_SetBit(GPIOC, PIN6);
//        delayMs(1000);

//        OledShowPicture(0,0,128,8,BMP1);
//        screenTurn(1);
//        OledDisplayTurn(1);
//        GPIO_ClrBit(GPIOC, PIN6);
//        delayMs(10);
//        screenTurn(0);
//        OledDisplayTurn(0);
    }
}


#if USE_GPIOA4_IRQ

void GPIOC5_Handler(void)
{
    EXTI_Clear(GPIOC, PIN5);
}

#else

void GPIOC_Handler(void)
{
    if(EXTI_State(GPIOC, PIN5))
    {
        InvDrvP(pCOut[5]);
    }
}

#endif

