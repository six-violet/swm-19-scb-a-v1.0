/*  杭州六堇科技有限公司
 *  2023-04-19
 *  drvExI2C
 *  By Dawoo.Zheng
 *
 *  Synwit MCU
 */

#include "drvExI2C.h"

void I2C_Master_Init(uint8_t setFlag,uint32_t speed,I2C_TypeDef * i2c_x);
void I2C_Slave_Init(uint8_t setFlag,uint16_t slvAddr,I2C_TypeDef * i2c_x);

void I2C_Master_Init(uint8_t setFlag,uint32_t speed,I2C_TypeDef * i2c_x)
{
    I2C_InitStructure i2C_InitS;
    i2C_InitS.Master = 0x01;        //uint8_t  1 主机模式
    if((setFlag & 0x02) == 0)
        i2C_InitS.Addr7b = 0x01;    //uint8_t  ==1> 7位地址
    else
        i2C_InitS.Addr7b = 0x00;    //uint8_t  ==0>10位地址

    i2C_InitS.MstClk = speed;       //uint32_t 主机传输时钟频率
    if((setFlag & 0x01) == 0)
        i2C_InitS.MstIEn = 0;       //uint8_t  主机模式中断使能
    else
        i2C_InitS.MstIEn = 0;       //uint8_t  主机模式中断使能
    I2C_Init(i2c_x, &i2C_InitS);
    I2C_Open(i2c_x);
}

void I2C_Slave_Init(uint8_t setFlag,uint16_t slvAddr,I2C_TypeDef * i2c_x)
{
    I2C_InitStructure i2C_InitS;
    i2C_InitS.Master = 0x00;        //uint8_t  1 主机模式
    if((setFlag & 0x02) == 0)
        i2C_InitS.Addr7b = 0x01;    //uint8_t  ==1> 7位地址
    else
        i2C_InitS.Addr7b = 0x00;    //uint8_t  ==0>10位地址

    i2C_InitS.SlvAddr = slvAddr;    //uint16_t 从机地址
    i2C_InitS.SlvAddrMask  = 0xFF;  //uint16_t 从机地址掩码
    i2C_InitS.SlvRxEndIEn  = 0x01;  //uint8_t  从机接收完成中断使能
    i2C_InitS.SlvTxEndIEn  = 0x01;  //uint8_t  从机发送完成中断使能
    i2C_InitS.SlvSTADetIEn = 0x01;  //uint8_t  从机检测到起始中断使能
    i2C_InitS.SlvSTODetIEn = 0x01;  //uint8_t  从机检测到终止中断使能
    i2C_InitS.SlvRdReqIEn  = 0x00;  //uint8_t  从机接收到读请求中断使能
    i2C_InitS.SlvWrReqIEn  = 0x01;  //uint8_t  从机接收到写请求中断使能
    I2C_Init(i2c_x, &i2C_InitS);

    I2C_Open(i2c_x);
}

uint8_t I2C_Master_Transmit(I2C_TypeDef * i2c_x, uint8_t mAddr,uint8_t * dat, uint8_t datLen)
{
//	uint8_t ack,i;
//	for(i=0; i<datLen; i++)
//	{
//		ack = I2C_Write(i2c_x, *dat++);
//	}
//	return ack;

    uint8_t re = 0,i,ack;
    uint8_t * p;
    p = dat;
//    I2C_Stop(i2c_x);
    ack = I2C_Start(i2c_x, mAddr);  //(mAddr << 1) | 0);
    if(ack == 0)
    {
        I2C_Stop(i2c_x);
        return ++re;
    }
    for(i=0; i<datLen; i++)
    {
        ack = I2C_Write(i2c_x, *p++);
        ++re;
        if(ack == 0)
        {
            I2C_Stop(i2c_x);
            return re;
        }
    }
    I2C_Stop(i2c_x);
    return re;
}


uint8_t I2C_Master_ReceiveByte(I2C_TypeDef * i2c_x, uint8_t mAddr,uint8_t * dat, uint8_t datLen)
{
    uint8_t re = 0,i,ack;
    uint8_t * p;
    p = dat;
//    I2C_Stop(i2c_x);
    ack = I2C_Start(i2c_x, mAddr);  //(mAddr << 1) | 0);
    if(ack == 0)
    {
        I2C_Stop(i2c_x);
        return ++re;
    }
    for(i=0; i<datLen; i++)
    {
        ack = I2C_Write(i2c_x, *p++);
        ++re;
        if(ack == 0)
        {
            I2C_Stop(i2c_x);
            return re;
        }
    }
//    I2C_Stop(i2c_x);
    ack = I2C_Start(i2c_x, mAddr+1);  //(mAddr << 1) | 1);+1
    if(ack == 0)
    {
        printf("Read Start addr. Error!\r\n");
        I2C_Stop(i2c_x);
        return ack;
    }
    for(i=0; i<datLen-1; i++)
    {
        re = I2C_Read(i2c_x, 1);
    }
    re = I2C_Read(i2c_x, 0);
    I2C_Stop(i2c_x);
    return re;
}

uint8_t I2C_Master_Receive(I2C_TypeDef * i2c_x, uint8_t mAddr,uint8_t * dat, uint8_t datLen)
{
    uint8_t ack = 99,i;
    uint8_t * p;
    p = dat;
//    I2C_Stop(i2c_x);
    ack = I2C_Start(i2c_x, mAddr);  //(mAddr << 1) | 1);+1
    if(ack == 0)
    {
        printf("Read Start addr. Error!\r\n");
        I2C_Stop(i2c_x);
        return ack;
    }
    for(i=0; i<datLen-1; i++)
    {
        *p++ = I2C_Read(i2c_x, 1);
    }
    *p++ = I2C_Read(i2c_x, 0);
    I2C_Stop(i2c_x);
    return ack;
}

