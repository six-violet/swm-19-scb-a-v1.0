/* www.6violet.com
 * FileName drvMcu.c
 * creat time: 2023-8-27 by dawoo.Zheng
 *    The last modifier: dawoo.Zheng
 * The last change time: 2023-8-27
 * commit:
 */

#include "drvMcu.h"
#include "drvGpio.h"
#include "displayConfig.h"
//#include "drvTimEncoder.h"
//#include "drvIWDT.h"

#define TIM_CLOCK_FREQ (10000)
#define MAXCRCBUF 256
#define _MAX_ENCODER_COUNT_ 256

uint8_t crcDataBuf[MAXCRCBUF];
//TIM_HandleTypeDef tim6Handler;
//UART_HandleTypeDef uart1Handle;
//UART_HandleTypeDef uart2Handle;
//UART_HandleTypeDef uart3Handle;
//UART_HandleTypeDef uart4Handle;
volatile uint32_t mKeyPressFlag;

volatile uint32_t lastCountTIM;

volatile uint8_t nowKeyType;
//volatile uint8_t flagKEY;
volatile _RUNKEYFLAG mRunKeyFlag;
//volatile _ENCODER_FLAG mEncoderFlag;
volatile uint8_t pKeyTime,rKeyTime;

//TIM_HandleTypeDef	timxHandler;
//TIM_Encoder_InitTypeDef	timxEncoderInitHandler;

typedef struct
{
    uint8_t flagNMs1:1;       //
    uint8_t flagNMs2:1;       //
    uint8_t flagNMs3:1;       //
    uint8_t flagNMs4:1;       //
    uint8_t flagNMs5:1;       //
    uint8_t flagNMs6:1;       //
    uint8_t flagNMs7:1;       //
    uint8_t flagNMs8:1;       //
} _FLAG_NMs ;
volatile _FLAG_NMs mFlagNm;
volatile uint32_t _5MsCount;

/// void uartInit(UART_TypeDef * uartName,uint32_t fu32Baudrate, uint32_t fu32Mode)
void SerialInit(void)
{
    uint16_t i,j;
    UART_InitStructure UART_initStruct;

    PORT_Init(PORTB, PIN8, PORTB_PIN8_UART2_TX, 0);		//GPIOB.8->>UART2 TXD
    PORT_Init(PORTB, PIN7, PORTB_PIN7_UART2_RX, 1);		//GPIOB.7->>UART2 RXD
    UART_initStruct.Baudrate = 115200;
    UART_initStruct.DataBits = UART_DATA_8BIT;
    UART_initStruct.Parity = UART_PARITY_NONE;
    UART_initStruct.StopBits = UART_STOP_1BIT;
    UART_initStruct.RXThresholdIEn = 0;
    UART_initStruct.TXThresholdIEn = 0;
    UART_initStruct.TimeoutIEn = 0;
    UART_Init(UART2, &UART_initStruct);
    UART_Open(UART2);

    for(i=0; i<50; i++)
        for(j=0; j<1400; j++)
            ;

    PORT_Init(PORTD, PIN1, PORTD_PIN1_UART0_TX, 0);		//GPIOD.1->>UART0 TXD
    PORT_Init(PORTD, PIN0, PORTD_PIN0_UART0_RX, 1);		//GPIOD.0->>UART0 RXD
    UART_initStruct.Baudrate = 115200;
    UART_initStruct.DataBits = UART_DATA_8BIT;
    UART_initStruct.Parity = UART_PARITY_NONE;
    UART_initStruct.StopBits = UART_STOP_1BIT;
    UART_initStruct.RXThresholdIEn = 0;
    UART_initStruct.TXThresholdIEn = 0;
    UART_initStruct.TimeoutIEn = 0;
    UART_Init(UART0, &UART_initStruct);
    UART_Open(UART0);
}

void I2CMstInit(void)
{
    I2C_InitStructure I2C_initStruct;

    PORT_Init(PORTB, PIN15, PORTB_PIN15_I2C0_SCL, 1);	//GPIOB.15配置为I2C0 SCL引脚
    PORTB->PULLU |= (1 << PIN15);					    //必须使能上拉，用于模拟开漏
    PORT_Init(PORTB, PIN14, PORTB_PIN14_I2C0_SDA, 1);	//GPIOB.14配置为I2C0 SDA引脚
    PORTB->PULLU |= (1 << PIN14);					    //必须使能上拉，用于模拟开漏

    I2C_initStruct.Master = 1;
    I2C_initStruct.Addr7b = 1;
    I2C_initStruct.MstClk = 10000;
    I2C_initStruct.MstIEn = 0;
    I2C_Init(I2C0, &I2C_initStruct);

    I2C_Open(I2C0);
//	I2C_Master_Init(0, 100000, I2C0);
}

void printZ(UART_TypeDef * UARTx, char * str)
{
    unsigned long int sl = 0L;
    while(sl < 3000)
    {
        while(UART_IsTXBusy(UARTx));
        if(*str != 0)
            UART_WriteByte(UARTx, *str);
        else
            sl = 3000;
        str++;
    }
}

void mcuInit(void)
{
    InitAllPin();
    SerialInit();
    I2CMstInit();
}

/// Ex: delayMs(100);
void delayUs(volatile uint32_t u32DelayNum)
{
    uint32_t t_i,i;
    t_i = (u32DelayNum * CyclesPerUs)/10;
    for(i=0; i<t_i; i++)
    {
        ;
    }
}

void delayMs(volatile uint32_t u32DelayNum)
{
    uint32_t t_i;
    t_i = u32DelayNum * 1000;
    if(t_i >= u32DelayNum)
        delayUs(t_i);
    else
    {
        for(t_i = 0; t_i<u32DelayNum ; t_i++)
        {
            delayUs(1000);
        }
    }
//    uint32_t i,j,k;
//    for(k=0; k<u32DelayMsNum; k++)
//    {
//        for(i=0; i<130; i++)
//        {
//            for(j=0; j<130; j++)
//            {
//                ;
//            }
//        }
//    }
}

void crcDataInit(void)
{
    uint32_t i;
    for(i = 0; i < MAXCRCBUF-2; i++ )
    {
        crcDataBuf[i] = i;
    }
}

uint16_t strSizeC(uint8_t *strBuf)
{
    uint8_t * str;
    uint16_t count = 0;
    str = strBuf;
    while(count < 65000)
    {
        if(*str++ == 0)
            return (uint16_t)(str-strBuf-1);
    }
    return 0;
}

void keyInputInit(void)
{
    //Set               PA5 KeyInput
}

void gpioInitAll(void)
{
    InitAllPin();
#if (SCREEN_USE_SPI == 1)       //如果使用SPI接口
#endif  //SCREEN_USE_SPI ==1
}

#if (SCREEN_USE_SPI == 1)       //如果使用SPI接口
void gpioInitSPI(void)
{
    SPI_InitStructure SPI_initStruct;

    SPI_initStruct.clkDiv = SPI_CLKDIV_2;
    SPI_initStruct.FrameFormat = SPI_FORMAT_SPI;
    SPI_initStruct.SampleEdge = SPI_FIRST_EDGE;
    SPI_initStruct.IdleLevel = SPI_LOW_LEVEL;
    SPI_initStruct.WordSize = 8;
    SPI_initStruct.Master = 1;
    SPI_initStruct.RXThreshold = 0;
    SPI_initStruct.RXThresholdIEn = 0;
    SPI_initStruct.TXThreshold = 0;
    SPI_initStruct.TXThresholdIEn = 0;
    SPI_initStruct.TXCompleteIEn = 0;
    SPI_Init(SPI0, &SPI_initStruct);
    SPI_Open(SPI0);
}

void Spi_Send_N_Data(SPI_TypeDef* SPIx, uint8_t *data, uint32_t num)
{
    uint32_t i;
    for(i=0; i<num ; i++)
    {
        SPI_WriteWithWait(SPIx,*(uint8_t *)(data++));
    }
}
#endif  //SCREEN_USE_SPI ==1

void gpioInitUart(void)
{
}

void screenPortInit(void)
{
}

void BSPInit(void)
{
    gpioInitAll();
#if (SCREEN_USE_SPI == 1)       //????SPI??
    gpioInitSPI();
#endif  //SCREEN_USE_SPI ==1
//    SerialInit();
    I2CMstInit();
    //gpioInitUart();
    //uartInit(UART1, 115200, UART_MODE_TX_RX);
    //uartInit(UART3, 115200, UART_MODE_TX_RX_DEBUG);

    //tim6Init();
    //keyInputInit();
    //encoderInit();
    screenDrvInit();
}

