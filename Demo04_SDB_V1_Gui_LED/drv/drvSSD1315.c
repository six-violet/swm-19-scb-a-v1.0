/* www.6violet.com
 * FileName drvSSD1315.c
 * creat time: 2023-8-28 by dawoo.Zheng
 *    The last modifier: dawoo.Zheng
 * The last change time: 2024-03-02
 * commit:
 */

#include "drvMcu.h"
//#include "drvGpio.h"
#include "drvSSD1315.h"

uint8_t OledGRAM[SV_SCREEN_H/8][SV_SCREEN_W];
//extern void screenPortInit(void);             //对应的硬件接口初始化

#if (SCREEN_USE_SPI == 1)                       //如果使用SPI接口
//#include "SWM190_spi.h"
#endif  //SCREEN_USE_SPI ==1

#if (SCREEN_USE_SGPIO == 1)                     //如果使用GPIO模拟串行接口
//#include "drvGpio.h"
#endif  //SCREEN_USE_SGPIO ==1

#if (SCREEN_USE_PGPIO == 1)                     //如果使用GPIO并行接口屏
//#include "drvGpio.h"
#endif  //SCREEN_USE_PGPIO ==1

void screenGpioInit(void)
{
#if (SCREEN_USE_SGPIO == 1)                     //如果使用GPIO模拟串行接口
    screenSclkSet();
    screenMosiSet();
#endif  //SCREEN_USE_SPI ==1
#if (SCREEN_HARDRESET == 1)                     //如果屏有硬件复位
    screenRESSet();
#endif
#if (SCREEN_USE_PGPIO == 1)                     //如果使用GPIO并行接口屏
    screenRDSet();                              //RD=1
    screenWRSet();                              //WR=1
#endif
    screenDCSet();
    screenCSSet();
#if (SCREEN_HARD_BLK == 1)   //如果屏有硬件背光控制
    screenBLKSet();
#endif
    screenCSClr();
}

void screenWriteByteNoCS(uint8_t data)
{
#if (SCREEN_USE_SGPIO == 1)
    uint8_t i;
    for(i=0; i<8; i++)
    {
        screenSclkClr();
        if(data&0x80)
        {
            screenMosiSet();
        }
        else
        {
            screenMosiClr();
        }
        screenSclkSet();
        data<<=1;
    }
#endif
#if (SCREEN_USE_SPI == 1)
    SV_SPI_Transmit1Byte(&data);
#endif
}

void screenWriteByteData(uint8_t data)
{
#if (SCREEN_USE_SPI == 1)
    SV_SPI_Transmit1Byte(&data);
#endif
#if (SCREEN_USE_SGPIO == 1)
    uint8_t i;
    for(i=0; i<8; i++)
    {
        screenSclkClr();
        if(data&0x80)
        {
            screenMosiSet();
        }
        else
        {
            screenMosiClr();
        }
        screenSclkSet();
        data<<=1;
    }
#endif
#if (SCREEN_USE_PGPIO == 1)
    screenWRClr();
    screenWriteP16Bit((uint16_t)data);
    screenWRSet();
#endif   //SCREEN_USE_PGPIO
}

void screenWriteWordData(uint16_t data)
{
#if (SCREEN_USE_SPI == 1)
    static uint8_t temp[2];
    temp[0] = data >> 8;
    temp[1] = data;
    SV_SPI_TransmitNByte(&temp,2);
#endif
#if (SCREEN_USE_SGPIO == 1)
    uint8_t i;
    for(i=0; i<16; i++)
    {
        screenSclkClr();
        if(data&0x8000)
        {
            screenMosiSet();
        }
        else
        {
            screenMosiClr();
        }
        screenSclkSet();
        data<<=1;
    }
#endif
#if (SCREEN_USE_PGPIO == 1)
    screenWRClr();
    screenWriteP16Bit(data);
    screenWRSet();
#endif   //SCREEN_USE_PGPIO
}

void screenWriteDoubleData(uint32_t data)
{
#if (SCREEN_USE_SPI == 1)
    uint8_t temp[4];
    temp[0] = (uint8_t)((data >> 24)& 0xff);
    temp[1] = (uint8_t)((data >> 16)& 0xff);
    temp[2] = (uint8_t)((data >> 8)& 0xff);
    temp[3] = (uint8_t)((data)& 0xff);
    SV_SPI_TransmitNByte(&temp,4);
#endif
#if (SCREEN_USE_SGPIO == 1)
    uint8_t i;
    for(i=0; i<32; i++)
    {
        screenSclkClr();
        if(data&0x80000000)
        {
            screenMosiSet();
        }
        else
        {
            screenMosiClr();
        }
        screenSclkSet();
        data<<=1;
    }
#endif
#if (SCREEN_USE_PGPIO == 1)
    screenWRClr();
    screenWriteP16Bit((uint16_t)(data>>16));
    screenWRSet();
    screenWRClr();
    screenWriteP16Bit((uint16_t)(data&0xFFFF));
    screenWRSet();
#endif   //SCREEN_USE_PGPIO
}

void screenWrite_NByteData(unsigned char * data,uint16_t number)
{
#if (SCREEN_USE_SPI == 1)
    SV_SPI_TransmitNByte(data,number);
#endif
#if (SCREEN_USE_SGPIO == 1)
    uint16_t i;
    for(i=0; i<number; i++)
    {
        screenWriteByteNoCS(*data++);
    }
#endif
#if (SCREEN_USE_PGPIO == 1)
    uint16_t i;
    for(i=0; i<number; i++)
    {
        screenWriteWordData(*(uint16_t *)data);
        data+=2;
    }
#endif   //SCREEN_USE_PGPIO
}

void screenWriteByteCMD(uint8_t data)               //屏幕写 8bit命令
{
    screenDCClr();                                  //写命令
    screenCSSet();
    screenCSClr();
    screenWriteByteData(data);
    screenDCSet();                                  //写数据
}

void screenWriteWordCMD(uint16_t data)              //屏幕写16bit命令
{
    screenDCClr();                                  //写命令
    screenCSSet();
    screenCSClr();
    screenWriteByteData((uint8_t)(data>>8));
//    screenDCSet();
//    screenDCClr();
    screenWriteByteData((uint8_t)data);
    screenDCSet();                                  //写数据
}

void screenSetViewArea(SV_COORD x1,SV_COORD y1,SV_COORD x2,SV_COORD y2)
{
    screenWriteByteCMD(SVO_SetColumnAddr);          //C0[0x21]:设置列地址
    screenWriteByteData(x1&0x7F);
    screenWriteByteData(x2&0x7F);
    screenWriteByteCMD(SVO_SetPageAddr);            //C0[0x22]:设置行地址
    screenWriteByteData(y1&0x7F);
    screenWriteByteData(y2&0x7F);
}

void screenDrvPixel(SV_COORD x,SV_COORD y,SV_COLOR color)
{
    uint8_t x0,y0,y1;
    x0=x&(SV_SCREEN_W-1);
    y0= y/8;
    y1= y%8;
    if(color != 0)
        OledGRAM[y0][x0] |= (0x01<<y1);
    else
        OledGRAM[y0][x0] &= (~(0x01<<y1));
}

void screenDrvBlock(SV_COORD x,SV_COORD y,uint16_t w,uint16_t h,SV_COLOR color)     //画色块
{
#if (SCREEN_USE_SGPIO == 1)
    uint16_t i,row;
#endif
#if (SCREEN_USE_SPI == 1)
    uint16_t i,row;
    uint8_t buffer[SV_SCREEN_W*2];
    uint16_t remain;
#endif
    screenCSClr();
//    screenWriteByteCMD(SV_ColumnAddSet);            //0x2A//纵地址设置§
//    screenWriteByteData((x+LCD_XOFFSET_P)>>8);
//    screenWriteByteData(x+LCD_XOFFSET_P);
//    screenWriteByteData((x+LCD_XOFFSET_P+w-1)>>8);
//    screenWriteByteData(x+LCD_XOFFSET_P+w-1);
//    screenWriteByteCMD(SV_PageAddSet);              //0x2B//页地址设置§
//    screenWriteByteData((y+LCD_YOFFSET_P)>>8);
//    screenWriteByteData(y+LCD_YOFFSET_P);
//    screenWriteByteData((y+LCD_YOFFSET_P+h-1)>>8);
//    screenWriteByteData(y+LCD_YOFFSET_P+h-1);
//    screenWriteByteCMD(SV_MemoryWrite);             //0x2C//内存写§
#if (SCREEN_USE_SGPIO == 1)
    for(row = 0; row < h; row++)
    {
        for(i = 0; i < w; i++)
        {
            screenWriteWordData(color);
        }
    }
#endif
#if (SCREEN_USE_SPI == 1)
    row = (w * h)/ SV_SCREEN_W;
    remain = (w * h)% SV_SCREEN_W ;
    for(i = 0; i<SV_SCREEN_W; i++)
    {
        buffer[i*2] = color >> 8;
        buffer[i*2+1] = color;
    }

    for(i = 0; i<row; i++)
    {
        SV_SPI_TransmitNByte(buffer, SV_SCREEN_W*2);
    }

    if(remain > 0)
    {
        SV_SPI_TransmitNByte(buffer, remain*2);
    }
#endif
    screenCSSet();
}

void screenSetBright(uint8_t bright)
{
//    screenCSClr();
//    screenWriteByteCMD(SV_WriteDispBright);
//    screenWriteByteData(bright);
//    screenCSSet();
}

void screenDrvInit(void)
{
    screenGpioInit();        //已经在drvMcu.c BSPInit()中初始化掉了。
#if (SCREEN_HARDRESET == 1)     //如果屏有硬件复位
    screenRESClr();
    delayMs(50); //Delay 50ms
    screenRESSet();
    delayMs(70); //Delay 70ms
#else
//    screenWriteByteCMD(SV_SoftwareReset);           //0x01//软复位
    delayMs(120); //Delay 120ms
#endif

    screenWriteByteCMD(SVO_SetDispOff);             //C0[0xAE]:设置关显示
    screenWriteByteCMD(SVO_SetPageLAddr+0);         //C0[0x00-0x0F]:设置坐标低页地址
    screenWriteByteCMD(SVO_SetPageHAddr+0);         //C0[0x10-0x17]:设置坐标高页地址
    screenWriteByteCMD(SVO_SetDispSLine);           //C0[0x40-0x7F]:设置显示开始行
    screenWriteByteCMD(SVO_SetContrast);            //C0[0x81]:设置对比控制
    screenWriteByteCMD(0xCF);                       //C1[0x00-0xFF]:对比度值
    screenWriteByteCMD(SVO_SetSegRemap+1);          //C0[0xA0-0xA1]:设置部分重绘
    screenWriteByteCMD(SVO_SetComOutDir+8);         //C0[0xC0-0xC8]:设置COM脚的扫描方向
    screenWriteByteCMD(SVO_SetNor_Inv+0);           //C0[0xA6-0xA7]:设置正反转
    screenWriteByteCMD(SVO_SetMultRatio);           //C0[0xA8]:设置多比率
    screenWriteByteCMD(0x3F);                       //C1[0x00-0x3F]:
    screenWriteByteCMD(SVO_SetDispOffset);          //C0[0xD3]:设置偏移量
    screenWriteByteCMD(0x00);                       //C1[0x00-0x3F]:
    screenWriteByteCMD(SVO_SetDispClkFre);          //C0[0xD5]:设置时钟相关
    screenWriteByteCMD(0x80);                       //C1[0x00-0xFF]:b[3:0]/b[7:4]
    screenWriteByteCMD(SVO_SetPha2Charge);          //C0[0xD9]:设置相1相2充电时钟
    screenWriteByteCMD(0xF1);                       //C1[0x00-0xFF]:
    screenWriteByteCMD(SVO_SetCOM_Hard);            //C0[0xDA]:设置COM硬件设置
    screenWriteByteCMD(0x12);                       //C1[0x02-0x32]:b1=1b;
    screenWriteByteCMD(SVO_SetVcomhLev);            //C0[0xDB]:设置COM电压
    screenWriteByteCMD(0x40);                       //C1[0x00-0x30]:0.65，0.71，0.77，0.83
    screenWriteByteCMD(SVO_SetMemoryAddr);          //C0[0x20]:设置内存地址，下一个命令
    screenWriteByteCMD(0x02);                       //C1[0x00-0x03]:四种模式
    screenWriteByteCMD(SVO_SetChargePump);          //C0[0x8D]:设置内部充电选择
    screenWriteByteCMD(0x14);                       //C1[0x10-0x95]:set,b7+b2+b0;
    screenWriteByteCMD(SVO_SetEntireON);            //C0[0xA4-0xA5]:设置全部显示
    screenWriteByteCMD(SVO_SetNor_Inv);             //C0[0xA6-0xA7]:设置正反转
    screenWriteByteCMD(SVO_SetDispON);              //C0[0xAE]:设置开显示

    delayMs(20);
#if (SCREEN_HARD_BLK == 1)   //如果屏有硬件背光控制
    screenBLKSet();                                 //打开背光
#endif
}

void screenOff(void)    //关闭屏幕或背光
{
    screenCSClr();
    screenWriteByteCMD(SVO_SetChargePump);      //0x8D//电荷泵使能
    screenWriteByteCMD(0x10);                   //0x10//开启电荷泵
    screenWriteByteCMD(SVO_SetDispOff);         //0xAE//显示关闭
#if (SCREEN_HARD_BLK == 1)   //如果屏有硬件背光控制
    screenBLKClr();
#endif
    screenCSSet();
}

void screenON(void)     //打开屏幕或背光
{
    screenCSClr();
    screenWriteByteCMD(SVO_SetChargePump);      //0x8D//电荷泵使能
    screenWriteByteCMD(0x14);                   //0x14//开启电荷泵
    screenWriteByteCMD(SVO_SetDispON);          //0xAF//显示打开§
#if (SCREEN_HARD_BLK == 1)   //如果屏有硬件背光控制
    screenBLKSet();
#endif
    screenCSSet();
}

void screenDisplayInverOff(void)
{
    screenCSClr();
    screenWriteByteCMD(SVO_SetNor_Inv);         //0xA6//倒置关闭
    screenCSSet();
}

void screenDisplayInverON(void)
{
    screenCSClr();
    screenWriteByteCMD(SVO_SetNor_Inv + 1);     //0xA7//倒置打开§
    screenCSSet();
}

///////>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
void screenDrvNop()                              //空指令
{
    screenWriteByteCMD(SVO_Soft_NOP);        //0xE3//空指令
}
void screenDrvSleep(uint8_t state)               //=1进入睡眠//=0退出睡眠
{
}
void screenDrvBrightnes(uint8_t val)
{
}
void screenDrvInversion(uint8_t state)           //=1进入反色//=0退出反色
{
    screenCSClr();
    if(state == 0)
        screenWriteByteCMD(SVO_SetNor_Inv);         //0xA6//倒置关闭
    if(state == 1)
        screenWriteByteCMD(SVO_SetNor_Inv + 1);     //0xA7//倒置打开§
    screenCSSet();
}

void screenDrvDisplay(uint8_t state)             //=1进入反色//=0退出反色
{
}

void screenDrvSoftReset()
{
}

uint8_t screenDrvReadByte()
{
    return 0;
}

uint16_t screenDrvReadWord()
{
    return 0;
}

uint8_t screenDrvGetManu()                      //读取显示屏制造商
{
    return 0;
}

uint8_t screenDrvGetVersion()                   //读取显示屏版本号
{
    return 0;
}

uint8_t screenDrvGetId()                        //读取显示屏版本号
{
    return 0;
}

void screenSetBeginCoord(SV_COORD x,SV_COORD y)
{
    if(x<0 || x>=SV_SCREEN_W)
        x=0;
    if(y<0 || y>=SV_SCREEN_H)
        y=0;
    screenWriteByteCMD(SVO_SetPageStart | y);   //C0[0xB0-0xB7]:设置GDDRAM_PageStart
    screenWriteByteCMD(SVO_SetPageLAddr | (uint8_t)(x&0x0f));       //C0[0x00-0x0F]:设置坐标低页地址
    screenWriteByteCMD(SVO_SetPageHAddr | (uint8_t)((x>>4)&0x0f));    //C0[0x10-0x17]:设置坐标高页地址
}

void screenFill(SV_COLOR color)
{
    uint8_t i,j;
    uint8_t cDat;
    cDat = (uint8_t)color&0xFF;
    screenCSClr();
    for (i=0; i<8; i++)
    {
        screenSetBeginCoord(0,i);
        for (j=0; j<SV_SCREEN_W; j++)
        {
            screenWriteByteData(cDat);
        }
    }
    screenCSSet();
}

void screenFill_A5(void)
{
    uint8_t i,j;
    for (i=0; i<8; i++)
    {
        for (j=0; j<SV_SCREEN_W; j++)
        {
            if((j%2) == 0)
                OledGRAM[i][j] = 0xAA;
            else
                OledGRAM[i][j] = 0x55;
        }
    }
}

void screenFill_5A(void)
{
    uint8_t i,j;
    for (i=0; i<8; i++)
    {
        for (j=0; j<SV_SCREEN_W; j++)
        {
            if((j%2) == 0)
                OledGRAM[i][j] = 0x55;
            else
                OledGRAM[i][j] = 0xAA;
        }
    }
}

void screenFillRam(SV_COLOR color)
{
    uint8_t i,j;
    uint8_t cDat;
    cDat = (uint8_t)color&0xFF;
    for (i=0; i<8; i++)
    {
        for (j=0; j<SV_SCREEN_W; j++)
        {
            OledGRAM[i][j] = cDat;
        }
    }
}

void OledShowPicture(SV_COORD x0,SV_COORD y0,SV_COORD x1,SV_COORD y1,unsigned char * picData)
{
    uint8_t x=0,y=0;
    screenCSClr();
    for(y= 0; y<y1; y++)
    {
        screenSetBeginCoord(x0,y0+y);
        for(x=0; x<x1; x++)
        {
            screenWriteByteData(*picData++);
        }
    }
    screenCSSet();
}

void ShowPictureRam(SV_COORD x0,SV_COORD y0,SV_COORD x1,SV_COORD y1,unsigned char * picData)
{
    uint8_t x=0,y=0;
//    uint16_t ramOffset;
    screenCSClr();
    for(y= 0; y<y1; y++)
    {
//        ramOffset = x0 + SV_SCREEN_W*y;
        for(x=0; x<x1; x++)
        {
            OledGRAM[y0+y][x0+x] = *picData++;
//            screenWriteByteData(*picData++);
        }
    }
    screenCSSet();
}

void screenTurn(uint8_t i)                      //i=0,屏幕旋转0度;i=1,屏幕旋转180度
{
    if(i == 0)
    {
        screenWriteByteCMD(SVO_SetComOutDir + 8);       //C0[0xC8]
        screenWriteByteCMD(SVO_SetSegRemap + 1);        //C0[0xA1]
    }
    if(i == 1)
    {
        screenWriteByteCMD(SVO_SetComOutDir + 0);       //C0[0xC0]
        screenWriteByteCMD(SVO_SetSegRemap + 0);        //C0[0xA0]
    }
}

void screenAllRefresh(void)                             //更新显存到OLED
{
    uint8_t i,n;
    screenCSClr();
    for(i=0; i<8; i++)
    {
        screenWriteByteCMD(SVO_SetPageStart + i);   //设置行起始地址
        screenWriteByteCMD(SVO_SetPageLAddr);   //设置低列起始地址
        screenWriteByteCMD(SVO_SetPageHAddr);   //设置高列起始地址
        for(n=0; n < SV_SCREEN_W; n++)
            screenWriteByteData(OledGRAM[i][n]);
    }
    screenCSSet();
}


