/*  杭州六堇科技有限公司
 *  2023-03-27
 *  GPIO Demo
 *  By Dawoo.Zheng
 *
 */

#include "main.h"
//#include "drvMcu.h"
#include "bmp.h"

#define _B_J4_ON       0		//PCB J4 ON Open AW9523B IC Pow

#define ADC_SIZE  1000
uint16_t ADC_Result[ADC_SIZE] = {0};

const uint32_t colLedList[]=
{
    0x00FF0000,
    0x0000FF00,
    0x000000FF,
    0x00BF0040,
    0x00BF4000,
    0x00BF0000,
    0x0000BF40,
    0x0040BF00,
    0x0000BF00,
    0x000040BF,
    0x004000BF,
    0x000000BF,
    0x007F007F,
    0x007F7F00,
    0x007F3F3F,
    0x007F0000,
    0x007F003F,
    0x007F3F00,
    0x003F3FFF,
    0x003F6F3F,
    0x003F0F3F,
    0x003F3F3F,
    0x00BF0F0F,
    0x000FBF0F,
    0x000F0FBF,
    0x00BFBF0F,
    0x000FBFBF,
    0x00BF0FBF,
    0x007F0F0F,
    0x000F7F0F,
    0x000F0F7F,
    0x007F7F0F,
    0x000F7F7F,
    0x007F0F7F,
    0x003F0F0F,
    0x000F3F0F,
    0x000F0F3F,
    0x003F3F0F,
    0x000F3F3F,
    0x003F0F3F,
    0x00000000
};

extern void delayUs(volatile uint32_t u32DelayNum);
extern void delayMs(volatile uint32_t u32DelayNum);

int main(void)
{
    uint16_t i = 0;//j=0,k=0;
    uint32_t adcVal1,adcVal2;
    SystemInit();

    delayMs(10);
    BSPInit();

    SetDrvP(pCOut[5]);
    for(i=0; i<16; i++)
    {
        awLEDPmA[0][i] = 0x0;
        awLEDPmA[1][i] = 0x0;
        awLEDPmA[2][i] = 0x0;
        awLEDPmA[3][i] = 0x0;
    }
#if( _B_J4_ON == 1)
    i = drvSoftReset9523(0); //初始化0???AW9523B
    i = drvSoftReset9523(2); //初始化0???AW9523B
    drvInitAW9523B();    //初始化AW9523B芯片
    drvSetLEDUpdata(0);
    drvSetLEDUpdata(2);
#endif	//_B_J4_ON == 1
    screenDisplayInverOff();//
    screenTurn(0);//
    screenAllRefresh();
    screenFillRam(0);
    drawString(16, 0, 0,(uint8_t *)"ADC0.1>", 0);
    drawString(16, 0,32,(uint8_t *)"ADC0.2>", 0);
    while(1==1)
    {
        ADC_Start(ADC0);
        ADC0->CH[1].STAT |= 0x01;
        adcVal1 = ADC_Read(ADC0, ADC_CH1);
        adcVal2 = ADC_Read(ADC0, ADC_CH2);
        drawHex4B(16,64, 0,adcVal1,0);
        drawUint16(16,64,16,adcVal2,0);
        drawHex4B(16,64,32,adcVal1,0);
        drawUint16(16,64,48,adcVal2,0);
//		printf("%d,%d,",adcVal1,adcVal2);
        delayMs(50);
    }
}

//void DMA_Handler(void)
//{
//	uint32_t i;
//
//	if(DMA_CH_INTStat(DMA_CH0, DMA_IT_DONE))
//	{
//		DMA_CH_INTClr(DMA_CH0, DMA_IT_DONE);
//		ADC_Stop(ADC0);
//
//		for(i = 0; i < ADC_SIZE; i++)
//		{
//			if(((ADC_Result[i] & ADC_FIFDR_NUM_Msk) >> ADC_FIFDR_NUM_Pos) == 0)			// 通道0
//			{
//				printf("%4d,", ADC_Result[i] & ADC_FIFDR_VAL_Msk);
//			}
//		}
//
//		DMA_CH_Open(DMA_CH0);	// 重新开始，再次搬运
//		ADC_Start(ADC0);
//	}
//}

#if USE_GPIOA4_IRQ

void GPIOC5_Handler(void)
{
    EXTI_Clear(GPIOC, PIN5);
}

#else

void GPIOC_Handler(void)
{
    if(EXTI_State(GPIOC, PIN5))
    {
        InvDrvP(pCOut[5]);
    }
}

#endif

