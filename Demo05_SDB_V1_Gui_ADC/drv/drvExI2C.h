/*  杭州六堇科技有限公司
 *  2023-04-19
 *  drvExI2C
 *  By Dawoo.Zheng
 *
 *  Synwit MCU
 */

#include "SWM190.h"
#include "drvMcu.h"

#ifndef _DRV_EXI2C_H_
#define _DRV_EXI2C_H_

void I2C_Master_Init(uint8_t setFlag,uint32_t speed,I2C_TypeDef * i2c_x);
void I2C_Slave_Init(uint8_t setFlag,uint16_t slvAddr,I2C_TypeDef * i2c_x);

uint8_t I2C_Master_Transmit(I2C_TypeDef * i2c_x, uint8_t mAddr,uint8_t * dat, uint8_t datLen);
uint8_t I2C_Master_ReceiveByte(I2C_TypeDef * i2c_x, uint8_t mAddr,uint8_t * dat, uint8_t datLen);
uint8_t I2C_Master_Receive(I2C_TypeDef * i2c_x, uint8_t mAddr,uint8_t * dat, uint8_t datLen);

#endif //_DRV_EXI2C_H_

