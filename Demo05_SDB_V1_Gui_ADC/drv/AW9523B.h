/**
  ******************************************************************************
  * File Name          : AW9523B.h
  * Description        : 主模块数字 IO
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */

#ifndef _AW9523B_H_
#define _AW9523B_H_

/* ------------- 声明头文件 ------------------------------------------------- */
#include "SWM190.h"
#include "SWM190_i2c.h"
#include "drvExI2C.h"

/* ------------- 定义全局宏 ------------------------------------------------- */

#define Addr9523_M0      0xB0       //Addr. 1011,09-ad1-ad0-R1/W0
#define Addr9523_M1      0xB2       //Addr. 1011,09-ad1-ad0-R1/W0
#define Addr9523_M2      0xB4       //Addr. 1011,09-ad1-ad0-R1/W0
#define Addr9523_M3      0xB6       //Addr. 1011,09-ad1-ad0-R1/W0

#define ReadPort0        0x00       //P0 口输入状态
#define ReadPort1        0x01       //P1 口输入状态
#define RW_Port0_Out     0x02       //P0 口输出状态
#define RW_Port1_Out     0x03       //P1 口输出状态
#define RW_Port0Conf     0x04       //P0 口输入或输出配置
#define RW_Port1Conf     0x05       //P1 口输入或输出配置
#define RW_Port0IntEn    0x06       //P0 口中断使能
#define RW_Port1IntEn    0x07       //P1 口中断使能

#define ReadID_Reg       0x10       //ID 寄存器（只读）
#define RW_CtrlReg       0x11       //全局控制寄存器
#define RW_LEDModeSw0    0x12       //P0_7~P0_0 工作模式切换
#define RW_LEDModeSw1    0x13       //P1_7~P1_0 工作模式切换

#define WriteP1Dim00_I    0x20      //P1_0 口LED 电流控制
#define WriteP1Dim01_I    0x21      //P1_1 口LED 电流控制
#define WriteP1Dim02_I    0x22      //P1_2 口LED 电流控制
#define WriteP1Dim03_I    0x23      //P1_3 口LED 电流控制
#define WriteP1Dim04_I    0x24      //P0_0 口LED 电流控制
#define WriteP1Dim05_I    0x25      //P0_1 口LED 电流控制
#define WriteP1Dim06_I    0x26      //P0_2 口LED 电流控制
#define WriteP1Dim07_I    0x27      //P0_3 口LED 电流控制
#define WriteP1Dim08_I    0x28      //P0_4 口LED 电流控制
#define WriteP1Dim09_I    0x29      //P0_5 口LED 电流控制
#define WriteP1Dim10_I    0x2A      //P0_6 口LED 电流控制
#define WriteP1Dim11_I    0x2B      //P0_7 口LED 电流控制
#define WriteP1Dim12_I    0x2C      //P1_4 口LED 电流控制
#define WriteP1Dim13_I    0x2D      //P1_5 口LED 电流控制
#define WriteP1Dim14_I    0x2E      //P1_6 口LED 电流控制
#define WriteP1Dim15_I    0x2F      //P1_7 口LED 电流控制

#define WriteSW_RSTN      0x7F      //软件复位控制

//#define CTRL_DEF_VAL        0x12    //Set Push-Pull and Imax/2
typedef struct __SV_LED_P_mA
{
    uint8_t ledRedIC;
    uint8_t ledRedmA;
    uint8_t ledGreenIC;
    uint8_t ledGreenmA;
    uint8_t ledBlueIC;
    uint8_t ledBluemA;
} SV_LED;
extern const SV_LED svLED[];
extern uint8_t awLEDPmA[][16];

/* ------------- 定义全局类型 ----------------------------------------------- */
extern uint8_t i2cCmdList[18];
//extern uint8_t *awLEDPmA;

/* ------------- 声明全局函数 ----------------------------------------------- */
extern uint8_t drvInitAW9523B(void);
extern uint8_t drvSoftReset9523(uint8_t indexM);                 //AW9523B->软复位功能
extern uint8_t drvSet9523REG1(uint8_t indexM,uint8_t comReg, uint8_t Dat8b);
extern uint8_t drvSet9523REG2(uint8_t indexM,uint8_t comReg, uint16_t Dat16b);    //0x1234
extern uint8_t drvSet9523LedMa(uint8_t indexM,uint8_t * pData);   //AW9523B->写调光电流寄存器
extern uint8_t drvSetLEDUpdata(uint8_t index);
extern uint16_t drvRead9523Reg(uint8_t indexM);                            //AW9523B->读状态寄存器
extern uint8_t drvRead9523RegN(uint8_t indexM, uint8_t RegAddr, uint8_t n);   //0x10
extern uint8_t drvRead9523_ID(uint8_t indexM, uint8_t RegAddr);  //AW9523B->读指定寄存器
extern void drvinitDI(void);
extern void drvinitDO(void);

extern uint8_t drvReadDI_xBit( uint8_t bitS);
extern void drvSet0_DO_xBit(uint8_t sc);
extern void drvSet1_DO_xBit(uint8_t sc);
extern void drvSetDO_xBit(uint8_t sc, uint8_t bitS);

#endif /* _AW9523B_H_ */

