/* www.6violet.com
 * FileName displayConfig.h
 * creat time: 2023-12-07 by dawoo.Zheng
 *    The last modifier: dawoo.Zheng
 * The last change time: 2023-12-07
 * commit:
 * ------------------------------------------------------------------------------------
 * 需要在devMCU.h中定义的功能：（按需求）
 * extern SPI_HandleTypeDef lcdSpiHandle;   //对应的硬件接口
 * ------------------------------------------------------------------------------------
 * 需要在LCD_OLED_IC.h对应的文件实现的驱动功能：
 * void screenWriteByteData(uint8_t data);                 //屏幕写 8bit数据
 * void screenWriteWordData(uint16_t data);                //屏幕写16bit数据
 * void screenWriteDoubleData(uint32_t data);              //屏幕写32bit数据
 * void screenWrite_NByteData(unsigned char * data,uint16_t number);      //屏幕写X Byte数据
 * void screenWriteByteCMD(uint8_t data);                  //屏幕写 8bit命令
 * void screenWriteWordCMD(uint16_t data);                 //屏幕写16bit命令
 * void screenSetViewArea(SV_COORD x1,SV_COORD y1,SV_COORD x2,SV_COORD y2);    //设置坐标函数
 * void screenSetPosition(SV_COORD x1,SV_COORD y1);        //设置坐标函数
 * void screenDrvPixel(SV_COORD x,SV_COORD y,SV_COLOR color);     //画一个点
 * void screenDrvBlock(SV_COORD x,SV_COORD y,uint16_t w,uint16_t h,SV_COLOR color);    //画色块
 * void screenDrvInit(void);                               //初始化屏幕
 * void screenOff(void);                                   //关闭屏幕或背光
 * void screenON(void);                                    //打开屏幕或背光
 *
 * ///>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 * void screenDrvNop(void);                                //空指令
 * void screenDrvSleep(uint8_t state);                     //=1进入睡眠//=0退出睡眠
 * void screenDrvBrightnes(uint8_t val);
 * void screenDrvInversion(uint8_t state);                 //=1进入反色//=0退出反色
 * void screenDrvDisplay(uint8_t state);                   //=1进入反色//=0退出反色
 * void screenDrvSoftReset(void);
 *
 * uint8_t screenDrvReadByte(void);
 * uint16_t screenDrvReadWord(void);
 *
 * uint8_t screenDrvGetManu(void);                         //读取显示屏制造商
 * uint8_t screenDrvGetVersion(void);                      //读取显示屏版本号
 * uint8_t screenDrvGetId(void);                           //读取显示屏版本号
 * ------------------------------------------------------------------------------------
 */

#ifndef __DISPLAYCONFIG__H
#define __DISPLAYCONFIG__H

#include "stdint.h"
#include "SWM190.h"
#include "drvGpio.h"
//#include "drvMcu.h"             //与硬件MCU,板子相关的所有资源
#include "displayRes.h"

///设置自己的屏座标类型
typedef int16_t SV_COORD;
///设置自己的颜色类型
typedef uint8_t SV_COLOR;      // uint32_t(if 18bitColor or 24BitColor then)
#include "drvSSD1315.h"	//LCD_OLED_IC.h"        //这个文件名要替换成显示屏对应的IC驱动文件

//设置彩色数据字节数           16b|  18b|  24b|
#define SV_COLOR_BYTE_NUM       2//   3//   3//
//设置彩色的位数              16b|  18b|  24b|
#define SV_COLOR_NUM          1 // 18 // 24 //  
//设置RGB的有效位数           16b|  18b|  24b|
#define SV_COLOR_R_BIT_       3 //  2 //  0 //Red   = 8-5 = 3
#define SV_COLOR_G_BIT_       2 //  2 //  0 //Green = 8-6 = 2
#define SV_COLOR_B_BIT_       3 //  2 //  0 //Blue  = 8-5 = 3
//设置RGB对应的有效色起始位   16b|  18b|  24b|
#define SV_COLOR_R_BEBIT_    11 // 12 // 16 //Red
#define SV_COLOR_G_BEBIT_     5 //  6 //  8 //Green
#define SV_COLOR_B_BEBIT_     0 //  0 //  0 //Blue

extern SV_COORD centreX;
extern SV_COORD centreY;

#if (SV_COLOR_NUM != 1)     //不是单色的情况下要处理多色的功能
extern SV_COLOR reRGB(SV_COLOR color);
#define SV_GET_R_COL(col) (SV_COLOR)((col>>(16+SV_COLOR_R_BIT_))<<SV_COLOR_R_BEBIT_)
#define SV_GET_G_COL(col) (SV_COLOR)((col>>(8+SV_COLOR_G_BIT_))<<SV_COLOR_G_BEBIT_)
#define SV_GET_B_COL(col) (SV_COLOR)((col>>(SV_COLOR_B_BIT_))<<SV_COLOR_B_BEBIT_)
#define SV_RGB(col) (SV_COLOR)(SV_GET_R_COL(col)+SV_GET_G_COL(col)+SV_GET_B_COL(col))
//use example:
//reten color: (SV_COLOR)SV_RGB(SV_Yellow);
#endif

typedef struct
{
    SV_COLOR bkColorVal;    //设置屏的背景色
    SV_COLOR fgColorVal;    //设置屏的前景色
    uint8_t screenModeX;
    uint8_t screenModeY;
} screenSys;

//
#define SV_SCREEN_HV_MODE   2   //设置横屏或者竖屏显示 0或1为竖屏 2或3为横屏
/// # drv 底层显示驱动芯片相关的设置
#define SCREEN_USE_SPI      1   //如果使用SPI接口 == 1
#define SCREEN_USE_SGPIO    0   //如果使用GPIO模拟串行接口 == 1
#define SCREEN_USE_PGPIO    0   //如果使用GPIO并行接口屏 == 1
#define SCREEN_HARD_BLK     0   //如果屏有硬件背光控制
#define SCREEN_HARDRESET    0   //如果屏有硬件复位
#define SCREEN_HAVE_OFFS    0   //如果屏有偏移量

///Memery Set
///取屏数据缓存大小，一行屏数据大。取大值
#define SV_SCREEN_MAX_SIDE (SV_SCREEN_W>SV_SCREEN_H?SV_SCREEN_W:SV_SCREEN_H)
#define SV_FONT_RAM_BUF_  (SV_SCREEN_MAX_SIDE*2)    //48*48*2=4608

//如果使用SinCos的查表功能， USE_SV_SIN_COS_TABLE_ == 1
//在画扇形之类时要使用。
#define USE_SV_SIN_COS_TABLE_ 0

////如果使用汉字与ASCII的查表功能， USE_SV_FONTLIB_TABLE_ == 1
////在显示汉字与ASCII字符时要使用。
#define USE_SV_FONTLIB_TABLE_ 1

//如果使用图的查表功能， USE_SV_PICDATA_TABLE_ == 1
//在显示图标与图片时要使用。
#define USE_SV_PICDATA_TABLE_ 1

//Screen Size Setup        OLED|1.47P|1.50P|1.14P|1.06P|
#define SV_SCREEN_W        128// 320// 240// 240// 160//
#define SV_SCREEN_H         64// 172// 240// 135//  96//
//                        1.77P|1.47P|1.50P|1.14P|1.06P|
#if (SCREEN_HAVE_OFFS == 1)
#define LCD_XOFFSET_P        0//   0//   0//   0//    //
#define LCD_YOFFSET_P        0//  34//   0//  16//    //
#endif
//
//定义GPIO的对应关系：
extern const _SV_SGpio lcdDCport ;
extern const _SV_SGpio lcdCSport ;

#if (SCREEN_HARD_BLK == 1)   //如果屏有硬件背光控制
extern const _SV_SGpio lcdBLKport;
#define screenBLKClr()      (ClrDrvP(lcdBK))		    //
#define screenBLKSet()      (SetDrvP(lcdBK))		    //
#endif //SCREEN_HARD_BLK
//
#if (SCREEN_HARDRESET == 1)   //如果屏有硬件复位
extern const _SV_SGpio lcdRESport;
#define screenRESClr()      (ClrDrvP(lcdRESport))		    //PC2
#define screenRESSet()      (SetDrvP(lcdRESport))		    //PC2
#endif //SCREEN_HARDRESET 

#define screenDCClr()       (ClrDrvP(lcdDCport ))		    //
#define screenDCSet()       (SetDrvP(lcdDCport ))		    //
#define screenCSClr()       (ClrDrvP(lcdCSport ))		    //
#define screenCSSet()       (SetDrvP(lcdCSport ))		    //

#if (SCREEN_USE_SPI == 1)       //如果使用SPI接口
//#include "drvSpi.h"
//extern SPI_TypeDef lcdSpiHandle;
#define SV_SPI_PORT SPI0
#define SV_SPI_Transmit1Byte(a) (SPI_WriteWithWait(SV_SPI_PORT,*(uint8_t *)a))    //a type is uint8_t *
#define SV_SPI_TransmitNByte(a,b) (Spi_Send_N_Data(SV_SPI_PORT,(uint8_t *)a,b))    //b is Number
//#define SV_SPI_TransmitHAL(a,b,c,d) (HAL_SPI_Transmit(a,b,c,d);
#endif  //SCREEN_USE_SPI ==1

#if (SCREEN_USE_SGPIO == 1)     //如果使用GPIO模拟串行接口
//const _SV_SGpio lcdSClkport= {GPIOB, PIN6};	        //PB6
//const _SV_SGpio lcdMosiport= {GPIOB, PIN5};	        //PB5
#define screenSclkClr()    (ClrDrvP(lcdSClkport))		    //PB6
#define screenSclkSet()    (SetDrvP(lcdSClkport))		    //PB6
#define screenMosiClr()    (ClrDrvP(lcdMosiport))		    //PB5
#define screenMosiSet()    (SetDrvP(lcdMosiport))		    //PB5
#endif  //SCREEN_USE_SGPIO ==1

#if (SCREEN_USE_PGPIO == 1)     //如果使用GPIO并行接口屏
extern const _SV_SGpio lcdWRport ;
extern const _SV_SGpio lcdRDport ;
#define screenWRClr()  (ClrDrvP(lcdWRport ))		    //PD0
#define screenWRSet()  (SetDrvP(lcdWRport ))		    //PD0
#define screenRDClr()  (ClrDrvP(lcdRDport ))		    //PD1
#define screenRDSet()  (SetDrvP(lcdRDport ))		    //PD1

#define screenP16DirWrite()      (GPIOAB->DIR |= 0x0000FFFF) //管脚方向设为写
#define screenP16DirRead()       (GPIOAB->DIR &= 0xFFFF0000) //管脚方向设为读
#define screenWriteP16Bit(data)  ((GPIOAB->ODATA &= 0x0000FFFF),(GPIOAB->ODATA |=(uint32_t)(data<<16)))  //并口写16Bit数据
//#define screenWriteH8Bit(data)   ((GPIOAB->ODATA |= (uint32_t)(data<<16)),\
//                                    GPIOAB->ODATA &= (0xFFFFFF00+(~(uint32_t)(data<<24))))  //并口写8Bit数据
//#define screenWriteL8Bit(data)   ((GPIOAB->ODATA |= (uint32_t)(data<<16)),\
//                                    GPIOAB->ODATA &= (0xFFFF00FF+(~(uint32_t)(data<<24))))  //并口写8Bit数据
#endif  //SCREEN_USE_PGPIO ==1


#endif          ///__DISPLAYCONFIG__H

