astyle -A1 -S app\*.c
astyle -A1 -S app\*.h
astyle -A1 -S drv\*.c
astyle -A1 -S drv\*.h
astyle -A1 -S CSL\CMSIS\CoreSupport\*.h
astyle -A1 -S CSL\CMSIS\DeviceSupport\*.c
astyle -A1 -S CSL\CMSIS\DeviceSupport\*.h
astyle -A1 -S CSL\SWM190_StdPeriph_Driver\*.c
astyle -A1 -S CSL\SWM190_StdPeriph_Driver\*.h
astyle -A1 -S res\*.h
del *.bak /s
del *~ /s
del *.orig /s
del out\*.o /s
del out\*.d /s
del out\*.crf /s
del out\*._2i /s
ctags -R .

