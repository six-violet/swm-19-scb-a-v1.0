/* www.6violet.com
 * FileName drvMcu.c
 * creat time: 2023-8-27 by dawoo.Zheng
 *    The last modifier: dawoo.Zheng
 * The last change time: 2023-8-27
 * commit:
 */

#include "drvMcu.h"
//#include "AW9523B.h"

//#include "drvGpio.h"
//#include "drvGC9A01.h"
//#include "drvTimEncoder.h"
//#include "drvIWDT.h"

#define TIM_CLOCK_FREQ (10000)
#define MAXCRCBUF 256
#define _MAX_ENCODER_COUNT_ 256

uint8_t crcDataBuf[MAXCRCBUF];
//TIM_HandleTypeDef tim6Handler;
//UART_HandleTypeDef uart1Handle;
//UART_HandleTypeDef uart2Handle;
//UART_HandleTypeDef uart3Handle;
//UART_HandleTypeDef uart4Handle;
volatile uint32_t mKeyPressFlag;

volatile uint32_t lastCountTIM;

volatile uint8_t nowKeyType;
//volatile uint8_t flagKEY;
volatile _RUNKEYFLAG mRunKeyFlag;
//volatile _ENCODER_FLAG mEncoderFlag;
volatile uint8_t pKeyTime,rKeyTime;

//TIM_HandleTypeDef	timxHandler;
//TIM_Encoder_InitTypeDef	timxEncoderInitHandler;

typedef struct
{
    uint8_t flagNMs1:1;       //
    uint8_t flagNMs2:1;       //
    uint8_t flagNMs3:1;       //
    uint8_t flagNMs4:1;       //
    uint8_t flagNMs5:1;       //
    uint8_t flagNMs6:1;       //
    uint8_t flagNMs7:1;       //
    uint8_t flagNMs8:1;       //
} _FLAG_NMs ;
volatile _FLAG_NMs mFlagNm;
volatile uint32_t _5MsCount;

/// void uartInit(UART_TypeDef * uartName,uint32_t fu32Baudrate, uint32_t fu32Mode)
void SerialInit(void)
{
    uint16_t i,j;
    UART_InitStructure UART_initStruct;

    PORT_Init(PORTB, PIN8, PORTB_PIN8_UART2_TX, 0);		//GPIOB.8->>UART2 TXD
    PORT_Init(PORTB, PIN7, PORTB_PIN7_UART2_RX, 1);		//GPIOB.7->>UART2 RXD
    UART_initStruct.Baudrate = 115200;
    UART_initStruct.DataBits = UART_DATA_8BIT;
    UART_initStruct.Parity = UART_PARITY_NONE;
    UART_initStruct.StopBits = UART_STOP_1BIT;
    UART_initStruct.RXThresholdIEn = 0;
    UART_initStruct.TXThresholdIEn = 0;
    UART_initStruct.TimeoutIEn = 0;
    UART_Init(UART2, &UART_initStruct);
    UART_Open(UART2);

    for(i=0; i<50; i++)
        for(j=0; j<1400; j++)
            ;

    PORT_Init(PORTD, PIN1, PORTD_PIN1_UART0_TX, 0);		//GPIOD.1->>UART0 TXD
    PORT_Init(PORTD, PIN0, PORTD_PIN0_UART0_RX, 1);		//GPIOD.0->>UART0 RXD
    UART_initStruct.Baudrate = 115200;
    UART_initStruct.DataBits = UART_DATA_8BIT;
    UART_initStruct.Parity = UART_PARITY_NONE;
    UART_initStruct.StopBits = UART_STOP_1BIT;
    UART_initStruct.RXThresholdIEn = 0;
    UART_initStruct.TXThresholdIEn = 0;
    UART_initStruct.TimeoutIEn = 0;
    UART_Init(UART0, &UART_initStruct);
    UART_Open(UART0);
}

void I2CMstInit(void)
{
    I2C_InitStructure I2C_initStruct;

    PORT_Init(PORTB, PIN15, PORTB_PIN15_I2C0_SCL, 1);	//GPIOB.15配置为I2C0 SCL引脚
    PORTB->PULLU |= (1 << PIN15);					    //必须使能上拉，用于模拟开漏
    PORT_Init(PORTB, PIN14, PORTB_PIN14_I2C0_SDA, 1);	//GPIOB.14配置为I2C0 SDA引脚
    PORTB->PULLU |= (1 << PIN14);					    //必须使能上拉，用于模拟开漏

    I2C_initStruct.Master = 1;
    I2C_initStruct.Addr7b = 1;
    I2C_initStruct.MstClk = 10000;
    I2C_initStruct.MstIEn = 0;
    I2C_Init(I2C0, &I2C_initStruct);

    I2C_Open(I2C0);
//	I2C_Master_Init(0, 100000, I2C0);
}

void printZ(UART_TypeDef * UARTx, char * str)
{
    unsigned long int sl = 0L;
    while(sl < 3000)
    {
        while(UART_IsTXBusy(UARTx));
        if(*str != 0)
            UART_WriteByte(UARTx, *str);
        else
            sl = 3000;
        str++;
    }
}

void mcuInit(void)
{
    InitAllPin();
    SerialInit();
    I2CMstInit();
}

//Nms Flag:
uint8_t getNMsEvent(uint8_t index)
{
    uint8_t reVal;
    switch(index)
    {
        case 0:
            reVal = mFlagNm.flagNMs1;
            break;
        case 1:
            reVal = mFlagNm.flagNMs2;
            break;
        case 2:
            reVal = mFlagNm.flagNMs3;
            break;
        case 3:
            reVal = mFlagNm.flagNMs4;
            break;
        case 4:
            reVal = mFlagNm.flagNMs5;
            break;
        case 5:
            reVal = mFlagNm.flagNMs6;
            break;
        case 6:
            reVal = mFlagNm.flagNMs7;
            break;
        case 7:
            reVal = mFlagNm.flagNMs8;
            break;
        default:
            reVal = 0;
            break;
    }
    return reVal;
}

void clrNMsEvent(uint8_t index)
{
    switch(index)
    {
        case 0:
            mFlagNm.flagNMs1 = 0;
            break;
        case 1:
            mFlagNm.flagNMs2 = 0;
            break;
        case 2:
            mFlagNm.flagNMs3 = 0;
            break;
        case 3:
            mFlagNm.flagNMs4 = 0;
            break;
        case 4:
            mFlagNm.flagNMs5 = 0;
            break;
        case 5:
            mFlagNm.flagNMs6 = 0;
            break;
        case 6:
            mFlagNm.flagNMs7 = 0;
            break;
        case 7:
            mFlagNm.flagNMs8 = 0;
            break;
        default:
            break;
    }
}

/************************************************************************
 * function   : uartInit
 * Description: Uart Initiation.
 ************************************************************************/
//void uartInit(UART_TypeDef * uartName,uint32_t fu32Baudrate, uint32_t fu32Mode)
//{
//    uint8_t uartPort =0;

//    UART_HandleTypeDef uartXHandle;
//    uartXHandle.Instance = uartName;   //UART2;
//    uartXHandle.Init.BaudRate   = fu32Baudrate;
//    uartXHandle.Init.WordLength = UART_WORDLENGTH_8B;
//    uartXHandle.Init.StopBits   = UART_STOPBITS_1;
//    uartXHandle.Init.Parity     = UART_PARITY_NONE;
//    uartXHandle.Init.Mode       = fu32Mode;
//    uartXHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;

//    HAL_UART_Init(&uartXHandle);
//    if(uartName == UART1)
//    {
//        uartPort = 1;
//    }
//    else if(uartName == UART2)
//    {
//        uartPort = 2;
//    }
//    else if(uartName == UART3)
//    {
//        uartPort = 3;
//    }
//    else if(uartName == UART4)
//    {
//        uartPort = 4;
//    }
//    /*
//        if(uartName == UART1)
//        {
//            uart1Handle.Instance = uartXHandle.Instance;
//            uart1Handle.Init.BaudRate = uartXHandle.Init.BaudRate;
//            uart1Handle.Init.WordLength = uartXHandle.Init.WordLength;
//            uart1Handle.Init.StopBits = uartXHandle.Init.StopBits;
//            uart1Handle.Init.Parity = uartXHandle.Init.Parity;
//            uart1Handle.Init.Mode = uartXHandle.Init.Mode;
//            uart1Handle.Init.HwFlowCtl = uartXHandle.Init.HwFlowCtl;
//        }
//        if(uartName == UART2)
//        {
//            uart2Handle.Instance = uartXHandle.Instance;
//            uart2Handle.Init.BaudRate = uartXHandle.Init.BaudRate;
//            uart2Handle.Init.WordLength = uartXHandle.Init.WordLength;
//            uart2Handle.Init.StopBits = uartXHandle.Init.StopBits;
//            uart2Handle.Init.Parity = uartXHandle.Init.Parity;
//            uart2Handle.Init.Mode = uartXHandle.Init.Mode;
//            uart2Handle.Init.HwFlowCtl = uartXHandle.Init.HwFlowCtl;
//            str0[4]='2';
//        }
//        if(uartName == UART3)
//        {
//            uart3Handle.Instance = uartXHandle.Instance;
//            uart3Handle.Init.BaudRate = uartXHandle.Init.BaudRate;
//            uart3Handle.Init.WordLength = uartXHandle.Init.WordLength;
//            uart3Handle.Init.StopBits = uartXHandle.Init.StopBits;
//            uart3Handle.Init.Parity = uartXHandle.Init.Parity;
//            uart3Handle.Init.Mode = uartXHandle.Init.Mode;
//            uart3Handle.Init.HwFlowCtl = uartXHandle.Init.HwFlowCtl;
//            str0[4]='3';
//        }
//        if(uartName == UART4)
//        {
//            uart4Handle.Instance = uartXHandle.Instance;
//            uart4Handle.Init.BaudRate = uartXHandle.Init.BaudRate;
//            uart4Handle.Init.WordLength = uartXHandle.Init.WordLength;
//            uart4Handle.Init.StopBits = uartXHandle.Init.StopBits;
//            uart4Handle.Init.Parity = uartXHandle.Init.Parity;
//            uart4Handle.Init.Mode = uartXHandle.Init.Mode;
//            uart4Handle.Init.HwFlowCtl = uartXHandle.Init.HwFlowCtl;
//            str0[4]='4';
//        }
//    	*/

//    /* UART_DEBUG_ENABLE control printfS */
////    printfS("MCU is running, HCLK=%dHz, PCLK=%dHz\n", System_Get_SystemClock(), System_Get_APBClock());
//    printfS("init Uart %d , baud rate: %d bps! \n",uartPort,fu32Baudrate);
//}

/// Ex: delayMs(100);
void delayMs(volatile uint32_t u32DelayMsNum)
{
//    System_Delay_MS(u32DelayMsNum);
}

void delayUs(volatile uint32_t u32DelayNum)
{
//    System_Delay(u32DelayNum);
}

void crcDataInit(void)
{
    uint32_t i;
    for(i = 0; i < MAXCRCBUF-2; i++ )
    {
        crcDataBuf[i] = i;
    }
}

uint16_t strSizeC(uint8_t *strBuf)
{
    uint8_t * str;
    uint16_t count = 0;
    str = strBuf;
    while(count < 65000)
    {
        if(*str++ == 0)
            return (uint16_t)(str-strBuf-1);
    }
    return 0;
}

//uint32_t crcCount(uint8_t * crcBuf)
//{
//    uint32_t resultCRC;
//    uint32_t bytesLen;

//    CRC_HandleTypeDef handleCRC;

////	crcDataInit();
//    bytesLen = strSizeC(crcBuf);
//    if(bytesLen == 0)
//        return 0;
//    if(bytesLen > MAXCRCBUF)
//        return 0;

//    handleCRC.Instance = CRC;
//    handleCRC.Init.PolyRev = CRC_POLY_REV_EN;
//    handleCRC.Init.OutxorRev = CRC_OUTXOR_REV_EN;
//    handleCRC.Init.InitRev = CRC_INIT_REV_EN;
//    handleCRC.Init.RsltRev = CRC_RSLT_REV_EN;
//    handleCRC.Init.DataRev = CRC_DATA_REV_BY_BYTE;
//    handleCRC.Init.PolyLen = CRC_POLTY_LEN_16;
//    handleCRC.Init.DataLen = CRC_DATA_LEN_1B;

//    handleCRC.Init.PolyData = 0x8005;  //poly = x16+x15+x2+1
//    handleCRC.Init.InitData = 0xFFFF;
//    handleCRC.Init.OutXorData = 0x0000;

//    handleCRC.CRC_Data_Buff = crcBuf;
//    handleCRC.CRC_Data_Len = bytesLen;

//    resultCRC = HAL_CRC_Calculate(&handleCRC);
//    printf("resultCRC = 0x%x\n", resultCRC);
//    return resultCRC;
//}

/************************************************************************
 * function   : TIM6_IRQHandler
 * Description: TIM6 Interrupt Handler
 ************************************************************************/
//void keyInputInit(void)
//{
//    //Set               PA5 KeyInput
//    gpioInitPort(GPIOA, GPIO_PIN_5,GPIO_MODE_INPUT, GPIO_PULLUP, GPIO_FUNCTION_0);

//    nowKeyType = EVENT_NONE_KEY;
//    mRunKeyFlag.runKeyFlag = 0;
//    mRunKeyFlag.lastKeyVal = _KEYOFF;
//    mRunKeyFlag.runKeyMode = 0;
//    mRunKeyFlag.kOnTime = 0;
//    mRunKeyFlag.kOffTime = 0;
//}


//void Timer_encoder_Init(uint32_t timer_clk, uint32_t duration)
//{
//    uint32_t sys_fclk;

//    //Set               EnCoder A    EnCoder B
//    gpioInitPort(GPIOA, GPIO_PIN_6 | GPIO_PIN_7, GPIO_MODE_AF_PP, GPIO_PULLUP, GPIO_FUNCTION_2);     //Set EnCoder PA6,PA7
//    sys_fclk = System_Get_APBClock();
//    if((SCU->CCR2 & BIT10))
//        sys_fclk *= 2;

//    timxHandler.Instance				= TIM3;
//    timxHandler.Init.ARRPreLoadEn		= TIM_ARR_PRELOAD_ENABLE;
//    timxHandler.Init.ClockDivision		= TIM_CLOCKDIVISION_DIV1;
//    timxHandler.Init.CounterMode		= TIM_COUNTERMODE_UP;
//    timxHandler.Init.Period				= (duration - 1);
//    timxHandler.Init.Prescaler			= (sys_fclk / timer_clk - 1);
//    timxHandler.Init.RepetitionCounter	= 0;

//    timxEncoderInitHandler.EncoderMode = TIM_SLAVE_MODE_ENC3;

//    timxEncoderInitHandler.ic1.ICPolarity = TIM_CC1_SLAVE_CAPTURE_POL_RISING;
//    timxEncoderInitHandler.ic1.ICPrescaler = TIM_IC1_PRESCALER_1;
//    timxEncoderInitHandler.ic1.ICSelection = TIM_ICSELECTION_DIRECTTI;
//    timxEncoderInitHandler.ic1.TIFilter = 8;

//    timxEncoderInitHandler.ic2.ICPolarity = TIM_CC2_SLAVE_CAPTURE_POL_RISING;
//    timxEncoderInitHandler.ic2.ICPrescaler = TIM_IC2_PRESCALER_1;
//    timxEncoderInitHandler.ic2.ICSelection = TIM_ICSELECTION_DIRECTTI;
//    timxEncoderInitHandler.ic2.TIFilter = 8;

//    HAL_TIMER_MSP_Init(&timxHandler);
//    TIM3->SR = 0;
//    NVIC_ClearPendingIRQ(TIM3_IRQn);
//    NVIC_EnableIRQ(TIM3_IRQn);

//    HAL_TIMER_Base_Init(&timxHandler);

//    HAL_TIM_Encoder_Init(&timxHandler, &timxEncoderInitHandler);
//    HAL_TIM_Encoder_Start_IT(&timxHandler, TIM_CHANNEL_1);

//    HAL_TIMER_Base_Start(TIM3);
//}

void encoderInit(void)
{
//    Timer_encoder_Init( 40000000, _MAX_ENCODER_COUNT_);
}

//void TIM3_IRQHandler(void)
//{
//    if(TIM3->SR & TIMER_SR_CC1IF)
//    {
//        TIM3->SR &= ~(TIMER_SR_CC1IF);
//    }
//}

//void TIM6_IRQHandler(void)
//{
//    uint8_t tempValA,tempValB;
//    if (TIM6->SR & TIMER_SR_UIF)
//    {
//        _5MsCount ++;
//        if((_5MsCount % WAIT_TIME_MS_NUM) == 0)
//        {
//            *(uint8_t *)&mFlagNm = 0xff;
//        }
//        // Read Middle Key Val
//        if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_5) == _KEYON)
//        {
//            if(mRunKeyFlag.lastKeyVal == _KEYOFF)   //刚刚按下KEY
//            {
//                mRunKeyFlag.lastKeyVal = _KEYON;
//                if(mRunKeyFlag.runKeyFlag == 0)     //没按键信息，刚刚进入
//                {
//                    mRunKeyFlag.runKeyFlag = 1;     //Run Key;
//                    mRunKeyFlag.runKeyMode = 1;     //click Mode;
//                }
//                else
//                {
//                    if(mRunKeyFlag.runKeyMode == 1)
//                        mRunKeyFlag.runKeyMode = 2;     //doubleClick;
//                }
//                mRunKeyFlag.kOnTime = 0;            //开始计时
//            }
//            else
//            {
//                mRunKeyFlag.kOnTime ++;
//                if(mRunKeyFlag.runKeyMode == 1)
//                {
//                    if(mRunKeyFlag.kOnTime > WAIT_KEY_TIME_A)   // >1.2S
//                    {
//                        mRunKeyFlag.runKeyMode = 3;
//                        nowKeyType = EVENT_KEY_PRESS;    //Get KEY Val == EVENT_KEY_PRESS
////                        flagKEY = EVENT_KEY_PRESS + 0x80;
//                    }
//                }
//            }
//        }
//        else            // KEY == _KENOFF
//        {
//            if(mRunKeyFlag.runKeyFlag != 0)
//            {
//                if(mRunKeyFlag.lastKeyVal == _KEYON)   //刚刚释放KEY
//                {
//                    if(mRunKeyFlag.kOnTime < WAIT_KEY_TIME_C)
//                    {
//                        mRunKeyFlag.runKeyFlag = 0;
//                        mRunKeyFlag.lastKeyVal = _KEYOFF;
////                    mRunKeyFlag.nowKeyVal = _KEYOFF;
//                        mRunKeyFlag.runKeyMode = 0;
//                        mRunKeyFlag.kOnTime = 0;
//                        mRunKeyFlag.kOffTime = 0;
//                    }
//                    else
//                    {
//                        mRunKeyFlag.lastKeyVal = _KEYOFF;
//                        mRunKeyFlag.kOffTime = 0;
//                    }
//                }
//                else
//                {
//                    mRunKeyFlag.kOffTime ++;
//                    if(mRunKeyFlag.runKeyMode == 1)     // click Mode
//                    {
//                        if(mRunKeyFlag.kOffTime > WAIT_KEY_TIME_B)  //>0.6S
//                        {
//                            mRunKeyFlag.runKeyFlag = 0;     //Key Stop;
//                            nowKeyType = EVENT_KEY_CLICK;    //Get KEY Val == EVENT_KEY_CLICK
////                        flagKEY = EVENT_KEY_CLICK + 0x80;
//                        }
//                    }
//                    if(mRunKeyFlag.runKeyMode == 2)     // doubleClick Mode
//                    {
//                        if(mRunKeyFlag.kOffTime > WAIT_KEY_TIME_B)  //>0.6S
//                        {
//                            mRunKeyFlag.runKeyFlag = 0;     //Key Stop;
//                            nowKeyType = EVENT_KEY_DOUBLE_CLICK;    //Get KEY Val == EVENT_KEY_DOUBLE_CLICK
////                        flagKEY = EVENT_KEY_DOUBLE_CLICK + 0x80;
//                        }
//                    }
//                    if(mRunKeyFlag.runKeyMode == 3)     // press Mode
//                    {
//                        if(mRunKeyFlag.kOffTime > WAIT_KEY_TIME_B)  //>0.6S
//                        {
//                            mRunKeyFlag.runKeyFlag = 0;     //Key Stop;
//                            nowKeyType = EVENT_KEY_RELEASE;    //Get KEY Val == EVENT_KEY_RELEASE
////                        flagKEY = EVENT_KEY_RELEASE + 0x80;
//                        }
//                    }
//                }
//            }
//        }
//        /*
//                // Read Encoder Val
//                if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_0) != mEncoderFlag.phaseALastVal)   //phaseA
//                {
//                    tempValA = mEncoderFlag.phaseALastVal;
//                    tempValB = (~tempValA)&0x01;
//                    mEncoderFlag.phaseALastChange = tempValB;
//                    mEncoderFlag.phaseAUseFlag = 1;
//        //            if(mEncoderFlag.nextChangeVal == tempValA)
//                    if(tempValA == 0)
//                    {
//                        if(mEncoderFlag.phaseBLastChange == tempValB && mEncoderFlag.phaseBUseFlag ==1)
//                        {
//                            mEncoderFlag.phaseBUseFlag = 0;
//                            mEncoderFlag.encoderDir = 0;
//                            mEncoderFlag.encoderVal --;
//                            mEncoderFlag.phaseALastChange = tempValA;
//        //                    mEncoderFlag.nextChangeVal = tempValB;
//                        }
//                    }
//                    mEncoderFlag.phaseALastVal = tempValB;
//                }
//                else if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_1) != mEncoderFlag.phaseBLastVal)   //phaseB
//                {
//                    tempValA = mEncoderFlag.phaseBLastVal;
//                    tempValB = (~tempValA)&0x01;
//                    mEncoderFlag.phaseBLastChange = tempValB;
//                    mEncoderFlag.phaseBUseFlag = 1;
//        //            if(mEncoderFlag.nextChangeVal == tempValA)
//                    if(tempValA == 0)
//                    {
//                        if(mEncoderFlag.phaseALastChange == tempValB && mEncoderFlag.phaseAUseFlag ==1)
//                        {
//                            mEncoderFlag.phaseAUseFlag = 0;
//                            mEncoderFlag.encoderDir = 1;
//                            mEncoderFlag.encoderVal ++;
//                            mEncoderFlag.phaseALastChange = tempValA;
//        //                    mEncoderFlag.nextChangeVal = tempValB;
//                        }
//                    }
//                    mEncoderFlag.phaseBLastVal = tempValB;
//                }
//        */
//    }

//    TIM6->SR = 0;   //write 0 to clear hardware flag
//}

//void tim6Init(void)
//{
//    uint32_t timerClock;

//    timerClock = System_Get_APBClock();

//    if (System_Get_SystemClock() != System_Get_APBClock())  // if hclk/pclk != 1, then timer clk = pclk * 2
//    {
//        timerClock =  System_Get_APBClock() << 1;
//    }

//    tim6Handler.Instance = TIM6;
//    tim6Handler.Init.ARRPreLoadEn = TIM_ARR_PRELOAD_ENABLE;
//    tim6Handler.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
//    tim6Handler.Init.CounterMode = TIM_COUNTERMODE_UP;
//    tim6Handler.Init.RepetitionCounter = 0;
//    tim6Handler.Init.Prescaler = (timerClock/TIM_CLOCK_FREQ) - 1;
//    tim6Handler.Init.Period = (TIM_CLOCK_FREQ/1000)*5 - 1;  // 5ms

////	TIM6_MSP_Pre_Init(&tim6Handler);
//    HAL_TIMER_MSP_Init(&tim6Handler);
//    HAL_TIMER_Base_Init(&tim6Handler);
//    HAL_TIM_ENABLE_IT(&tim6Handler, TIMER_INT_EN_UPD);
////	TIM6_MSP_Post_Init();

//    HAL_TIMER_Base_Start(tim6Handler.Instance);
//}

int fputc(int ch, FILE *f)
{
    UART_WriteByte(UART0, ch);
    while(UART_IsTXBusy(UART0));
    return ch;
}

void BSPInit(void)
{
//    mcuInit();
    InitAllPin();
    SerialInit();
    I2CMstInit();
    //uartInit(UART1, 115200, UART_MODE_TX_RX);
    //uartInit(UART3, 115200, UART_MODE_TX_RX_DEBUG);

//    tim6Init();
    //keyInputInit();
    //encoderInit();
//    OledInit();
}

