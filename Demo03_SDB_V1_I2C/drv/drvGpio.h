/*  杭州六堇科技有限公司
 *  2023-03-27
 *  GPIO Demo
 *  By Dawoo.Zheng
 *
 *  drvGpio.h
 *
 */

#ifndef _DRV_GPIO_H_
#define _DRV_GPIO_H_

#include "SWM190.h"
#include "SWM190_port.h"
#include "SWM190_gpio.h"

typedef struct _GPIO_P_
{
    GPIO_TypeDef * ioPort;
    uint32_t ioPin;
} _SV_SGpio;


extern const _SV_SGpio pAOut[];
extern const _SV_SGpio pBOut[];
extern const _SV_SGpio pCOut[];
extern const _SV_SGpio pDOut[];
extern const _SV_SGpio pEOut[];

void InitAllPin(void);
void SetDrvP(_SV_SGpio dp);
void ClrDrvP(_SV_SGpio dp);
void InvDrvP(_SV_SGpio dp);
uint32_t GetInvP(_SV_SGpio dp);

#endif  //_DRV_GPIO_H_

