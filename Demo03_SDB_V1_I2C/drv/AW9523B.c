/**
  ******************************************************************************
  * File Name          : AW9523B.c
  * Description        : 主模块数字 IO
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */

#ifndef _AW9523B_C_
#define _AW9523B_C_

/* ------------- 声明头文件 ------------------------------------------------- */
#include "AW9523B.h"

/* ------------- 定义局部宏 ------------------------------------------------- */


/* ------------- 定义局部类型 ----------------------------------------------- */
/**
  * @brief  定义主控模块控制寄存器的数据对应
  */
uint16_t nowDIval;
uint16_t nowDOval;
uint8_t i2cCmdList[18];
const uint8_t mode0Addr = 0;
const uint8_t mode1Addr = 1;
#define I2CPART I2C0

uint8_t awLEDPmA[4][16];

const SV_LED svLED[]= {{0, 4,2, 4,0, 0},
    {0, 5,2, 5,0, 1},
    {0, 6,2, 6,0, 2},
    {0, 7,2, 7,0, 3},
    {0, 8,2, 8,0,12},
    {0, 9,2, 9,0,13},
    {0,10,2,10,0,14},
    {0,11,2,11,0,15}
};

void delayIIC(void)
{
    uint16_t i,j;
    for(i=0; i<10; i++)
        for(j=0; j<100; j++)
            ;
}

/* ------------- 定义芯片AW9523B函数 ----------------------------------------------- */
/**
  * @brief  芯片AW9523B->软复位功能
  */
uint8_t drvSoftReset9523(uint8_t indexM) //0x7F
{
    uint8_t reSTD;
    i2cCmdList[0] = WriteSW_RSTN;
    i2cCmdList[1] = 0x00;
//    i2cCmdList[2] = 0x00;
    switch(indexM)
    {
        case 0:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M0, (uint8_t *)i2cCmdList,(2));
            break;
        case 1:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M1, (uint8_t *)i2cCmdList,(2));
            break;
        case 2:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M2, (uint8_t *)i2cCmdList,(2));
            break;
        case 3:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M3, (uint8_t *)i2cCmdList,(2));
            break;
        default:
            reSTD = 99;
            break;
    }
    delayIIC();
    return reSTD;
}

/**
  * @brief  芯片AW9523B->写调光电流寄存器
  */
uint8_t drvSet9523LedMa(uint8_t indexM,uint8_t * pData)   //0x20-0x2F
{
    uint8_t i;
    uint8_t reSTD;
    i2cCmdList[0] = WriteP1Dim00_I;
    for(i=0 ; i<16 ; i++)
        i2cCmdList[i+1] = *pData++;
    switch(indexM)
    {
        case 0:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M0, (uint8_t *)i2cCmdList,(17));
            break;
        case 1:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M1, (uint8_t *)i2cCmdList,(17));
            break;
        case 2:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M2, (uint8_t *)i2cCmdList,(17));
            break;
        case 3:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M3, (uint8_t *)i2cCmdList,(17));
            break;
        default:
            reSTD = 99;
            break;
    }
    delayIIC();
    return reSTD;
}

uint8_t drvSetLEDUpdata(uint8_t index)
{
    if (index >3)
        index = 0;
    return drvSet9523LedMa(index,&awLEDPmA[index][0]);
}

/**
  * @brief  芯片AW9523B->设置所有输出功能
  */
uint8_t drvSet9523REG1(uint8_t indexM,uint8_t comReg, uint8_t Dat8b)    //0x1234
{
    uint8_t reSTD;
    i2cCmdList[0] = comReg;
    i2cCmdList[1] = (uint8_t)(Dat8b);
    switch(indexM)
    {
        case 0:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M0, (uint8_t *)i2cCmdList,(2));
            break;
        case 1:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M1, (uint8_t *)i2cCmdList,(2));
            break;
        case 2:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M2, (uint8_t *)i2cCmdList,(2));
            break;
        case 3:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M3, (uint8_t *)i2cCmdList,(2));
            break;
        default:
            reSTD = 99;
            break;
    }
    return reSTD;
}

/**
  * @brief  芯片AW9523B->设置所有输出功能
  */
uint8_t drvSet9523REG2(uint8_t indexM,uint8_t comReg, uint16_t Dat16b)    //0x1234
{
    uint8_t reSTD;
    i2cCmdList[0] = comReg;
    i2cCmdList[1] = (uint8_t)(Dat16b & 0xFF);
    i2cCmdList[2] = (uint8_t)(Dat16b>>8) & 0xFF;
    switch(indexM)
    {
        case 0:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M0, (uint8_t *)i2cCmdList,(3));
            break;
        case 1:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M1, (uint8_t *)i2cCmdList,(3));
            break;
        case 2:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M2, (uint8_t *)i2cCmdList,(3));
            break;
        case 3:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M3, (uint8_t *)i2cCmdList,(3));
            break;
        default:
            reSTD = 99;
            break;
    }
    return reSTD;
}

/**
  * @brief  芯片AW9523B->读状态寄存器
  */
uint16_t drvRead9523Reg(uint8_t indexM)   //0x00
{
    uint16_t reSTD;
    i2cCmdList[0] = ReadPort0;
    switch(indexM)
    {
        case 0:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M0, (uint8_t *)i2cCmdList,(1));
            reSTD=I2C_Master_Receive(I2CPART, Addr9523_M0, (uint8_t *)i2cCmdList,(2));
            break;
        case 1:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M1, (uint8_t *)i2cCmdList,(1));
            reSTD=I2C_Master_Receive(I2CPART, Addr9523_M1, (uint8_t *)i2cCmdList,(2));
            break;
        case 2:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M2, (uint8_t *)i2cCmdList,(1));
            reSTD=I2C_Master_Receive(I2CPART, Addr9523_M2, (uint8_t *)i2cCmdList,(2));
            break;
        case 3:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M3, (uint8_t *)i2cCmdList,(1));
            reSTD=I2C_Master_Receive(I2CPART, Addr9523_M3, (uint8_t *)i2cCmdList,(2));
            break;
        default:
            reSTD = 0;
            break;
    }
    reSTD = i2cCmdList[0] + (uint16_t)(i2cCmdList[1]<<8);
    return reSTD;
}

/**
  * @brief  芯片AW9523B->读标记寄存器
  */
uint8_t drvRead9523_ID(uint8_t indexM, uint8_t RegAddr)   //0x10
{
    uint8_t reReg=0;
    i2cCmdList[0] = RegAddr;
    switch(indexM)
    {
        case 0:
            reReg = I2C_Master_ReceiveByte(I2CPART, Addr9523_M0, (uint8_t *)i2cCmdList,(1));
//            I2C_Master_ReceiveByte(I2CPART, Addr9523_M0, (uint8_t *)&reReg,(1));
            break;
        case 1:
            reReg = I2C_Master_ReceiveByte(I2CPART, Addr9523_M1, (uint8_t *)i2cCmdList,(1));
//            I2C_Master_Receive(I2CPART, Addr9523_M1, (uint8_t *)&reReg,(1));
            break;
        case 2:
            reReg = I2C_Master_ReceiveByte(I2CPART, Addr9523_M2, (uint8_t *)i2cCmdList,(1));
//            I2C_Master_Receive(I2CPART, Addr9523_M2, (uint8_t *)&reReg,(1));
            break;
        case 3:
            reReg = I2C_Master_ReceiveByte(I2CPART, Addr9523_M3, (uint8_t *)i2cCmdList,(1));
//            I2C_Master_Receive(I2CPART, Addr9523_M3, (uint8_t *)&reReg,(1));
            break;
        default:
            break;
    }
    return reReg;
}

/**
  * @brief  芯片AW9523B->连续读寄存器N个字节
  */
uint8_t drvRead9523RegN(uint8_t indexM, uint8_t RegAddr, uint8_t n)   //0x10
{
    uint8_t reSTD;
    i2cCmdList[0] = RegAddr;
    switch(indexM)
    {
        case 0:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M0, (uint8_t *)i2cCmdList,(1));
            reSTD=I2C_Master_Receive(I2CPART, Addr9523_M0, (uint8_t *)i2cCmdList,(n));
            break;
        case 1:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M1, (uint8_t *)i2cCmdList,(1));
            reSTD=I2C_Master_Receive(I2CPART, Addr9523_M1, (uint8_t *)i2cCmdList,(n));
            break;
        case 2:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M2, (uint8_t *)i2cCmdList,(1));
            reSTD=I2C_Master_Receive(I2CPART, Addr9523_M2, (uint8_t *)i2cCmdList,(n));
            break;
        case 3:
            reSTD=I2C_Master_Transmit(I2CPART, Addr9523_M3, (uint8_t *)i2cCmdList,(1));
            reSTD=I2C_Master_Receive(I2CPART, Addr9523_M3, (uint8_t *)i2cCmdList,(n));
            break;
        default:
            reSTD = 99;
            break;
    }
    return reSTD;
}

/**
  * @brief  芯片AW9523B->芯片初始化。
  */
uint8_t drvInitAW9523B(void)
{
    uint8_t re ;
    re = drvSet9523REG2(0,RW_LEDModeSw0,0x0000);    //0x12-0x13//P0+P1 Set LED Mode
    delayIIC();
    re = drvSet9523REG2(2,RW_LEDModeSw0,0xFF00);    //0x12-0x13//P0+P1 Set LED Mode
    delayIIC();
    re = drvSet9523REG1(0,RW_CtrlReg,0x13);         //0x11//全局控制
    delayIIC();
    re = drvSet9523REG1(2,RW_CtrlReg,0x13);         //全局控制
    delayIIC();
    drvSet9523REG2(0,RW_Port0_Out,0xFFFF);     //0x02-0x03//全部高电平
    drvSet9523REG2(2,RW_Port0_Out,0x00FF);     //Lv
    re = drvSet9523REG2(0,RW_Port0Conf,0xFFFF);     //0x04-0x05//16pin Output
    delayIIC();
    re = drvSet9523REG2(2,RW_Port0Conf,0x00FF);     //16pin Input
    delayIIC();
    re = drvSet9523REG2(0,RW_Port0IntEn,0xFFFF);    //0x06-0x07//中断不使能
    delayIIC();
    re = drvSet9523REG2(2,RW_Port0IntEn,0x00FF);    //中断不使能
    delayIIC();
    return re;
}

/**
  * @brief  0号芯片AW9523->芯片初始化。
  */
void drvinitDI(void)
{
    drvSet9523REG2(0,RW_LEDModeSw0,0xFFFF);    //P0+P1 Set GPIO
    drvSet9523REG2(0,RW_Port0_Out,0x0000);     //Lv
    drvSet9523REG2(0,RW_Port0Conf,0x0000);     //16pin Output
    drvSet9523REG2(0,RW_Port0IntEn,0xFFFF);    //中断不使能
    drvSet9523REG1(0,RW_CtrlReg,0x10);         //全局控制
}

/**
  * @brief  1号芯片AW9523->芯片初始化。
  */
void drvinitDO(void)
{
    drvSet9523REG2(1,RW_LEDModeSw0,0xFFFF);    //P0+P1 Set GPIO
    drvSet9523REG2(1,RW_Port0_Out,0x0000);     //Lv
    drvSet9523REG2(1,RW_Port0Conf,0xFFFF);     //16pin Input
    drvSet9523REG2(1,RW_Port0IntEn,0xFFFF);    //中断不使能
    drvSet9523REG1(1,RW_CtrlReg,0x00);         //全局控制
}

/**
  * @brief  0号芯片AW9523->芯片位操作。
  */
void drvSet0_DO_xBit(uint8_t sc)
{
    uint16_t tmp = (nowDOval&(~(0x0001<<sc)));
    nowDOval = tmp;
    drvSet9523REG2(mode0Addr,RW_Port0_Out,tmp);
}

/**
  * @brief  0号芯片AW9523->芯片位操作。
  */
void drvSet1_DO_xBit(uint8_t sc)
{
    uint16_t tmp = (nowDOval&(~(0x0001<<sc)));
    tmp += (0x0001<<sc);
    nowDOval = tmp;
    drvSet9523REG2(mode0Addr,RW_Port0_Out,tmp);
}

/**
  * @brief  0号芯片AW9523->芯片位操作。
  */
void drvSetDO_xBit(uint8_t sc, uint8_t bitS)
{
    if(bitS == 0)
        drvSet0_DO_xBit(sc);
    else
        drvSet1_DO_xBit(sc);
}

/**
  * @brief  1号芯片AW9523->芯片位操作。
  */
uint8_t drvReadDI_xBit( uint8_t bitS)
{
    uint16_t tmp16=0,tmpA;
    tmpA = 0x0001<<bitS;
    tmp16 = (drvRead9523_ID(mode1Addr, ReadPort1)<<8);
    tmp16 += drvRead9523_ID(mode1Addr, ReadPort0);
    if((tmp16 & tmpA) !=0)
        return 0;
    else
        return 1;
}

#endif // Define _AW9523B_C_

