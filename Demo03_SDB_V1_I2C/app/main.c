/*  杭州六堇科技有限公司
 *  2023-03-27
 *  GPIO Demo
 *  By Dawoo.Zheng
 *
 */

#include "main.h"

const uint32_t colLedList[]=
{
    0x00FFFFFF,
    0x00BFFFFF,
    0x00FFBFFF,
    0x00FFFFBF,
    0x00BFBFFF,
    0x00FFBFBF,
    0x00BFFFBF,
    0x00BFBFBF,
    0x007FFFFF,
    0x00FF7FFF,
    0x00FFFF7F,
    0x007F7FFF,
    0x00FF7F7F,
    0x007FFF7F,
    0x007F7F7F,
    0x003FFFFF,
    0x00FF3FFF,
    0x00FFFF3F,
    0x003F3FFF,
    0x00FF3F3F,
    0x003FFF3F,
    0x003F3F3F,
    0x00BF0F0F,
    0x000FBF0F,
    0x000F0FBF,
    0x00BFBF0F,
    0x000FBFBF,
    0x00BF0FBF,
    0x007F0F0F,
    0x000F7F0F,
    0x000F0F7F,
    0x007F7F0F,
    0x000F7F7F,
    0x007F0F7F,
    0x003F0F0F,
    0x000F3F0F,
    0x000F0F3F,
    0x003F3F0F,
    0x000F3F3F,
    0x003F0F3F,
    0x00000000
};

void delay_ms(uint32_t t)
{
    uint16_t i,j;
    for(i=0; i<t; i++)
        for(j=0; j<1400; j++)
            ;
}

int main(void)
{
    uint16_t i = 0,j=0,k=0;
    SystemInit();

    delay_ms(10);
    BSPInit();

    SetDrvP(pCOut[5]);
    printf("SixViolet Demo App!");
    printZ(UART2, "SixViolet Demo App!");

    for(i=0; i<16; i++)
    {
        awLEDPmA[0][i] = 0x0;
        awLEDPmA[1][i] = 0x0;
        awLEDPmA[2][i] = 0x0;
        awLEDPmA[3][i] = 0x0;
    }
    i = drvSoftReset9523(0); //初始化0???AW9523B
    i = drvSoftReset9523(2); //初始化0???AW9523B
    drvInitAW9523B();    //初始化AW9523B芯片
    drvSetLEDUpdata(0);
    drvSetLEDUpdata(2);

    j = 0x0;
    while(1==1)
    {
        if( k < 41)
            k++;
        else
        {
            k = 0;
            awLEDPmA[svLED[j].ledRedIC][svLED[j].ledRedmA] = 0x00;
            awLEDPmA[svLED[j].ledGreenIC][svLED[j].ledGreenmA] = 0x00;
            awLEDPmA[svLED[j].ledBlueIC][svLED[j].ledBluemA] = 0x00;
            if (j < 7)
                j++;
            else
                j = 0;
        }
        awLEDPmA[svLED[j].ledRedIC][svLED[j].ledRedmA] = (uint8_t)(colLedList[k]>>16);
        awLEDPmA[svLED[j].ledGreenIC][svLED[j].ledGreenmA] = (uint8_t)(colLedList[k]>>8);
        awLEDPmA[svLED[j].ledBlueIC][svLED[j].ledBluemA] = (uint8_t)(colLedList[k]);
        drvSetLEDUpdata(0);
        drvSetLEDUpdata(2);

        i= drvRead9523_ID(2, 0x10);
        i= drvRead9523_ID(2, 0x01);
        delay_ms(500);
    }
}


#if USE_GPIOA4_IRQ

void GPIOC5_Handler(void)
{
    EXTI_Clear(GPIOC, PIN5);
}

#else

void GPIOC_Handler(void)
{
    if(EXTI_State(GPIOC, PIN5))
    {
        InvDrvP(pCOut[5]);
    }
}

#endif

