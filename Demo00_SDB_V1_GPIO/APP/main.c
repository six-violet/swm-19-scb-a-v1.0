/*  杭州六堇科技有限公司
 *  2023-03-27
 *  GPIO Demo
 *  By Dawoo.Zheng
 *
 */

#include "main.h"
//#include "drvMcu.h"

#define __MTIME_A 10000
#define __MTIME_B 200

void delay_xms(void)
{
    uint32_t i,j;
    for(i=0; i< __MTIME_A; i++)
    {
        for(j= 0; j<__MTIME_B; j++)
        {
            ;
        }
    }
}

int main(void)
{
//    uint8_t i;
    SystemInit();
    InitAllPin();

    InvDrvP(pBOut[7]);	//LED A Off
    InvDrvP(pBOut[8]);	//LED B Off
    while(1==1)
    {
        delay_xms();
        InvDrvP(pCOut[5]);	//LED Red
        delay_xms();
        InvDrvP(pCOut[5]);	//LED Green
        delay_xms();
        InvDrvP(pBOut[7]);	//LED A ON
        delay_xms();
        InvDrvP(pBOut[7]);	//LED A Off
        delay_xms();
        InvDrvP(pBOut[8]);	//LED B ON
        delay_xms();
        InvDrvP(pBOut[8]);	//LED B Off
    }
}


