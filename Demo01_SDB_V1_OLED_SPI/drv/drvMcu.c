/* www.6violet.com
 * FileName drvMcu.c
 * creat time: 2023-8-27 by dawoo.Zheng
 *    The last modifier: dawoo.Zheng
 * The last change time: 2023-8-27
 * commit:
 */

#include "drvMcu.h"
#include "drvGpio.h"
#include "displayConfig.h"
//#include "drvTimEncoder.h"
//#include "drvIWDT.h"

#define TIM_CLOCK_FREQ (10000)
#define MAXCRCBUF 256
#define _MAX_ENCODER_COUNT_ 256

/// Ex: delayMs(100);
void delayUs(volatile uint32_t u32DelayNum)
{
    uint32_t t_i,i;
    t_i = (u32DelayNum * CyclesPerUs)/10;
    for(i=0; i<t_i; i++)
    {
        ;
    }
}

void delayMs(volatile uint32_t u32DelayNum)
{
    uint32_t t_i;
    t_i = u32DelayNum * 1000;
    if(t_i >= u32DelayNum)
        delayUs(t_i);
    else
    {
        for(t_i = 0; t_i<u32DelayNum ; t_i++)
        {
            delayUs(1000);
        }
    }
//    uint32_t i,j,k;
//    for(k=0; k<u32DelayMsNum; k++)
//    {
//        for(i=0; i<130; i++)
//        {
//            for(j=0; j<130; j++)
//            {
//                ;
//            }
//        }
//    }
}

uint16_t strSizeC(uint8_t *strBuf)
{
    uint8_t * str;
    uint16_t count = 0;
    str = strBuf;
    while(count < 65000)
    {
        if(*str++ == 0)
            return (uint16_t)(str-strBuf-1);
    }
    return 0;
}

void keyInputInit(void)
{
    //Set               PA5 KeyInput
}

void gpioInitAll(void)
{
    InitAllPin();
#if (SCREEN_USE_SPI == 1)       //如果使用SPI接口
#endif  //SCREEN_USE_SPI ==1
}

#if (SCREEN_USE_SPI == 1)       //如果使用SPI接口
void gpioInitSPI(void)
{
    SPI_InitStructure SPI_initStruct;

    SPI_initStruct.clkDiv = SPI_CLKDIV_2;
    SPI_initStruct.FrameFormat = SPI_FORMAT_SPI;
    SPI_initStruct.SampleEdge = SPI_FIRST_EDGE;
    SPI_initStruct.IdleLevel = SPI_LOW_LEVEL;
    SPI_initStruct.WordSize = 8;
    SPI_initStruct.Master = 1;
    SPI_initStruct.RXThreshold = 0;
    SPI_initStruct.RXThresholdIEn = 0;
    SPI_initStruct.TXThreshold = 0;
    SPI_initStruct.TXThresholdIEn = 0;
    SPI_initStruct.TXCompleteIEn = 0;
    SPI_Init(SPI0, &SPI_initStruct);
    SPI_Open(SPI0);
}

void Spi_Send_N_Data(SPI_TypeDef* SPIx, uint8_t *data, uint32_t num)
{
    uint32_t i;
    for(i=0; i<num ; i++)
    {
        SPI_WriteWithWait(SPIx,*(uint8_t *)(data++));
    }
}
#endif  //SCREEN_USE_SPI ==1

void gpioInitUart(void)
{
}

void screenPortInit(void)
{
}

void BSPInit(void)
{
    gpioInitAll();
#if (SCREEN_USE_SPI == 1)       //????SPI??
    gpioInitSPI();
#endif  //SCREEN_USE_SPI ==1
//    Spi_SetCS(M0P_SPI, FALSE);
//    M0P_SPI->SSN = FALSE;
//    Spi_SendData(M0P_SPI, 0x55);
    //gpioInitUart();
    //uartInit(UART1, 115200, UART_MODE_TX_RX);
    //uartInit(UART3, 115200, UART_MODE_TX_RX_DEBUG);

    //tim6Init();
    //keyInputInit();
    //encoderInit();
    screenDrvInit();
}

