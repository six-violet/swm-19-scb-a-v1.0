/* www.6violet.com
 * FileName drvSSD1315.h
 * creat time: 2023-8-28 by dawoo.Zheng
 *    The last modifier: dawoo.Zheng
 * The last change time: 2023-12-18
 * commit:
 */

#ifndef __DRV_SSD1315_H
#define __DRV_SSD1315_H

#include "displayConfig.h"

//#if (SCREEN_USE_SPI == 1)       //如果使用SPI接口
//#include "drvSpi.h"
//#endif  //SCREEN_USE_SPI ==1

#if (SCREEN_USE_SGPIO == 1)     //如果使用GPIO模拟串行接口
#include "gpio.h"
#endif  //SCREEN_USE_SGPIO ==1

#if (SCREEN_USE_PGPIO == 1)     //如果使用GPIO并行接口屏
#include "drvGpio.h"
#endif  //SCREEN_USE_PGPIO ==1

#define SVO_SetPageLAddr      0x00        //C0[0x00-0x0F]:设置坐标低页地址
#define SVO_SetPageHAddr      0x10        //C0[0x10-0x17]:设置坐标高页地址
#define SVO_SetMemoryAddr     0x20        //C0[0x20]:设置内存地址，下一个命令
//                                        //C1[0x00-0x03]:四种模式
#define SVO_SetColumnAddr     0x21        //C0[0x21]:设置列地址，下一个命令
//                                        //C1[0x00-0x7F]:
//                                        //C2[0x00-0x7F]:本命令只在0x20的00,01模式
#define SVO_SetPageAddr       0x22        //C0[0x22]:设置行地址，下一个命令
//                                        //C1[0x00-0x07]:
//                                        //C2[0x00-0x07]:本命令只在0x20的00,01模式
#define SVO_SetDispSLine      0x40        //C0[0x40-0x7F]:设置显示开始行
#define SVO_SetContrast       0x81        //C0[0x81]:设置对比控制
//                                        //C1[0x00-0xFF]:对比度值
#define SVO_SetSegRemap       0xA0        //C0[0xA0-0xA1]:设置部分重绘
#define SVO_SetEntireON       0xA4        //C0[0xA4-0xA5]:设置全部显示
#define SVO_SetNor_Inv        0xA6        //C0[0xA6-0xA7]:设置正反转
#define SVO_SetMultRatio      0xA8        //C0[0xA8]:设置多比率
//                                        //C1[0x00-0x3F]:
#define SVO_SetExtOrIntI      0xAD        //C0[0xAD]:设置内外部参考电流
//                                        //C1[0x00-0x30]:b7,6,3,2,1,0=0b
#define SVO_SetDispOff        0xAE        //C0[0xAE]:设置关显示
#define SVO_SetDispON         0xAF        //C0[0xAE]:设置开显示
#define SVO_SetPageStart      0xB0        //C0[0xB0-0xB7]:设置GDDRAM_PageStart
#define SVO_SetComOutDir      0xC0        //C0[0xC0-0xC8]:设置COM脚的扫描方向
#define SVO_SetDispOffset     0xD3        //C0[0xD3]:设置偏移量
//                                        //C1[0x00-0x3F]:
#define SVO_SetDispClkFre     0xD5        //C0[0xD5]:设置时钟相关
//                                        //C1[0x00-0xFF]:b[3:0]/b[7:4]
#define SVO_SetPha2Charge     0xD9        //C0[0xD9]:设置相1相2充电时钟
//                                        //C1[0x00-0xFF]:
#define SVO_SetCOM_Hard       0xDA        //C0[0xDA]:设置COM硬件设置
//                                        //C1[0x02-0x32]:b1=1b;
#define SVO_SetVcomhLev       0xDB        //C0[0xDB]:设置COM电压
//                                        //C1[0x00-0x30]:0.65，0.71，0.77，0.83
#define SVO_Soft_NOP          0xE3        //C0[0xE3]:NOP
#define SVO_SetChargePump     0x8D        //C0[0x8D]:设置内部充电选择
//                                        //C1[0x10-0x95]:set,b7+b2+b0;
#define SVO_SetRightHorScr    0x26        //C0[0x26]:设置右水平滚动
//                                        //C1[0x00]:
//                                        //C2[0x00-0x07]:开始Page0-Page7
//                                        //C3[0x00-0x07]:设置帧间隔
//                                        //C4[0x00-0x07]:结束Page0-Page7
//                                        //C5[0x00-0x7F]:开始列地址
//                                        //C6[0x00-0x7F]:结束列地址
#define SVO_SetLeftHorScr     0x27        //C0[0x27]:设置左水平滚动
//                                        //C1[0x00]:
//                                        //C2[0x00-0x07]:开始Page0-Page7
//                                        //C3[0x00-0x07]:设置帧间隔
//                                        //C4[0x00-0x07]:结束Page0-Page7
//                                        //C5[0x00-0x7F]:开始列地址
//                                        //C6[0x00-0x7F]:结束列地址
#define SVO_SetVer_HorScr     0x29        //C0[0x29-0x2A]:设置4向滚动
//                                        //C1[0x00]:
//                                        //C2[0x00-0x07]:开始Page0-Page7
//                                        //C3[0x00-0x07]:设置帧间隔
//                                        //C4[0x00-0x07]:结束Page0-Page7
//                                        //C5[0x00-0x7F]:开始列地址
//                                        //C6[0x00-0x7F]:结束列地址
#define SVO_SetDeacScroll     0x2E        //C0[0x2E]:解除滚动
#define SVO_SetActiScroll     0x2F        //C0[0x2F]:激活滚动
#define SVO_SetVerScrollA     0xA3        //C0[0xA3]:设置垂直滚动区域
//                                        //C1[0x00-0x3F]:设置顶部固定的行数
//                                        //C2[0x00-0x7F]:设置固定之下滚动行数
#define SVO_SetContentScr     0x2C        //C0[0x2C-0x2D]:设置左右内容滚动
//                                        //C1[0x00]:
//                                        //C2[0x00-0x07]:开始Page0-Page7
//                                        //C3[0x00-0x07]:设置帧间隔
//                                        //C4[0x00-0x07]:结束Page0-Page7
//                                        //C5[0x00-0x7F]:开始列地址
//                                        //C6[0x00-0x7F]:结束列地址
#define SVO_SetFadeFrames     0x23        //C0[0x23]:设置淡入淡出时钟
//                                        //C1[0x00-0x3F]:
//                                        b[5:4]:=00b禁用，=10b启用
//                      b[3:0]:0000b=8帧,0001b=16帧,0010b=24帧,1111b=128帧,
#define SVO_SetZoomIn         0xD6        //C0[0xD6]:设置放大模式
//                                        //C1[0x00-0x01]:0b=Disable,1b=Enable

extern void delayMs(volatile uint32_t u32DelayMsNum);

void screenDrvInit(void);                               //初始化屏幕
void screenSetBeginCoord(SV_COORD x,SV_COORD y);    //设置坐标函数
void screenWriteByteData(uint8_t data);                 //屏幕写 8bit数据
void screenWrite_NByteData(unsigned char * data,uint16_t number);      //屏幕写X Byte数据
void screenWriteByteCMD(uint8_t data);                  //屏幕写 8bit命令
void OledShowPicture(SV_COORD x0,SV_COORD y0,SV_COORD x1,SV_COORD y1,unsigned char * picData);
void ShowPictureRam(SV_COORD x0,SV_COORD y0,SV_COORD x1,SV_COORD y1,unsigned char * picData);
void screenFill(SV_COLOR color);
void screenFillRam(SV_COLOR color);
void screenOff(void);                                   //关闭屏幕或背光
void screenON(void);                                    //打开屏幕或背光
void screenDrvNop(void);                                //空指令
void screenDrvSleep(uint8_t state);                     //=1进入睡眠//=0退出睡眠
void screenDrvBrightnes(uint8_t val);
void screenDisplayInverOff(void);                       //0xA6//倒置关闭
void screenDisplayInverON(void);                        //0xA7//倒置打开
void screenDrvInversion(uint8_t state);                 //=1进入反色//=0退出反色
void screenDrvDisplay(uint8_t state);                   //=1进入反色//=0退出反色
void screenDrvSoftReset(void);
void screenTurn(uint8_t i);                             //i=0,屏幕旋转0度;i=1,屏幕旋转180度
void screenDrvPixel(SV_COORD x,SV_COORD y,SV_COLOR color);     //画一个点
void screenAllRefresh(void);                            //更新显存到OLED
void screenFill_A5(void);
void screenFill_5A(void);
/*
void screenWriteWordData(uint16_t data);                //屏幕写16bit数据
void screenWriteDoubleData(uint32_t data);              //屏幕写32bit数据
void screenWriteWordCMD(uint16_t data);                 //屏幕写16bit命令
void screenSetViewArea(SV_COORD x1,SV_COORD y1,SV_COORD x2,SV_COORD y2);    //设置坐标函数
void screenSetPosition(SV_COORD x1,SV_COORD y1);        //设置坐标函数
void screenDrvBlock(SV_COORD x,SV_COORD y,uint16_t w,uint16_t h,SV_COLOR color);    //画色块

///>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

uint8_t screenDrvReadByte(void);
uint16_t screenDrvReadWord(void);

uint8_t screenDrvGetManu(void);                         //读取显示屏制造商
uint8_t screenDrvGetVersion(void);                      //读取显示屏版本号
uint8_t screenDrvGetId(void);                           //读取显示屏版本号

*/

#endif      //__DRV_SSD1315_H


