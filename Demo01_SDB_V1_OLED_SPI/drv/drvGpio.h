/* www.6violet.com
 * FileName drvGpio.h
 * creat time: 2023-8-27 by dawoo.Zheng
 *    The last modifier: dawoo.Zheng
 * The last change time: 2023-12-27
 * commit:
 */

#ifndef __DRV_GPIO_H
#define __DRV_GPIO_H

#include "SWM190.h"
#include "SWM190_port.h"
#include "SWM190_gpio.h"

#define SV_GPIO_TYPE GPIO_TypeDef

typedef struct _GPIO_P_
{
    SV_GPIO_TYPE * ioPort;
    uint32_t ioPin;
} _SV_SGpio;

extern const _SV_SGpio lcdMosiport;
extern const _SV_SGpio lcdSClkport;
extern const _SV_SGpio lcdCSport;
extern const _SV_SGpio lcdDCport;
extern const _SV_SGpio rs485dir;
extern const _SV_SGpio inKeyInt;

void InitAllPin(void);
void SetDrvP(_SV_SGpio dp);
void ClrDrvP(_SV_SGpio dp);
void InvDrvP(_SV_SGpio dp);
uint32_t GetDrvP(_SV_SGpio dp);

#endif      //__DRV_GPIO_H

