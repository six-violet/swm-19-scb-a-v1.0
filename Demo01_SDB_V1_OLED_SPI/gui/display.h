/* www.6violet.com
 * FileName display.h
 * creat time: 2023-8-28 by dawoo.Zheng
 *    The last modifier: dawoo.Zheng
 * The last change time: 2023-12-11
 * commit:
 */

#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#include "displayConfig.h"  //所有与屏与硬件相关的配置信息

extern void screenSetViewArea(SV_COORD x1,SV_COORD y1,SV_COORD x2,SV_COORD y2);    //设置坐标函数
extern void screenPoint(SV_COORD x,SV_COORD y,SV_COORD color);     //画一个点

//Init SCREEN
//#define displayInit() screenDrvInit()
//#define displayOff() screenOff()    //关掉背光
//#define displayON() screenON()      //打开背光


//void displayClean(void)
//void displayReverse(void);

//------------------------------------o
//  设置默认背景色   |     COLOR      |
void setDefBKColorVal(SV_COLOR bkcolor);

//------------------------------------o
//  设置默认前景色   |     COLOR      |
void setDefFGColorVal(SV_COLOR fgcolor);

//------------------------------------------------------o
//    单独画     |   X坐    |    Y坐    |    COLOR      |
//    点程序     |   标点   |    标点   |    颜色       |
uint8_t drawPoint(SV_COORD x, SV_COORD y, SV_COLOR color);

//---------------------------------------------------------------------------------o
//     画线     |    X1坐   |    Y1坐    |    X2坐    |    Y2坐    |    COLOR      |
//     函数     |    标点   |    标点    |    标点    |    标点    |    颜色       |
uint8_t drawLine(SV_COORD x1, SV_COORD y1, SV_COORD x2, SV_COORD y2, SV_COLOR color);

//-----------------------------------------------------------------------------------o
//       画方形      |   X1坐    |    Y1坐   |   X2坐    |    Y2坐   |     COLOR     |
//       线宽1       |   标点    |    标点   |   标点    |    标点   |     颜色      |
uint8_t drawRectangle(SV_COORD x1,SV_COORD y1,SV_COORD x2,SV_COORD y2, SV_COLOR color);

//--------------------------------------------------------------------------------------------------o
//       画方形          |   X1坐   |    Y1坐   |   X方向   |   Y方向   |    X==Y   |    COLOR      |
//       线宽z点         |   标点   |    标点   |   宽度    |   宽度    |    宽度Z  |    颜色       |
uint8_t drawRectangleEdge(SV_COORD x, SV_COORD y, uint16_t w, uint16_t h, uint16_t z, SV_COLOR color);

//----------------------------------------------------------------------------------o
//      画方形       |   X1坐   |    Y1坐   |   X方向   |   Y方向   |     COLOR     |
//      全填充       |   标点   |    标点   |   宽度    |   宽度    |     颜色      |
uint8_t fillRectangle(SV_COORD x, SV_COORD y, uint16_t w, uint16_t h, SV_COLOR color);

//--------------------------------------------------------------------------------------o
//     画环形     |  圆心坐   |   圆心坐   |     半径长      |  圆环的  |     COLOR     |
//     线宽w点    |  标点X    |   标点Y    |     r像素       |   宽度   |     颜色      |
uint8_t drawCircle(uint16_t ox, uint16_t oy, uint16_t radiusP, uint8_t w, SV_COLOR color);

#if (USE_SV_SIN_COS_TABLE_ == 1)
extern const int16_t cSinCosList[][2];
//------------扇形的圆心是常量，const uint16_t centreX,centreY;-----------------------------o
//       画扇形         |  起始角  |   终止角  |     半径长      |  扇形的  |     COLOR     |
//       线宽w点        |  [1,360] |  [1,360]  |     r像素       |   宽度   |     颜色      |
uint8_t drawCircleSector(uint16_t a, uint16_t b, uint16_t radiusP, uint8_t w, SV_COLOR color);
#endif

//-----------------------------------------------------------------------------------o
//     画16色    |  左上坐  |   左上坐  |  图像    |   图像    |      COLOR颜色      |
//     彩图      |  标点X   |   标点Y   |  宽度    |   高度    |      数据指针       |
uint8_t drawImage(SV_COORD x, SV_COORD y,uint16_t w, uint16_t h, unsigned char * data);

//------------------------------------------------------------------------------------------------------------------o
//     使用图标     |   X1坐    |    Y1坐    |    X2坐    |    Y2坐    |  图像    |   图像    |      图标对应       |
//     填充区域     |   标点    |    标点    |    标点    |    标点    |  宽度    |   高度    |      数据指针       |
uint8_t drawPadImage(SV_COORD x1, SV_COORD y1, SV_COORD x2, SV_COORD y2,uint16_t w, uint16_t h, unsigned char * data);

//---------------------------------------------------------------------------------------------------------o
//     显示     |     字体       |   左上坐  |   左上坐  |   文字编      |     COLOR前    |     COLOR背    |
//     文字     |     高度       |   标点X   |   标点Y   |   码指针      |     景颜色     |     景颜色     |
uint8_t drawText(uint8_t fontSize, SV_COORD x, SV_COORD y, uint8_t * data, SV_COORD tColor, SV_COORD bColor);

//------------------------------------------------------------------------------------------------------o
//   显示有符   |     字体       |   左上坐  |   左上坐  |   8bit     |     COLOR前    |     COLOR背    |
//   号8bit数   |     高度       |   标点X   |   标点Y   |   数据     |     景颜色     |     景颜色     |
uint8_t drawInt8(uint8_t fontSize, SV_COORD x, SV_COORD y, int8_t data, SV_COORD tColor, SV_COORD bColor);

//---------------------------------------------------------------------------------------------------------o
//    显示无符    |     字体       |   左上坐  |   左上坐  |   16bit     |     COLOR前    |     COLOR背    |
//    号16bit数   |     高度       |   标点X   |   标点Y   |   数据      |     景颜色     |     景颜色     |
uint8_t drawUint16(uint8_t fontSize, SV_COORD x, SV_COORD y,uint16_t data, SV_COORD tColor, SV_COORD bColor);

//--------------------------------------------------------------------------------------------o
//      显示单       |      字体       |   左上坐  |   左上坐  |    文字编   |     显示的     |
//      个半字       |      高度       |   标点X   |   标点Y   |    码数据   |     模式       |
uint8_t drawAsciiChar(uint16_t fontSize, SV_COORD x, SV_COORD y, uint8_t data, uint8_t dispMod);

//-------------------------------------------------------------------------------------------o
//    显示En      |      字体       |   左上坐  |   左上坐  |     文字编    |     显示的     |
//    字符串      |      高度       |   标点X   |   标点Y   |     码数据    |     模式       |
uint8_t drawString(uint16_t fontSize, SV_COORD x, SV_COORD y, uint8_t * data, uint8_t dispMod);

//-----------------------------------------------------------------------------------------o
//    显示En     |      字体       |   左上坐  |   左上坐  |    文字编    |     显示的     |
//    字符串     |      高度       |   标点X   |   标点Y   |    码数据    |     模式       |
uint8_t drawHex1B(uint16_t fontSize, SV_COORD x, SV_COORD y, uint8_t  data, uint8_t dispMod);

//------------------------------------------------------------------------------------------o
//    显示En     |      字体       |   左上坐  |   左上坐  |    文字编     |     显示的     |
//    字符串     |      高度       |   标点X   |   标点Y   |    码数据     |     模式       |
uint8_t drawHex2B(uint16_t fontSize, SV_COORD x, SV_COORD y, uint16_t  data, uint8_t dispMod);

//------------------------------------------------------------------------------------------o
//    显示En     |      字体       |   左上坐  |   左上坐  |    文字编     |     显示的     |
//    字符串     |      高度       |   标点X   |   标点Y   |    码数据     |     模式       |
uint8_t drawHex4B(uint16_t fontSize, SV_COORD x, SV_COORD y, uint32_t  data, uint8_t dispMod);

//------------------------------------------------------------------------------------------------------------------o
//       显示单        |      字体       |   左上坐  |   左上坐  |     文字编     |     COLOR前    |     COLOR背    |
//       个汉字        |      高度       |   标点X   |   标点Y   |     码数据     |     景颜色     |     景颜色     |
uint8_t drawChineseChar(uint16_t fontSize, SV_COORD x, SV_COORD y, uint16_t hzCode, SV_COORD tColor, SV_COORD bColor);


#endif  //__DISPLAY_H__
