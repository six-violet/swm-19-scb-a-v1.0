/*  杭州六堇科技有限公司
 *  2023-03-27
 *  GPIO Demo
 *  By Dawoo.Zheng
 *
 */

#include "drvMcu.h"
#include "bmp.h"

extern void delayUs(volatile uint32_t u32DelayNum);
extern void delayMs(volatile uint32_t u32DelayNum);
//void delayMs(uint32_t t)
//{
//    uint16_t i,j;
//    for(i=0; i<t; i++)
//        for(j=0; j<1400; j++)
//            ;
//}

int main(void)
{
    uint8_t i;
    SystemInit();

    delayMs(10);
    BSPInit();

    screenDisplayInverOff();//
    screenTurn(0);//
    screenAllRefresh();
    for(i=0; i<10; i++)
    {
        drawRectangle(  1,  1,30,30,1);
        drawRectangle( 33,  1,63,30,1);
        drawRectangle( 65,  1,95,30,1);
        drawRectangle( 97,  1,127,30,1);
        screenAllRefresh();
        fillRectangle(  1, 33,30,30,1);
        fillRectangle( 33, 33,30,30,1);
        fillRectangle( 65, 33,30,30,1);
        fillRectangle( 97, 33,30,30,1);
        screenAllRefresh();
        delayMs(300);
        screenFillRam(0xFF);
        drawString(16, 0, 0,(uint8_t *)"ABCDEF0123456789", 0);
        drawString(16, 0,16,(uint8_t *)"EveryCourseHasRe", 0);
        drawString(16, 0,32,(uint8_t *)"alProjectsDesign", 0);
        drawString(16, 0,48,(uint8_t *)"fedcba9876543210", 0);
        screenAllRefresh();
        delayMs(300);
        screenFillRam(0xFF);
        drawString(16, 0, 0,(uint8_t *)"ABCDEF0123456789", 1);
        drawString(16, 0,16,(uint8_t *)"EveryCourseHasRe", 1);
        drawString(16, 0,32,(uint8_t *)"alProjectsDesign", 1);
        drawString(16, 0,48,(uint8_t *)"fedcba9876543210", 1);
        screenAllRefresh();
        delayMs(300);
        drawPadImage(  0,  0,127,8,32,4,(unsigned char *)gImage_04);
        delayMs(300);
        screenFill_5A();
        drawAsciiChar(16, 66,  7, 'A', 1);
        drawAsciiChar(16, 66, 37, 'B', 1);
        drawAsciiChar(16, 86,  7, 'C', 0);
        drawAsciiChar(16, 86, 37, 'D', 0);
		drawHex1B(16,32,32,i,0);
        screenAllRefresh();
        delayMs(800);
    }

    while(1==1)
    {
        OledShowPicture(  0,  0,32,4,(unsigned char *)gImage_01);
        OledShowPicture( 32,  0,32,4,(unsigned char *)gImage_02);
        OledShowPicture( 64,  0,32,4,(unsigned char *)gImage_03);
        OledShowPicture( 96,  0,32,4,(unsigned char *)gImage_04);
        OledShowPicture(  0,  4,32,4,(unsigned char *)gImage_05);
        OledShowPicture( 32,  4,32,4,(unsigned char *)gImage_06);
        OledShowPicture( 64,  4,32,4,(unsigned char *)gImage_07);
        OledShowPicture( 96,  4,32,4,(unsigned char *)gImage_08);
        GPIO_SetBit(GPIOC, PIN6);
        delayMs(300);

        ShowPictureRam(  0,  0,32,4,(unsigned char *)gImage_09);
        ShowPictureRam( 32,  0,32,4,(unsigned char *)gImage_10);
        ShowPictureRam( 64,  0,32,4,(unsigned char *)gImage_11);
        ShowPictureRam( 96,  0,32,4,(unsigned char *)gImage_12);
        ShowPictureRam(  0,  4,32,4,(unsigned char *)gImage_04);
        ShowPictureRam( 32,  4,32,4,(unsigned char *)gImage_03);
        ShowPictureRam( 64,  4,32,4,(unsigned char *)gImage_02);
        ShowPictureRam( 96,  4,32,4,(unsigned char *)gImage_01);
		drawHex1B(16,64,32,i++,0);
		drawHex2B(16,32,16,0x4321,0);
		drawHex4B(16, 0, 0,0x1A3B5C7D,1);
        screenAllRefresh();
        GPIO_ClrBit(GPIOC, PIN6);
        delayMs(300);

//        OledShowString(0, 2,"1234567890abcdefghij",12);
//        OledShowString(0,14,"klmnopqrstuvwxyz!@#$",12);
//        OledShowString(0,26,"ABCDEFGHIJKLMNOPQRST",12);
//        OledShowString(0,38,"UVWXYZ%^&*()_+-=~`[]",12);
//        OledShowString(0,50,"{}:;',><?/|\\\"",12);
//        OledRefresh();
//        GPIO_SetBit(GPIOC, PIN6);
//        delayMs(1000);

//        OledClear();
//        OledShowString(0, 0,"0123456789!@#$%^",16);
//        OledShowString(0,16,"ABCDEFGHIJKLMNOP",16);
//        OledShowString(0,32,"QRSTUVWXYZ_abcde",16);
//        OledShowString(0,48,"fghijklmnopqrstu",16);
//        OledRefresh();
//        GPIO_ClrBit(GPIOC, PIN6);
//        delayMs(1000);

//        OledClear();
//        OledShowString(0, 0,"0123456789",24);
//        OledShowString(0,24,"ABCDEFGHIJ",24);
//        OledRefresh();
//        GPIO_SetBit(GPIOC, PIN6);
//        delayMs(1000);

//        OledShowPicture(0,0,128,8,BMP1);
//        screenTurn(1);
//        OledDisplayTurn(1);
//        GPIO_ClrBit(GPIOC, PIN6);
//        delayMs(10);
//        screenTurn(0);
//        OledDisplayTurn(0);
    }
}


