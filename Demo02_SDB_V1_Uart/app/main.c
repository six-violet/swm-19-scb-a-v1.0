/*  杭州六堇科技有限公司
 *  2023-03-27
 *  GPIO Demo
 *  By Dawoo.Zheng
 *
 */

#include "drvMcu.h"
//#include "oledfont.h"
#include "bmp.h"

void delay_ms(uint32_t t)
{
    uint16_t i,j;
    for(i=0; i<t; i++)
        for(j=0; j<1400; j++)
            ;
}

int main(void)
{
    uint16_t i = 0;
    for(i=0; i<=60000; i++)
        ;
    SystemInit();

    delay_ms(10);
    BSPInit();

    SetDrvP(pCOut[5]);
    OledColorTurn(0);//
    OledDisplayTurn(0);//
    OledRefresh();
    OledShowPicture(0,0,128,8,BMP1);
    OledShowPicture(0,0,128,8,BMP2);
    printf("SixViolet Demo App!");
    printZ(UART2, "SixViolet Demo App!");

    while(1==1)
    {
        printf("SixViolet Demo App!Number=%d",i++);
        printZ(UART2, "Uart2! Test 115200bps \tSixViolet");
        InvDrvP(pCOut[5]);
        delay_ms(50);
    }
}


