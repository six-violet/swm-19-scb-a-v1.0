/*  杭州六堇科技有限公司
 *  2023-03-27
 *  GPIO Demo
 *  By Dawoo.Zheng
 *
 *  SetAllPin.h
 *
 */
#ifndef _SET_ALL_PIN_H_
#define _SET_ALL_PIN_H_

#include "SWM190.h"
#include "SWM190_port.h"
#include "SWM190_gpio.h"

#define __DEBUG_ON_

typedef struct _GPIO_P_
{
    GPIO_TypeDef * io_P;
    uint32_t io_n;
} pGpio;


extern const pGpio pAOut[];
extern const pGpio pBOut[];
extern const pGpio pCOut[];
extern const pGpio pDOut[];
extern const pGpio pEOut[];

void InitAllPin(void);
void SetDrvP(pGpio dp);
void ClrDrvP(pGpio dp);
void InvDrvP(pGpio dp);
uint32_t GetInvP(pGpio dp);

#endif  //_SET_ALL_PIN_H_

