/*  杭州六堇科技有限公司
 *  2023-03-27
 *  GPIO Demo
 *  By Dawoo.Zheng
 *
 *  SetAllPin
 */
#include "SetAllPin.h"

const pGpio pAOut[]=
{
    {GPIOA,PIN0},
    {GPIOA,PIN2},
    {GPIOA,PIN3},
    {GPIOA,PIN8},
    {GPIOA,PIN9},
    {GPIOA,PIN10},
    {GPIOA,PIN11},
    {GPIOA,PIN12},
    {GPIOA,PIN13},
    {GPIOA,PIN14},
    {GPIOA,PIN15},
};

const pGpio pBOut[]=
{
    {GPIOB,PIN0},
    {GPIOB,PIN1},
    {GPIOB,PIN2},
    {GPIOB,PIN3},
    {GPIOB,PIN4},
    {GPIOB,PIN5},
    {GPIOB,PIN6},
    {GPIOB,PIN7},
    {GPIOB,PIN8},
    {GPIOB,PIN9},
    {GPIOB,PIN10},
    {GPIOB,PIN11},
    {GPIOB,PIN12},
    {GPIOB,PIN13},
    {GPIOB,PIN14},
    {GPIOB,PIN15},
};

const pGpio pCOut[]=
{
    {GPIOC,PIN0},
    {GPIOC,PIN2},
    {GPIOC,PIN3},
    {GPIOC,PIN4},
    {GPIOC,PIN5},
    {GPIOC,PIN6},
    {GPIOC,PIN7},
};

const pGpio pDOut[]=
{
    {GPIOD,PIN0},
    {GPIOD,PIN1},
};

const pGpio pEOut[]=
{
    {GPIOE,PIN2},
    {GPIOE,PIN3},
    {GPIOE,PIN4},
    {GPIOE,PIN5},
    {GPIOE,PIN7},
};

void InitAllPin(void)
{
//  …………………………………………………………………………………………………………………………………………………………………\
//  |               设置GPIO的功能                          |              |
//  |         Port?|  PIN?| Fuction?            |IE?|       |              |
    PORT_Init(PORTA,  PIN0,      PORTA_PIN0_GPIO, 0);       //输出:GPIO-A00
    PORT_Init(PORTA,  PIN2,     PORTA_PIN2_SWCLK, 0);       //SWCLK
    PORT_Init(PORTA,  PIN3,     PORTA_PIN3_SWDIO, 0);       //SWDIO
    PORT_Init(PORTA,  PIN8, PORTA_PIN8_SPI0_SSEL, 0);       //SPI0:SSEL
    PORT_Init(PORTA,  PIN9, PORTA_PIN9_SPI0_MISO, 0);       //SPI0:MISO
    PORT_Init(PORTA, PIN10,PORTA_PIN10_SPI0_MOSI, 0);       //SPI0:MOSI
    PORT_Init(PORTA, PIN11,PORTA_PIN11_SPI0_SCLK, 0);       //SPI0:SCLK
    PORT_Init(PORTA, PIN12,     PORTA_PIN12_GPIO, 0);       //输出:GPIO-A12
    PORT_Init(PORTA, PIN13,     PORTA_PIN13_GPIO, 0);       //输出:GPIO-A13
    PORT_Init(PORTA, PIN14, PORTA_PIN14_ADC0_IN2, 0);       //ADC0: -2
    PORT_Init(PORTA, PIN15, PORTA_PIN15_ADC0_IN1, 0);       //ADC0: -1
//  |         Port?|  PIN?| Fuction?            |IE?|       |              |
    PORT_Init(PORTB,  PIN0,      PORTB_PIN0_GPIO, 0);       //输出:GPIO-B00
    PORT_Init(PORTB,  PIN1,      PORTB_PIN1_GPIO, 0);       //输出:GPIO-B01
    PORT_Init(PORTB,  PIN2,      PORTB_PIN2_GPIO, 0);       //输出:GPIO-B02
    PORT_Init(PORTB,  PIN3,      PORTB_PIN3_GPIO, 0);       //输出:GPIO-B03
    PORT_Init(PORTB,  PIN4,      PORTB_PIN4_GPIO, 0);       //输出:GPIO-B04
    PORT_Init(PORTB,  PIN5,      PORTB_PIN5_GPIO, 0);       //输出:GPIO-B05
    PORT_Init(PORTB,  PIN6,      PORTB_PIN6_GPIO, 0);       //输出:GPIO-B06
    PORT_Init(PORTB,  PIN7,  PORTB_PIN7_UART2_RX, 0);       //UART2:Rxd
    PORT_Init(PORTB,  PIN8,  PORTB_PIN8_UART2_TX, 0);       //UART2:Txd
    PORT_Init(PORTB,  PIN9,      PORTB_PIN9_GPIO, 0);       //输出:GPIO-B09
    PORT_Init(PORTB, PIN10,     PORTB_PIN10_GPIO, 0);       //输出:GPIO-B10
    PORT_Init(PORTB, PIN11,     PORTB_PIN11_GPIO, 0);       //输出:GPIO-B11
    PORT_Init(PORTB, PIN12,     PORTB_PIN12_GPIO, 0);       //输出:GPIO-B12
    PORT_Init(PORTB, PIN13,     PORTB_PIN13_GPIO, 0);       //输出:GPIO-B13
    PORT_Init(PORTB, PIN14, PORTB_PIN14_I2C0_SDA, 0);       //I2C0:SDA
    PORT_Init(PORTB, PIN15, PORTB_PIN15_I2C0_SCL, 0);       //I2C0:SCL
//  |         Port?|  PIN?| Fuction?            |IE?|       |              |
    PORT_Init(PORTC,  PIN0,  PORTC_PIN0_XTAL_OUT, 0);       //XTAL_OUT
    PORT_Init(PORTC,  PIN1,   PORTC_PIN1_XTAL_IN, 0);       //XTAL_IN
    PORT_Init(PORTC,  PIN2,      PORTC_PIN2_GPIO, 0);       //输出:GPIO-C02
    PORT_Init(PORTC,  PIN3,      PORTC_PIN3_GPIO, 0);       //输出:GPIO-C03
    PORT_Init(PORTC,  PIN4,      PORTC_PIN4_GPIO, 0);       //输出:GPIO-C04
    PORT_Init(PORTC,  PIN5,      PORTC_PIN5_GPIO, 0);       //输出:GPIO-C05
    PORT_Init(PORTC,  PIN6,      PORTC_PIN6_GPIO, 0);       //输出:GPIO-C06
    PORT_Init(PORTC,  PIN7,      PORTC_PIN7_GPIO, 0);       //输出:GPIO-C07
//  |         Port?|  PIN?| Fuction?            |IE?|       |              |
//    PORT_Init(PORTD,  PIN0,      PORTD_PIN0_GPIO, 1);       //输出:GPIO-D00
//    PORT_Init(PORTD,  PIN1,      PORTD_PIN1_GPIO, 0);       //输出:GPIO-D01
//  |         Port?|  PIN?| Fuction?            |IE?|       |              |
    PORT_Init(PORTE,  PIN2, PORTE_PIN2_SPI0_DAT3, 0);       //SPI0:DAT3
    PORT_Init(PORTE,  PIN3, PORTE_PIN3_SPI0_DAT2, 0);       //SPI0:DAT2
    PORT_Init(PORTE,  PIN4,      PORTE_PIN4_GPIO, 0);       //输出:GPIO-E04
    PORT_Init(PORTE,  PIN5,      PORTE_PIN5_GPIO, 1);       //输出:GPIO-E05
    PORT_Init(PORTE,  PIN7,      PORTE_PIN7_GPIO, 0);       //输出:GPIO-E07
//  …………………………………………………………………………………………………………………………………………………………………\
//  |               设置GPIO的方向与特性          |              |
//  |              |      |方|上|下|开|           |              |
//  |         Port?|      |向|拉|拉|漏|           |              |
    GPIO_Init(GPIOA,  PIN0, 1, 1, 0, 0);          //输出:GPIO-A00
//    GPIO_Init(GPIOA,  PIN2, 1, 1, 0, 0);          //输出:GPIO-A02
//    GPIO_Init(GPIOA,  PIN3, 1, 1, 0, 0);          //输出:GPIO-A03
//    GPIO_Init(GPIOA,  PIN8, 1, 1, 0, 0);          //输出:GPIO-A08
//    GPIO_Init(GPIOA,  PIN9, 1, 1, 0, 0);          //输出:GPIO-A09
//    GPIO_Init(GPIOA, PIN10, 1, 1, 0, 0);          //输出:GPIO-A10
//    GPIO_Init(GPIOA, PIN11, 1, 1, 0, 0);          //输出:GPIO-A11
    GPIO_Init(GPIOA, PIN12, 1, 1, 0, 0);          //输出:GPIO-A12
    GPIO_Init(GPIOA, PIN13, 1, 1, 0, 0);          //输出:GPIO-A13
//    GPIO_Init(GPIOA, PIN14, 1, 1, 0, 0);          //输出:GPIO-A14
//    GPIO_Init(GPIOA, PIN15, 1, 1, 0, 0);          //输出:GPIO-A15
//  |              |      |方|上|下|开|           |              |
//  |         Port?|      |向|拉|拉|漏|           |              |
    GPIO_Init(GPIOB,  PIN0, 1, 1, 0, 0);          //输出:GPIO-B00
    GPIO_Init(GPIOB,  PIN1, 1, 1, 0, 0);          //输出:GPIO-B01
    GPIO_Init(GPIOB,  PIN2, 1, 1, 0, 0);          //输出:GPIO-B02
    GPIO_Init(GPIOB,  PIN3, 1, 1, 0, 0);          //输出:GPIO-B03
    GPIO_Init(GPIOB,  PIN4, 1, 1, 0, 0);          //输出:GPIO-B04
    GPIO_Init(GPIOB,  PIN5, 1, 1, 0, 0);          //输出:GPIO-B05
    GPIO_Init(GPIOB,  PIN6, 1, 1, 0, 0);          //输出:GPIO-B06
//    GPIO_Init(GPIOB,  PIN7, 1, 1, 0, 0);          //输出:GPIO-B07
//    GPIO_Init(GPIOB,  PIN8, 1, 1, 0, 0);          //输出:GPIO-B08
    GPIO_Init(GPIOB,  PIN9, 1, 1, 0, 0);          //输出:GPIO-B09
    GPIO_Init(GPIOB, PIN10, 1, 1, 0, 0);          //输出:GPIO-B10
    GPIO_Init(GPIOB, PIN11, 1, 1, 0, 0);          //输出:GPIO-B11
    GPIO_Init(GPIOB, PIN12, 1, 1, 0, 0);          //输出:GPIO-B12
    GPIO_Init(GPIOB, PIN13, 1, 1, 0, 0);          //输出:GPIO-B13
//    GPIO_Init(GPIOB, PIN14, 1, 1, 0, 0);          //输出:GPIO-B14
//    GPIO_Init(GPIOB, PIN15, 1, 1, 0, 0);          //输出:GPIO-B15
//  |              |      |方|上|下|开|           |              |
//  |         Port?|      |向|拉|拉|漏|           |              |
    GPIO_Init(GPIOC,  PIN2, 1, 1, 0, 0);          //输出:
    GPIO_Init(GPIOC,  PIN3, 1, 1, 0, 0);          //输出:
    GPIO_Init(GPIOC,  PIN4, 0, 1, 0, 0);          //输入:LED_INT
    GPIO_Init(GPIOC,  PIN5, 0, 1, 0, 0);          //输入:KEY_INT
    GPIO_Init(GPIOC,  PIN6, 1, 0, 0, 0);          //输出:
    GPIO_Init(GPIOC,  PIN7, 1, 0, 0, 0);          //输出:
//  |              |      |方|上|下|开|           |              |
//  |         Port?|      |向|拉|拉|漏|           |              |
//    GPIO_Init(GPIOD,  PIN0, 1, 1, 0, 0);          //输出:
//    GPIO_Init(GPIOD,  PIN1, 1, 1, 0, 0);          //输出:
//  |              |      |方|上|下|开|           |              |
//  |         Port?|      |向|拉|拉|漏|           |              |
//    GPIO_Init(GPIOE,  PIN2, 0, 1, 0, 0);          //输入:
//    GPIO_Init(GPIOE,  PIN3, 0, 1, 0, 0);          //输入:
    GPIO_Init(GPIOE,  PIN4, 1, 1, 0, 0);          //输出:
    GPIO_Init(GPIOE,  PIN5, 1, 1, 0, 0);          //输出:
    GPIO_Init(GPIOE,  PIN6, 1, 1, 0, 0);          //输出:

// ...................................
//   GPIO Init
//
}

void SetDrvP(pGpio dp)
{
    GPIO_SetBit(dp.io_P,dp.io_n);
}

void ClrDrvP(pGpio dp)
{
    GPIO_ClrBit(dp.io_P,dp.io_n);
}

void InvDrvP(pGpio dp)
{
    GPIO_InvBit(dp.io_P,dp.io_n);
}

uint32_t GetInvP(pGpio dp)
{
    return GPIO_GetBit(dp.io_P, dp.io_n);
}

