/* www.6violet.com
 * FileName drvMcu.h
 * creat time: 2023-8-27 by dawoo.Zheng
 *    The last modifier: dawoo.Zheng
 * The last change time: 2023-8-27
 * commit:
 */

#ifndef __DRVMCU_H
#define __DRVMCU_H

#include "SWM190.h"
#include "SetAllPin.h"
#include "oled.h"

#define _KEYON 0
#define _KEYOFF 1

#define WAIT_TIME_MS_NUM 2
#define WAIT_KEY_TIME_A  160
#define WAIT_KEY_TIME_B   80
#define WAIT_KEY_TIME_C   30

/*
typedef enum
{
    EVENT_NONE_KEY,
    EVENT_KEY_CLICK,
    EVENT_KEY_DOUBLE_CLICK,
    EVENT_KEY_PRESS,
    EVENT_KEY_RELEASE,           // >1.2S
} enumKeyType;
*/
#define     EVENT_NONE_KEY       0
#define     EVENT_KEY_CLICK          1
#define     EVENT_KEY_DOUBLE_CLICK    2
#define     EVENT_KEY_PRESS          3
#define     EVENT_KEY_RELEASE        4   // >1.2S
extern volatile uint8_t nowKeyType;
//extern volatile uint8_t flagKEY;

extern volatile uint32_t _5MsCount;
extern volatile uint32_t lastCountTIM;

typedef struct
{
    uint8_t runKeyFlag:2;       // 0: Stop  1: Run
    uint8_t lastKeyVal:1;       // LastKeyVal = 0 | 1
//    uint8_t nowKeyVal:1;        // LastKeyVal = 0 | 1
    uint8_t runKeyMode:3;       // 000b: noneKey 001b: click 010b: doubleClick
    // 011b: press   100b: release
    uint8_t nKeyRe:2;         //
    uint16_t kOnTime;
    uint16_t kOffTime;
} _RUNKEYFLAG;
extern volatile _RUNKEYFLAG mRunKeyFlag;

uint8_t getNMsEvent(uint8_t index);
void clrNMsEvent(uint8_t index);

typedef struct
{
    uint8_t phaseALastVal:1;        //A相的上一次电平
    uint8_t phaseBLastVal:1;        //B相的上一次电平
    uint8_t phaseALastChange:1;     //A相的电平变化0:降 1:升
    uint8_t phaseBLastChange:1;     //B相的电平变化0:降 1:升
    uint8_t phaseAUseFlag:1;
    uint8_t phaseBUseFlag:1;
    uint8_t nextChangeVal:1;        //下一次找的电平变化。
    uint8_t encoderDir:1;
//    uint8_t firstIn:1;
//    uint8_t reserve:2;
    int8_t  encoderVal;
} _ENCODER_FLAG;
extern volatile _ENCODER_FLAG mEncoderFlag;

void uartInit(UART_TypeDef * uartName,uint32_t fu32Baudrate, uint32_t fu32Mode);
void printZ(UART_TypeDef * UARTx, char * str);
void encoderInit(void);
void delayMs(volatile uint32_t u32DelayMsNum);
void delayUs(volatile uint32_t u32DelayNum);
uint32_t crcCount(uint8_t * crcBuf);

void BSPInit(void);

#endif      //__DRVMCU_H

