#include "SWM190.h"
#include "oled.h"
#include "oledfont.h"

uint8_t OledGRAM[144][8];

//反显函数
void OledColorTurn(uint8_t i)
{
    if(i==0)
    {
        OledWR_Byte(0xA6,OledCMD);//正常显示
    }
    if(i==1)
    {
        OledWR_Byte(0xA7,OledCMD);//反色显示
    }
}

//屏幕旋转180度
void OledDisplayTurn(uint8_t i)
{
    if(i==0)
    {
        OledWR_Byte(0xC8,OledCMD);//正常显示
        OledWR_Byte(0xA1,OledCMD);
    }
    if(i==1)
    {
        OledWR_Byte(0xC0,OledCMD);//反转显示
        OledWR_Byte(0xA0,OledCMD);
    }
}


void OledWR_Byte(uint8_t dat,uint8_t cmd)
{
    uint8_t i;
    if(cmd)
        OledDCSet();
    else
        OledDCClr();
    OledCSClr();
    for(i=0; i<8; i++)
    {
        OledSCLKClr();
        if(dat&0x80)
            OledSDIN_Set();
        else
            OledSDIN_Clr();
        OledSCLKSet();
        dat<<=1;
    }
    OledCSSet();
    OledDCSet();
}

//开启OLED显示
void OledDisPlay_On(void)
{
    OledWR_Byte(0x8D,OledCMD);//电荷泵使能
    OledWR_Byte(0x14,OledCMD);//开启电荷泵
    OledWR_Byte(0xAF,OledCMD);//点亮屏幕
}

//关闭OLED显示
void OledDisPlay_Off(void)
{
    OledWR_Byte(0x8D,OledCMD);//电荷泵使能
    OledWR_Byte(0x10,OledCMD);//关闭电荷泵
    OledWR_Byte(0xAF,OledCMD);//关闭屏幕
}

void lcdBLKClr(void)
{
    OledDisPlay_Off();
}

void lcdBLKSet(void)
{
    OledDisPlay_On();
}

//更新显存到OLED
void OledRefresh(void)
{
    uint8_t i,n;
    for(i=0; i<8; i++)
    {
        OledWR_Byte(0xb0+i,OledCMD); //设置行起始地址
        OledWR_Byte(0x00,OledCMD);   //设置低列起始地址
        OledWR_Byte(0x10,OledCMD);   //设置高列起始地址
        for(n=0; n<LCD_W; n++)
            OledWR_Byte(OledGRAM[n][i],OledDATA);
    }
}

//清屏函数
void OledClear(void)
{
    uint8_t i,n;
    for(i=0; i<8; i++)
    {
        for(n=0; n<LCD_W; n++)
        {
            OledGRAM[n][i]=0;//清除所有数据
        }
    }
    OledRefresh();//更新显示
}

//画点
//x:0~(LCD_W-1)
//y:0~(LCD_H-1)
void OledDrawPoint(uint8_t x,uint8_t y)
{
    uint8_t i,m,n;
    i=y/8;
    m=y%8;
    n=1<<m;
    OledGRAM[x][i]|=n;
}

void lcdPoint(uint16_t x,uint16_t y,uint16_t color)
{
    OledDrawPoint(x,y);
}

//清除一个点
//x:0~(LCD_W-1)
//y:0~(LCD_H-1)
void OledClearPoint(uint8_t x,uint8_t y)
{
    uint8_t i,m,n;
    i=y/8;
    m=y%8;
    n=1<<m;
    OledGRAM[x][i]=~OledGRAM[x][i];
    OledGRAM[x][i]|=n;
    OledGRAM[x][i]=~OledGRAM[x][i];
}


//画线
//x:0~LCD_W
//y:0~LCD_H
void OledDrawLine(uint8_t x1,uint8_t y1,uint8_t x2,uint8_t y2)
{
    uint8_t i,k,k1,k2;			//,y0;
//    if((x1<0)||(x2>LCD_W)||(y1<0)||(y2>LCD_H)||(x1>x2)||(y1>y2))
    if((x2>LCD_W)||(y2>LCD_H)||(x1>x2)||(y1>y2))
        return;
    if(x1==x2)    //画竖线
    {
        for(i=0; i<(y2-y1); i++)
        {
            OledDrawPoint(x1,y1+i);
        }
    }
    else if(y1==y2)   //画横线
    {
        for(i=0; i<(x2-x1); i++)
        {
            OledDrawPoint(x1+i,y1);
        }
    }
    else      //画斜线
    {
        k1=y2-y1;
        k2=x2-x1;
        k=k1*10/k2;
        for(i=0; i<(x2-x1); i++)
        {
            OledDrawPoint(x1+i,y1+i*k/10);
        }
    }
}
//x,y:圆心坐标
//r:圆的半径
void OledDrawCircle(uint8_t x,uint8_t y,uint8_t r)
{
    int a, b,num;
    a = 0;
    b = r;
    while(2 * b * b >= r * r)
    {
        OledDrawPoint(x + a, y - b);
        OledDrawPoint(x - a, y - b);
        OledDrawPoint(x - a, y + b);
        OledDrawPoint(x + a, y + b);

        OledDrawPoint(x + b, y + a);
        OledDrawPoint(x + b, y - a);
        OledDrawPoint(x - b, y - a);
        OledDrawPoint(x - b, y + a);

        a++;
        num = (a * a + b * b) - r*r;//计算画的点离圆心的距离
        if(num > 0)
        {
            b--;
            a--;
        }
    }
}



//在指定位置显示一个字符,包括部分字符
//x:0~(LCD_W-1)
//y:0~(LCD_H-1)
//size:选择字体 12/16/24
//取模方式 逐列式
void OledShowChar(uint8_t x,uint8_t y,uint8_t chr,uint8_t size1)
{
    uint8_t i,m,temp,size2,chr1;
    uint8_t y0=y;
    size2=(size1/8+((size1%8)?1:0))*(size1/2);  //得到字体一个字符对应点阵集所占的字节数
    chr1=chr-' ';  //计算偏移后的值
    for(i=0; i<size2; i++)
    {
        if(size1==12)
        {
            temp=asc2_1206[chr1][i];   //调用1206字体
        }
        else if(size1==16)
        {
            temp=asc2_1608[chr1][i];   //调用1608字体
        }
        else if(size1==24)
        {
            temp=asc2_2412[chr1][i];   //调用2412字体
        }
        else return;
        for(m=0; m<8; m++)         //写入数据
        {
            if(temp&0x80)OledDrawPoint(x,y);
            else OledClearPoint(x,y);
            temp<<=1;
            y++;
            if((y-y0)==size1)
            {
                y=y0;
                x++;
                break;
            }
        }
    }
}


//显示字符串
//x,y:起点坐标
//size1:字体大小
//*chr:字符串起始地址
void OledShowString(uint8_t x,uint8_t y,uint8_t *chr,uint8_t size1)
{
    while((*chr>=' ')&&(*chr<='~'))//判断是不是非法字符!
    {
        OledShowChar(x,y,*chr,size1);
        x+=size1/2;
        if(x>LCD_W-size1)  //换行
        {
            x=0;
            y+=2;
        }
        chr++;
    }
}

//m^n
uint32_t OledPow(uint8_t m,uint8_t n)
{
    uint32_t result=1;
    while(n--)
    {
        result*=m;
    }
    return result;
}

////显示2个数字
////x,y :起点坐标
////len :数字的位数
////size:字体大小
void OledShowNum(uint8_t x,uint8_t y,uint32_t num,uint8_t len,uint8_t size1)
{
    uint8_t t,temp;
    for(t=0; t<len; t++)
    {
        temp=(num/OledPow(10,len-t-1))%10;
        if(temp==0)
        {
            OledShowChar(x+(size1/2)*t,y,'0',size1);
        }
        else
        {
            OledShowChar(x+(size1/2)*t,y,temp+'0',size1);
        }
    }
}

//显示汉字
//x,y:起点坐标
//num:汉字对应的序号
//取模方式 列行式
void OledShowChinese(uint8_t x,uint8_t y,uint8_t num,uint8_t size1)
{
    uint8_t i,m,n=0,temp,chr1;
    uint8_t x0=x,y0=y;
    uint8_t size3=size1/8;
    while(size3--)
    {
        chr1=num*size1/8+n;
        n++;
        for(i=0; i<size1; i++)
        {
            if(size1==16)
            {
                temp=Hzk1[chr1][i];   //调用16*16字体
            }
            else if(size1==24)
            {
                temp=Hzk2[chr1][i];   //调用24*24字体
            }
            else if(size1==32)
            {
                temp=Hzk3[chr1][i];   //调用32*32字体
            }
            else if(size1==64)
            {
                temp=Hzk4[chr1][i];   //调用64*64字体
            }
            else return;

            for(m=0; m<8; m++)
            {
                if(temp&0x01)OledDrawPoint(x,y);
                else OledClearPoint(x,y);
                temp>>=1;
                y++;
            }
            x++;
            if((x-x0)==size1)
            {
                x=x0;
                y0=y0+8;
            }
            y=y0;
        }
    }
}

//num 显示汉字的个数
//space 每一遍显示的间隔
void OledScrollDisplay(uint8_t num,uint8_t space)
{
    uint8_t i,n,t=0,m=0,r;
    while(1)
    {
        if(m==0)
        {
            OledShowChinese(LCD_W,24,t,16); //写入一个汉字保存在OledGRAM[][]数组中
            t++;
        }
        if(t==num)
        {
            for(r=0; r<16*space; r++)    //显示间隔
            {
                for(i=0; i<144; i++)
                {
                    for(n=0; n<8; n++)
                    {
                        OledGRAM[i-1][n]=OledGRAM[i][n];
                    }
                }
                OledRefresh();
            }
            t=0;
        }
        m++;
        if(m==16)
        {
            m=0;
        }
        for(i=0; i<144; i++) //实现左移
        {
            for(n=0; n<8; n++)
            {
                OledGRAM[i-1][n]=OledGRAM[i][n];
            }
        }
        OledRefresh();
    }
}

//配置写入数据的起始位置
void OledWR_BP(uint8_t x,uint8_t y)
{
    OledWR_Byte(0xb0+y,OledCMD);//设置行起始地址
    OledWR_Byte(((x&0xf0)>>4)|0x10,OledCMD);
    OledWR_Byte((x&0x0f),OledCMD);
}

//x0,y0：起点坐标
//x1,y1：终点坐标
//BMP[]：要写入的图片数组
void OledShowPicture(uint8_t x0,uint8_t y0,uint8_t x1,uint8_t y1,uint8_t BMP[])
{
    uint32_t j=0;
    uint8_t x=0,y=0;
    if(y%8==0)y=0;
    else y+=1;
    for(y=y0; y<y1; y++)
    {
        OledWR_BP(x0,y);
        for(x=x0; x<x1; x++)
        {
            OledWR_Byte(BMP[j],OledDATA);
            j++;
        }
    }
}
//OLED的初始化
void OledInit(void)
{
//    GPIO_InitTypeDef  GPIO_InitStructure;
//    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOG|RCC_APB2Periph_GPIOD|RCC_APB2Periph_GPIOE, ENABLE);	 //使能A端口时钟
//    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
//    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
//    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;//速度50MHz
//    GPIO_Init(GPIOG, &GPIO_InitStructure);	  //初始化GPIOG12
//    GPIO_SetBits(GPIOG,GPIO_Pin_12);

//    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1|GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_15;
//    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
//    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;//速度50MHz
//    GPIO_Init(GPIOD, &GPIO_InitStructure);	  //初始化GPIOD1,5,15
//    GPIO_SetBits(GPIOD,GPIO_Pin_1|GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_15);

//    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8|GPIO_Pin_10;
//    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
//    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;//速度50MHz
//    GPIO_Init(GPIOE, &GPIO_InitStructure);	  //初始化GPIOE8,10
//    GPIO_SetBits(GPIOE,GPIO_Pin_8|GPIO_Pin_10);

//    OledRES_Clr();
//    delay_ms(200);
//    OledRES_Set();

    OledWR_Byte(0xAE,OledCMD);//--turn off oled panel
    OledWR_Byte(0x00,OledCMD);//---set low column address
    OledWR_Byte(0x10,OledCMD);//---set high column address
    OledWR_Byte(0x40,OledCMD);//--set start line address  Set Mapping RAM Display Start Line (0x00~0x3F)
    OledWR_Byte(0x81,OledCMD);//--set contrast control register
    OledWR_Byte(0xCF,OledCMD);// Set SEG Output Current Brightness
    OledWR_Byte(0xA1,OledCMD);//--Set SEG/Column Mapping     0xa0左右反置 0xa1正常
    OledWR_Byte(0xC8,OledCMD);//Set COM/Row Scan Direction   0xc0上下反置 0xc8正常
    OledWR_Byte(0xA6,OledCMD);//--set normal display
    OledWR_Byte(0xA8,OledCMD);//--set multiplex ratio(1 to LCD_H)
    OledWR_Byte(0x3f,OledCMD);//--1/LCD_H duty
    OledWR_Byte(0xD3,OledCMD);//-set display offset	Shift Mapping RAM Counter (0x00~0x3F)
    OledWR_Byte(0x00,OledCMD);//-not offset
    OledWR_Byte(0xd5,OledCMD);//--set display clock divide ratio/oscillator frequency
    OledWR_Byte(0x80,OledCMD);//--set divide ratio, Set Clock as 100 Frames/Sec
    OledWR_Byte(0xD9,OledCMD);//--set pre-charge period
    OledWR_Byte(0xF1,OledCMD);//Set Pre-Charge as 15 Clocks & Discharge as 1 Clock
    OledWR_Byte(0xDA,OledCMD);//--set com pins hardware configuration
    OledWR_Byte(0x12,OledCMD);
    OledWR_Byte(0xDB,OledCMD);//--set vcomh
    OledWR_Byte(0x40,OledCMD);//Set VCOM Deselect Level
    OledWR_Byte(0x20,OledCMD);//-Set Page Addressing Mode (0x00/0x01/0x02)
    OledWR_Byte(0x02,OledCMD);//
    OledWR_Byte(0x8D,OledCMD);//--set Charge Pump enable/disable
    OledWR_Byte(0x14,OledCMD);//--set(0x10) disable
    OledWR_Byte(0xA4,OledCMD);// Disable Entire Display On (0xa4/0xa5)
    OledWR_Byte(0xA6,OledCMD);// Disable Inverse Display On (0xa6/a7)
    OledWR_Byte(0xAF,OledCMD);
    OledClear();
}
