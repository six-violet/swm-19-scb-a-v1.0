# 六菫科技SWM19S开发板Demo例子01.OLED

## 程序是板子对应的演示。
	目前已经有如下：
	GPIO
	UART
	OLED(GPIO 或 SPI)
	I2C(AW9523G)
	
## Taobao Link：淘宝链接：

[华芯微特SWM19SCBT开发板](https://item.taobao.com/item.htm?ft=t&id=727695718084)

###### 如有疑问请发Email:dawoo@6violet.com

![](SWM19SCB_Top.JPG)

## 开发板如上图所示，J4的短路环不要短路！

## 初始程序，显示是两个不同的屏的切换！

## 板载的模块与主要功能：

### 下面是产品的图片。

![](SWM19SCB_TopTxt.JPG)

```
1，128X64的单色OLED显示屏，可用于显示文字与图形信息。
2，扩展的8个RGB三色的LED灯理论上可以显示256级电流，共16777216种颜色。
3，扩展的8个按键 输入。同上LED的芯片（AW9523B）
4，ADC输入：一路光线强度的输入，一路电位器的电压输入。
5，USB2.0转串口，MCU使用的是UART2。开发板上的芯片是使用了CH340G。
6，电源有2个接口（a,TypeC Usb接口，b,DC-DC05接口），但使用中电源只能接一个，不能同时接2个电源。电源有在电路上做保护措施。TypeC USB加了防静电高压芯片
7，半双工的RS485接口，增加了通信方向指示。
8，SPI串行Flash存储芯片。
板子大小6.85CMX9.95CM
```
## 3DFile step
[PCB_STEP]("./HardwareV1.0/SWM19SCB_SVDBV1_0.step")

