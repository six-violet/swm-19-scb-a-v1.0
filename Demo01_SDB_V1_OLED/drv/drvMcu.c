/* www.6violet.com
 * FileName drvMcu.c
 * creat time: 2023-8-27 by dawoo.Zheng
 *    The last modifier: dawoo.Zheng
 * The last change time: 2023-8-27
 * commit:
 */

#include "drvMcu.h"
#include "drvGpio.h"
//#include "displayConfig.h"
//#include "drvTimEncoder.h"
//#include "drvIWDT.h"

#define TIM_CLOCK_FREQ (10000)
#define MAXCRCBUF 256
#define _MAX_ENCODER_COUNT_ 256

uint8_t crcDataBuf[MAXCRCBUF];
//TIM_HandleTypeDef tim6Handler;
volatile uint32_t mKeyPressFlag;

volatile uint32_t lastCountTIM;

volatile uint8_t nowKeyType;
//volatile uint8_t flagKEY;
volatile _RUNKEYFLAG mRunKeyFlag;
//volatile _ENCODER_FLAG mEncoderFlag;
volatile uint8_t pKeyTime,rKeyTime;

//TIM_HandleTypeDef	timxHandler;
//TIM_Encoder_InitTypeDef	timxEncoderInitHandler;

typedef struct
{
    uint8_t flagNMs1:1;       //
    uint8_t flagNMs2:1;       //
    uint8_t flagNMs3:1;       //
    uint8_t flagNMs4:1;       //
    uint8_t flagNMs5:1;       //
    uint8_t flagNMs6:1;       //
    uint8_t flagNMs7:1;       //
    uint8_t flagNMs8:1;       //
} _FLAG_NMs ;
volatile _FLAG_NMs mFlagNm;
volatile uint32_t _5MsCount;

void mcuInit(void)
{
    InitAllPin();
}

//Nms Flag:
uint8_t getNMsEvent(uint8_t index)
{
    uint8_t reVal;
    switch(index)
    {
        case 0:
            reVal = mFlagNm.flagNMs1;
            break;
        case 1:
            reVal = mFlagNm.flagNMs2;
            break;
        case 2:
            reVal = mFlagNm.flagNMs3;
            break;
        case 3:
            reVal = mFlagNm.flagNMs4;
            break;
        case 4:
            reVal = mFlagNm.flagNMs5;
            break;
        case 5:
            reVal = mFlagNm.flagNMs6;
            break;
        case 6:
            reVal = mFlagNm.flagNMs7;
            break;
        case 7:
            reVal = mFlagNm.flagNMs8;
            break;
        default:
            reVal = 0;
            break;
    }
    return reVal;
}

void clrNMsEvent(uint8_t index)
{
    switch(index)
    {
        case 0:
            mFlagNm.flagNMs1 = 0;
            break;
        case 1:
            mFlagNm.flagNMs2 = 0;
            break;
        case 2:
            mFlagNm.flagNMs3 = 0;
            break;
        case 3:
            mFlagNm.flagNMs4 = 0;
            break;
        case 4:
            mFlagNm.flagNMs5 = 0;
            break;
        case 5:
            mFlagNm.flagNMs6 = 0;
            break;
        case 6:
            mFlagNm.flagNMs7 = 0;
            break;
        case 7:
            mFlagNm.flagNMs8 = 0;
            break;
        default:
            break;
    }
}

/// Ex: delayMs(100);
void delayMs(volatile uint32_t u32DelayMsNum)
{
//    System_Delay_MS(u32DelayMsNum);
}

void delayUs(volatile uint32_t u32DelayNum)
{
//    System_Delay(u32DelayNum);
}

void crcDataInit(void)
{
    uint32_t i;
    for(i = 0; i < MAXCRCBUF-2; i++ )
    {
        crcDataBuf[i] = i;
    }
}

uint16_t strSizeC(uint8_t *strBuf)
{
    uint8_t * str;
    uint16_t count = 0;
    str = strBuf;
    while(count < 65000)
    {
        if(*str++ == 0)
            return (uint16_t)(str-strBuf-1);
    }
    return 0;
}


int fputc(int ch, FILE *f)
{
    UART_WriteByte(UART0, ch);
    while(UART_IsTXBusy(UART0));
    return ch;
}

void BSPInit(void)
{
    mcuInit();
    OledInit();
}

