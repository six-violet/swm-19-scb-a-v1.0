/* www.6violet.com
 * FileName drvGpio.c
 * creat time: 2023-8-27 by dawoo.Zheng
 *    The last modifier: dawoo.Zheng
 * The last change time: 2023-12-07
 * commit:
 * ------------------------------------------------------------------------------------
*/

#include "drvGpio.h"

const _SV_SGpio pAOut[]=
{
    {GPIOA,PIN0},
    {GPIOA,PIN2},
    {GPIOA,PIN3},
    {GPIOA,PIN8},
    {GPIOA,PIN9},
    {GPIOA,PIN10},
    {GPIOA,PIN11},
    {GPIOA,PIN12},
    {GPIOA,PIN13},
    {GPIOA,PIN14},
    {GPIOA,PIN15},
};

const _SV_SGpio pBOut[]=
{
    {GPIOB,PIN0},
    {GPIOB,PIN1},
    {GPIOB,PIN2},
    {GPIOB,PIN3},
    {GPIOB,PIN4},
    {GPIOB,PIN5},
    {GPIOB,PIN6},
    {GPIOB,PIN7},
    {GPIOB,PIN8},
    {GPIOB,PIN9},
    {GPIOB,PIN10},
    {GPIOB,PIN11},
    {GPIOB,PIN12},
    {GPIOB,PIN13},
    {GPIOB,PIN14},
    {GPIOB,PIN15},
};

const _SV_SGpio pCOut[]=
{
    {GPIOC,PIN0},
    {GPIOC,PIN2},
    {GPIOC,PIN3},
    {GPIOC,PIN4},
    {GPIOC,PIN5},
    {GPIOC,PIN6},
    {GPIOC,PIN7},
};

const _SV_SGpio pDOut[]=
{
    {GPIOD,PIN0},
    {GPIOD,PIN1},
};

const _SV_SGpio pEOut[]=
{
    {GPIOE,PIN2},
    {GPIOE,PIN3},
    {GPIOE,PIN4},
    {GPIOE,PIN5},
    {GPIOE,PIN7},
};
//定义GPIO的对应关系：
const _SV_SGpio lcdMosiport= {GPIOB, PIN5};
const _SV_SGpio lcdSClkport= {GPIOB, PIN6};
const _SV_SGpio lcdCSport= {GPIOB, PIN3};
//const _SV_SGpio lcdBK= {GPIOD, PIN9};
const _SV_SGpio lcdDCport= {GPIOB, PIN4};
const _SV_SGpio rs485dir= {GPIOC, PIN6};
const _SV_SGpio inKeyInt= {GPIOC, PIN5};

void InitAllPin(void)
{
//  …AAA…………………………………………………………………………………………………………………………………………………………………………………\
//  |AAA            设置GPIO的功能                          |              |
//  |AAA      PortA|  PIN?| Fuction?            |IE?|       |              |
    PORT_Init(PORTA,  PIN0,      PORTA_PIN0_GPIO, 0);       //输出:GPIO-A00
    PORT_Init(PORTA,  PIN2,     PORTA_PIN2_SWCLK, 0);       //SWCLK
    PORT_Init(PORTA,  PIN3,     PORTA_PIN3_SWDIO, 0);       //SWDIO
    PORT_Init(PORTA,  PIN8,      PORTA_PIN8_GPIO, 0);       //输入:GPIO-A08
    PORT_Init(PORTA,  PIN9,      PORTA_PIN9_GPIO, 0);       //输出:GPIO-A09
    PORT_Init(PORTA, PIN10,     PORTA_PIN10_GPIO, 0);       //输出:GPIO-A10
    PORT_Init(PORTA, PIN11,     PORTA_PIN11_GPIO, 0);       //输出:GPIO-A11
    PORT_Init(PORTA, PIN12,     PORTA_PIN12_GPIO, 0);       //输出:GPIO-A12
    PORT_Init(PORTA, PIN13,     PORTA_PIN13_GPIO, 0);       //输出:GPIO-A13
    PORT_Init(PORTA, PIN14, PORTA_PIN14_ADC0_IN2, 0);       //
    PORT_Init(PORTA, PIN15, PORTA_PIN15_ADC0_IN1, 0);       //
//==========================================================================
//  |AAA            设置GPIO的方向与特性          |              |
//  |AAA           |      |方|上|下|开|           |              |
//  |AAA      PortA|      |向|拉|拉|漏|           |              |
    GPIO_Init(GPIOA,  PIN0, 1, 1, 0, 0);          //
//    GPIO_Init(GPIOA,  PIN2, 1, 1, 0, 0);          //
//    GPIO_Init(GPIOA,  PIN3, 1, 1, 0, 0);          //
    GPIO_Init(GPIOA,  PIN8, 0, 1, 0, 0);          //输入:
    GPIO_Init(GPIOA,  PIN9, 1, 1, 0, 0);          //输出:
    GPIO_Init(GPIOA, PIN10, 1, 1, 0, 0);          //输出:
    GPIO_Init(GPIOA, PIN11, 1, 1, 0, 0);          //输出:
    GPIO_Init(GPIOA, PIN12, 1, 1, 0, 0);          //输出:
    GPIO_Init(GPIOA, PIN13, 1, 1, 0, 0);          //输出:
//    GPIO_Init(GPIOA, PIN14, 0, 1, 0, 0);          //输:
//    GPIO_Init(GPIOA, PIN15, 0, 1, 0, 0);          //输:

//  …BBB…………………………………………………………………………………………………………………………………………………………………………………\
//  |BBB            设置GPIO的功能                          |              |
//  |BBB      PortB|  PIN?| Fuction?            |IE?|       |              |
    PORT_Init(PORTB,  PIN0,      PORTB_PIN0_GPIO, 0);       //输出:GPIO-B00
    PORT_Init(PORTB,  PIN1,      PORTB_PIN1_GPIO, 0);       //输出:GPIO-B01
    PORT_Init(PORTB,  PIN2,      PORTB_PIN2_GPIO, 0);       //输出:GPIO-B02
    PORT_Init(PORTB,  PIN3,      PORTB_PIN3_GPIO, 0);       //输出:GPIO-B03
    PORT_Init(PORTB,  PIN4,      PORTB_PIN4_GPIO, 0);       //输出:GPIO-B04
    PORT_Init(PORTB,  PIN5,      PORTB_PIN5_GPIO, 0);       //
    PORT_Init(PORTB,  PIN6,      PORTB_PIN6_GPIO, 0);       //
    PORT_Init(PORTB,  PIN5, PORTB_PIN5_SPI0_MOSI, 0);       //
    PORT_Init(PORTB,  PIN6, PORTB_PIN6_SPI0_SCLK, 0);       //
    PORT_Init(PORTB,  PIN7,  PORTB_PIN7_UART2_RX, 0);       //
    PORT_Init(PORTB,  PIN8,  PORTB_PIN8_UART2_TX, 0);       //
    PORT_Init(PORTB,  PIN9,      PORTB_PIN9_GPIO, 0);       //输出:GPIO-B09
    PORT_Init(PORTB, PIN10,     PORTB_PIN10_GPIO, 0);       //输出:GPIO-B10
    PORT_Init(PORTB, PIN11,     PORTB_PIN11_GPIO, 0);       //输出:GPIO-B11
    PORT_Init(PORTB, PIN12,     PORTB_PIN12_GPIO, 0);       //输出:GPIO-B12
    PORT_Init(PORTB, PIN13,     PORTB_PIN13_GPIO, 0);       //输出:GPIO-B13
    PORT_Init(PORTB, PIN14, PORTB_PIN14_I2C0_SDA, 0);       //I2C0:SDA
    PORT_Init(PORTB, PIN15, PORTB_PIN15_I2C0_SCL, 0);       //I2C0:SCL
//==========================================================================
//  |BBB            设置GPIO的方向与特性          |              |
//  |BBB           |      |方|上|下|开|           |              |
//  |BBB      PortB|      |向|拉|拉|漏|           |              |
    GPIO_Init(GPIOB,  PIN0, 1, 1, 0, 0);          //
    GPIO_Init(GPIOB,  PIN1, 1, 1, 0, 0);          //
    GPIO_Init(GPIOB,  PIN2, 1, 1, 0, 0);          //
    GPIO_Init(GPIOB,  PIN3, 1, 1, 0, 0);          //
    GPIO_Init(GPIOB,  PIN4, 1, 1, 0, 0);          //
    GPIO_Init(GPIOB,  PIN5, 1, 1, 0, 0);          //
    GPIO_Init(GPIOB,  PIN6, 1, 1, 0, 0);          //
//    GPIO_Init(GPIOB,  PIN7, 1, 1, 0, 0);          //
//    GPIO_Init(GPIOB,  PIN8, 1, 1, 0, 0);          //
    GPIO_Init(GPIOB,  PIN9, 1, 1, 0, 0);          //
    GPIO_Init(GPIOB, PIN10, 1, 1, 0, 0);          //
    GPIO_Init(GPIOB, PIN11, 1, 1, 0, 0);          //
    GPIO_Init(GPIOB, PIN12, 1, 1, 0, 0);          //
    GPIO_Init(GPIOB, PIN13, 1, 1, 0, 0);          //
//    GPIO_Init(GPIOB, PIN14, 1, 1, 0, 0);          //
//    GPIO_Init(GPIOB, PIN15, 1, 1, 0, 0);          //

//  …CCC…………………………………………………………………………………………………………………………………………………………………………………\
//  |CCC            设置GPIO的功能                          |              |
//  |CCC      PortC|  PIN?| Fuction?            |IE?|       |              |
//    PORT_Init(PORTC,  PIN0,  PORTC_PIN0_XTAL_OUT, 0);       //
//    PORT_Init(PORTC,  PIN1,   PORTC_PIN1_XTAL_IN, 0);       //
    PORT_Init(PORTC,  PIN2,      PORTC_PIN2_GPIO, 0);       //输出:GPIO-C02
    PORT_Init(PORTC,  PIN3,      PORTC_PIN3_GPIO, 0);       //输出:GPIO-C03
    PORT_Init(PORTC,  PIN4,      PORTC_PIN4_GPIO, 0);       //输入:LED_INT
    PORT_Init(PORTC,  PIN5,      PORTC_PIN5_GPIO, 0);       //输入:KEY_INT
    PORT_Init(PORTC,  PIN6,      PORTC_PIN6_GPIO, 0);       //输出:GPIO-C06
    PORT_Init(PORTC,  PIN7,      PORTC_PIN7_GPIO, 0);       //输出:GPIO-C07
//==========================================================================
//  |CCC            设置GPIO的方向与特性          |              |
//  |CCC           |      |方|上|下|开|           |              |
//  |CCC      PortC|      |向|拉|拉|漏|           |              |
//    GPIO_Init(GPIOC,  PIN0, 1, 1, 0, 0);          //
//    GPIO_Init(GPIOC,  PIN1, 0, 1, 0, 0);          //
    GPIO_Init(GPIOC,  PIN2, 1, 1, 0, 0);          //输出:GPIO-C02
    GPIO_Init(GPIOC,  PIN3, 1, 1, 0, 0);          //输出:GPIO-C03
    GPIO_Init(GPIOC,  PIN4, 0, 1, 0, 0);          //输入:LED_INT
    GPIO_Init(GPIOC,  PIN5, 0, 1, 0, 0);          //输入:KEY_INT
    GPIO_Init(GPIOC,  PIN6, 1, 1, 0, 0);          //输出:GPIO-C06
    GPIO_Init(GPIOC,  PIN7, 1, 1, 0, 0);          //输出:GPIO-C07

//  …DDD…………………………………………………………………………………………………………………………………………………………………………………\
//  |DDD            设置GPIO的功能                          |              |
//  |DDD      PortD|  PIN?| Fuction?            |IE?|       |              |
    PORT_Init(PORTD,  PIN0,  PORTD_PIN0_UART0_RX, 0);       //UART0:Rxd
    PORT_Init(PORTD,  PIN1,  PORTD_PIN1_UART0_TX, 0);       //UART0:Txd
//==========================================================================
//  |DDD            设置GPIO的方向与特性          |              |
//  |DDD           |      |方|上|下|开|           |              |
//  |DDD      PortD|      |向|拉|拉|漏|           |              |
//    GPIO_Init(GPIOD,  PIN0, 1, 1, 0, 0);          //输出:GPIO-D00
//    GPIO_Init(GPIOD,  PIN1, 1, 1, 0, 0);          //输出:GPIO-D01

//  …EEE…………………………………………………………………………………………………………………………………………………………………………………\
//  |EEE            设置GPIO的功能                          |              |
//  |EEE      PortE|  PIN?| Fuction?            |IE?|       |              |
    PORT_Init(PORTE,  PIN2,      PORTE_PIN2_GPIO, 0);       //输出:GPIO-E02
    PORT_Init(PORTE,  PIN3,      PORTE_PIN3_GPIO, 0);       //输出:GPIO-E03
    PORT_Init(PORTE,  PIN4,      PORTE_PIN4_GPIO, 0);       //输出:GPIO-E02
    PORT_Init(PORTE,  PIN5,      PORTE_PIN5_GPIO, 0);       //输出:GPIO-E03
    PORT_Init(PORTE,  PIN7,      PORTE_PIN7_GPIO, 0);       //输出:GPIO-E03
//==========================================================================
//  |EEE            设置GPIO的方向与特性          |              |
//  |EEE           |      |方|上|下|开|           |              |
//  |EEE      PortE|      |向|拉|拉|漏|           |              |
    GPIO_Init(GPIOE,  PIN2, 1, 1, 0, 0);          //输出:GPIO-E02
    GPIO_Init(GPIOE,  PIN3, 1, 1, 0, 0);          //输出:GPIO-E03
    GPIO_Init(GPIOE,  PIN4, 1, 1, 0, 0);          //输出:GPIO-E02
    GPIO_Init(GPIOE,  PIN5, 1, 1, 0, 0);          //输出:GPIO-E03
    GPIO_Init(GPIOE,  PIN7, 1, 1, 0, 0);          //输出:GPIO-E03
// ...................................................................... //
//   GPIO Init
// ...................................................................... //
}

void SetDrvP(_SV_SGpio dp)
{
    GPIO_SetBit(dp.ioPort,dp.ioPin);
}

void ClrDrvP(_SV_SGpio dp)
{
    GPIO_ClrBit(dp.ioPort,dp.ioPin);
}

void InvDrvP(_SV_SGpio dp)
{
    GPIO_InvBit(dp.ioPort,dp.ioPin);
}

uint32_t GetInvP(_SV_SGpio dp)
{
    return GPIO_GetBit(dp.ioPort, dp.ioPin);
}

