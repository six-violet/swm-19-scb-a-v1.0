#ifndef __OledH
#define __OledH

#include "SWM190.h"
#include "stdlib.h"

#define LCD_W 128
#define LCD_H 64

//-----------------OLED端口定义----------------

#define OledSCLKClr() GPIO_ClrBit(GPIOB,PIN6)//SCL
#define OledSCLKSet() GPIO_SetBit(GPIOB,PIN6)

#define OledSDIN_Clr() GPIO_ClrBit(GPIOB,PIN5)//DIN
#define OledSDIN_Set() GPIO_SetBit(GPIOB,PIN5)

//#define OledRES_Clr() GPIO_ClrBit(GPIOB,GPIO_Pin_4)//RES
//#define OledRES_Set() GPIO_SetBit(GPIOB,GPIO_Pin_4)

#define OledDCClr() GPIO_ClrBit(GPIOB,PIN4)//DC
#define OledDCSet() GPIO_SetBit(GPIOB,PIN4)

#define OledCSClr()  GPIO_ClrBit(GPIOB,PIN3)//CS
#define OledCSSet()  GPIO_SetBit(GPIOB,PIN3)

#define OledCMD  0	//写命令
#define OledDATA 1	//写数据

#define uint8_t unsigned char
#define uint32_t unsigned int

void OledClearPoint(uint8_t x,uint8_t y);
void OledColorTurn(uint8_t i);
void OledDisplayTurn(uint8_t i);
void OledWR_Byte(uint8_t dat,uint8_t cmd);
void OledDisPlay_On(void);
void OledDisPlay_Off(void);
void OledRefresh(void);
void OledClear(void);
void OledDrawPoint(uint8_t x,uint8_t y);
void OledDrawLine(uint8_t x1,uint8_t y1,uint8_t x2,uint8_t y2);
void OledDrawCircle(uint8_t x,uint8_t y,uint8_t r);
void OledShowChar(uint8_t x,uint8_t y,uint8_t chr,uint8_t size1);
void OledShowString(uint8_t x,uint8_t y,uint8_t *chr,uint8_t size1);
void OledShowNum(uint8_t x,uint8_t y,uint32_t num,uint8_t len,uint8_t size1);
void OledShowChinese(uint8_t x,uint8_t y,uint8_t num,uint8_t size1);
void OledScrollDisplay(uint8_t num,uint8_t space);
void OledWR_BP(uint8_t x,uint8_t y);
void OledShowPicture(uint8_t x0,uint8_t y0,uint8_t x1,uint8_t y1,uint8_t BMP[]);
void OledInit(void);

void lcdPoint(uint16_t x,uint16_t y,uint16_t color);

#endif
