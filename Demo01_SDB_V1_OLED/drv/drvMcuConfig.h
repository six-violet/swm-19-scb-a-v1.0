/* www.6violet.com
 * FileName drvGpioConfig.h
 * creat time: 2023-12-07 by dawoo.Zheng
 *    The last modifier: dawoo.Zheng
 * The last change time: 2023-12-07
 * commit:
 * ------------------------------------------------------------------------------------
 * 需要在devMCU.h中定义的功能：（按需求）
 * ------------------------------------------------------------------------------------
 * ------------------------------------------------------------------------------------
 */

#ifndef _DRVGPIOCONFIG_H_
#define _DRVGPIOCONFIG_H_

//#include "stdint.h"
#include "SWM190.h"
#include "SWM190_port.h"
#include "SWM190_gpio.h"

#define _USE_SV_GPIO_ 1             //如果使用SV的GPIO则定义为1
#define _USE_SV_IIC0_ 0             //如果使用SV的IIC0则定义为1
#define _USE_SV_SPI0_ 0             //如果使用SV的SPI0则定义为1
#define _USE_SV_OLED_ 0             //如果使用SV的OLED则定义为1
//#define _USE_SV_GPIO_ 1             //如果使用SV的GPIO则定义为1

#if(_USE_SV_GPIO_ == 1)
#include "drvGpio.h"
///设置自己的GPIO类型
typedef GPIO_TypeDef SV_GPIO_TYPE;
#endif

#if(_USE_SV_IIC0_ == 1)
#include "drvExI2C.h"
#include "AW9523B.h"
#include "displayRes.h"
#endif

#if(_USE_SV_SPI0_ == 1)
#endif

#endif          ///_DRVGPIOCONFIG_H_

