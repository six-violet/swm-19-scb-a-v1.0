# 六菫科技SWM19S开发板Demo例子00.GPIO

## 本例子的程序是板子上的LED灯的亮灭变化演示。

###### 如有疑问请发Email:dawoo@6violet.com

## ![](mainBoard.jpg)

## ![](SWM19SCB_TopTxt.JPG)

## 开发板如上图所示，J4的短路环不要短路！

## 板子上一个三色的LED,红色的是电源指示，在RS485边上的两个LED灯是指示RS485的方向。
