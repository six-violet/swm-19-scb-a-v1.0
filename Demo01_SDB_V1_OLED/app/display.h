/* www.6violet.com
 * FileName display.h
 * creat time: 2023-8-28 by dawoo.Zheng
 *    The last modifier: dawoo.Zheng
 * The last change time: 2023-8-28
 * commit:
 */

#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#include "drvMcu.h"
#include "oled.h"

extern const int16_t cSinCosList[][2];
extern void lcdPoint(uint16_t x, uint16_t y, uint16_t color);

//画笔颜色
#define WHITE               0xFFFF
#define BLACK               0x0000
#define BLUE                0x001F
#define BRED                0XF81F
#define GRED                0XFFE0
#define GBLUE               0X07FF
#define RED                 0xF800
#define MAGENTA             0xF81F
#define GREEN               0x07E0
#define CYAN                0x7FFF
#define YELLOW              0xFFE0
#define BROWN               0XBC40 //棕色
#define BRRED               0XFC07 //棕红色
#define GRAY                0X8430 //灰色
#define DARKBLUE            0X01CF //深蓝色
#define LIGHTBLUE           0X7D7C //浅蓝色  
#define GRAYBLUE            0X5458 //灰蓝色
#define LIGHTGREEN          0X841F //浅绿色
#define LGRAY               0XC618 //浅灰色(PANNEL),窗体背景色
#define LGRAYBLUE           0XA651 //浅灰蓝色(中间层颜色)
#define LBBLUE              0X2B12 //浅棕蓝色(选择条目的反色)

//Init LCD
void displayInit(void);

//关掉背光
void displayOff(void);

//打开背光
void displayOn(void);
//void displayReverse(void);
//
void setDefBKColorVal(uint16_t bkcolor);
void setDefFGColorVal(uint16_t fgcolor);

//---------------------------------------------------o
//  单独画    |   X坐    |    Y坐    |    16Bit      |
//  点程序    |   标点   |    标点   |    颜色       |
void drawPoint(uint16_t x, uint16_t y, uint16_t color);

//------------------------------------------------------------------------------o
//    画线   |    X1坐   |    Y1坐    |    X2坐    |    Y2坐    |    16Bit      |
//    函数   |    标点   |    标点    |    标点    |    标点    |    颜色       |
void drawLine(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color);

//-------------------------------------------------------------------------------o
//    画方形      |   X1坐   |    Y1坐   |   X方向   |   Y方向   |     16Bit     |
//    线宽1       |   标点   |    标点   |   宽度    |   宽度    |     颜色      |
void drawRectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color);

//--------------------------------------------------------------------------------------------o
//    画方形          |   X1坐   |    Y1坐   |   X方向   |   Y方向   |    X==Y   |    16Bit      |
//    线宽z点         |   标点   |    标点   |   宽度    |   宽度    |    宽度Z  |    颜色       |
void drawRectangleEdge(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t z, uint16_t color);

//-------------------------------------------------------------------------------o
//    画方形      |   X1坐   |    Y1坐   |   X方向   |   Y方向   |     16Bit     |
//    全填充      |   标点   |    标点   |   宽度    |   宽度    |     颜色      |
void fillRectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color);

//-----------------------------------------------------------------------------------o
//    画环形   |  圆心坐   |   圆心坐   |     半径长      |  圆环的  |     16Bit     |
//    线宽w点  |  标点X    |   标点Y    |     r像素       |   宽度   |     颜色      |
void drawCircle(uint16_t ox, uint16_t oy, uint16_t rPixcel, uint8_t w, uint16_t color);

//---------扇形的圆心是常量，const uint16_t centreX,centreY;-----------------------------o
//    画扇形         |  起始角  |   终止角  |     半径长      |  扇形的  |     16Bit     |
//    线宽w点        |  [1,360] |  [1,360]  |     r像素       |   宽度   |     颜色      |
void drawCircleSector(uint16_t a, uint16_t b, uint16_t rPixcel, uint8_t w, uint16_t color);

//--------------------------------------------------------------------------------o
//    画16色  |  左上坐  |   左上坐  |  图像    |   图像    |      16Bit颜色      |
//    彩图    |  标点X   |   标点Y   |  宽度    |   高度    |      数据指针       |
void drawImage(uint16_t x, uint16_t y,uint16_t w, uint16_t h, unsigned char * data);

//---------------------------------------------------------------------------------------o
//    画16色         |  左上坐  |   左上坐  |  图像    |   图像    |      16Bit颜色      |
//    彩图           |  标点X   |   标点Y   |  宽度    |   高度    |      数据指针       |
void drawImageHyaline(uint16_t x, uint16_t y,uint16_t w, uint16_t h, unsigned char * data);

//----------------------------------------------------------------------------------------------o
//    画16色                |  左上坐  |   左上坐  |  图像    |   图像    |      16Bit颜色      |
//    彩图                  |  标点X   |   标点Y   |  宽度    |   高度    |      数据指针       |
void drawImageHyalineOctagon(uint16_t x, uint16_t y,uint16_t w, uint16_t h, unsigned char * data);

//----------------------------------------------------------------------------------------------------------------------------o
//    画单色            |  左上坐  |   左上坐  |  图像    |   图像    |      1Bit颜色       |     16Bit前    |     16Bit背    |
//    图填充            |  标点X   |   标点Y   |  宽度    |   高度    |      数据指针       |     景颜色     |     景颜色     |
void drawImageBlackwhite(uint16_t x, uint16_t y,uint16_t w, uint16_t h, unsigned char * data, uint16_t tColor, uint16_t bColor);

//-----------------------------------------------------------------------------------------------------------------------------------o
//    画单色                   |  左上坐  |   左上坐  |  图像    |   图像    |      1Bit颜色       |     16Bit前    |     16Bit背    |
//    图填充                   |  标点X   |   标点Y   |  宽度    |   高度    |      数据指针       |     景颜色     |     景颜色     |
void drawImageBlackwhiteOctagon(uint16_t x, uint16_t y,uint16_t w, uint16_t h, unsigned char * data, uint16_t tColor, uint16_t bColor);

//---------------------------------------------------------------------------------------------------o
//  显示     |     字体       |   左上坐  |   左上坐  |   文字编   |     16Bit前    |     16Bit背    |
//  文字     |     高度       |   标点X   |   标点Y   |   码指针   |     景颜色     |     景颜色     |
void drawText(uint8_t fontSize, uint16_t x, uint16_t y, char * data, uint16_t tColor, uint16_t bColor);

//---------------------------------------------------------------------------------------------------o
// 显示有符  |     字体       |   左上坐  |   左上坐  |   8bit     |     16Bit前    |     16Bit背    |
// 号8bit数  |     高度       |   标点X   |   标点Y   |   数据     |     景颜色     |     景颜色     |
void drawInt8(uint8_t fontSize, uint16_t x, uint16_t y, int8_t data, uint16_t tColor, uint16_t bColor);

//------------------------------------------------------------------------------------------------------o
// 显示无符    |     字体       |   左上坐  |   左上坐  |   16bit     |     16Bit前    |     16Bit背    |
// 号16bit数   |     高度       |   标点X   |   标点Y   |   数据      |     景颜色     |     景颜色     |
void drawUint16(uint8_t fontSize, uint16_t x, uint16_t y,uint16_t data, uint16_t tColor, uint16_t bColor);

//----------------------------------------------------------------------------------------------------------o
//   显示单       |      字体       |   左上坐  |   左上坐  |    文字编   |     16Bit前    |     16Bit背    |
//   个半字       |      高度       |   标点X   |   标点Y   |    码数据   |     景颜色     |     景颜色     |
void drawAsciiChar(uint16_t fontSize, uint16_t x, uint16_t y, uint8_t data, uint16_t tColor, uint16_t bColor);

//---------------------------------------------------------------------------------------------------------------o
//    显示单        |      字体       |   左上坐  |   左上坐  |     文字编     |     16Bit前    |     16Bit背    |
//    个汉字        |      高度       |   标点X   |   标点Y   |     码数据     |     景颜色     |     景颜色     |
void drawChineseChar(uint16_t fontSize, uint16_t x, uint16_t y, uint16_t hzCode, uint16_t tColor, uint16_t bColor);


#endif  //__DISPLAY_H__
