/* www.6violet.com
 * FileName display.c
 * creat time: 2023-8-27 by dawoo.Zheng
 *    The last modifier: dawoo.Zheng
 * The last change time: 2023-8-27
 * commit:
 */

#include "display.h"

#include "fontLib.h"
//#include "icon.h"
#include "cSinCosVal.h"

#define _MAX_RANK_ 6
#define _FONT_LIB_BUF_  2048    //32*32*2=2048

//#define __AUTO_RETURN_DISPLAY_

unsigned char dispRam[_FONT_LIB_BUF_];
const uint16_t centreX = 120;
const uint16_t centreY = 120;
uint16_t indexRank[_MAX_RANK_][2];
uint16_t indexNum;
uint16_t bkSetColVal;
uint16_t fgSetColVal;

uint8_t autoBeginEndArea;
void setreOutAreaVal(void)
{
    autoBeginEndArea = 1;
}

void clrreOutAreaVal(void)
{
    autoBeginEndArea = 0;
}

void setDefBKColorVal(uint16_t bkcolor)
{
    bkSetColVal = bkcolor;
}

void setDefFGColorVal(uint16_t fgcolor)
{
    fgSetColVal = fgcolor;
}

uint8_t reValInArea(uint16_t a,uint16_t b,uint16_t c)
{
    uint8_t reA;
    if(autoBeginEndArea == 1)
    {
        reA = 0;
    }
    else
    {
        reA = 1;
    }
    if(c < a || c > b)
        return reA;
    else
        return autoBeginEndArea;
}


void plotRegion(uint16_t a,uint16_t b)
{
    const uint16_t cRAngle[]= {89,179,269,359};
    if(a < cRAngle[0])
    {
        if( b <= cRAngle[0])
        {
            indexRank[indexNum][0] = a;
            indexRank[indexNum][1] = b;
            indexNum ++;
        }
        else if(b <= cRAngle[1])
        {
            indexRank[indexNum][0] = a;
            indexRank[indexNum][1] = cRAngle[0];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[0]+1;
            indexRank[indexNum][1] = b;
            indexNum ++;
        }
        else if(b <= cRAngle[2])
        {
            indexRank[indexNum][0] = a;
            indexRank[indexNum][1] = cRAngle[0];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[0]+1;
            indexRank[indexNum][1] = cRAngle[1];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[1]+1;
            indexRank[indexNum][1] = b;
            indexNum ++;
        }
        else
        {
            indexRank[indexNum][0] = a;
            indexRank[indexNum][1] = cRAngle[0];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[0]+1;
            indexRank[indexNum][1] = cRAngle[1];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[1]+1;
            indexRank[indexNum][1] = cRAngle[2];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[2]+1;
            indexRank[indexNum][1] = b;
            indexNum ++;
        }
    }
    else if(a<cRAngle[1])
    {
        if(b <= cRAngle[1])
        {
            indexRank[indexNum][0] = a;
            indexRank[indexNum][1] = b;
            indexNum ++;
        }
        else if(b <= cRAngle[2])
        {
            indexRank[indexNum][0] = a;
            indexRank[indexNum][1] = cRAngle[1];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[1]+1;
            indexRank[indexNum][1] = b;
            indexNum ++;
        }
        else
        {
            indexRank[indexNum][0] = a;
            indexRank[indexNum][1] = cRAngle[1];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[1]+1;
            indexRank[indexNum][1] = cRAngle[2];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[2]+1;
            indexRank[indexNum][1] = b;
            indexNum ++;
        }
    }
    else if(a<cRAngle[2])
    {
        if(b <= cRAngle[2])
        {
            indexRank[indexNum][0] = a;
            indexRank[indexNum][1] = b;
            indexNum ++;
        }
        else
        {
            indexRank[indexNum][0] = a;
            indexRank[indexNum][1] = cRAngle[2];
            indexNum ++;
            indexRank[indexNum][0] = cRAngle[2]+1;
            indexRank[indexNum][1] = b;
            indexNum ++;
        }
    }
    else if(a<cRAngle[3])
    {
        indexRank[indexNum][0] = a;
        indexRank[indexNum][1] = b;
        indexNum ++;
    }
}

uint8_t computeA2BRegion(uint16_t a, uint16_t b)
{
    uint16_t i;
    if( a == b)
        return 0;
    for(i=0; i<_MAX_RANK_; i++)
    {
        indexRank[i][0] = 0;
        indexRank[i][1] = 0;
    }
    indexNum = 0;
    if(a > b)
    {
        //begin b>a
        plotRegion(0,b);
        plotRegion(a,359);
    }
    else
    {
        //begin a>b
        plotRegion(a,b);
    }
    return 1;
}

uint8_t compute4Quadrant(int16_t x, int16_t y, int16_t indexRA0, int16_t indexRA1, int16_t indexRB0, int16_t indexRB1)
{
    if((indexRA0 > 0) && (indexRA1 > 0) && x > 0 && y > 0 && (indexRB0 > 0) && (indexRB1 > 0))
    {
        return 1;
    }
    if((indexRA0 < 0) && (indexRA1 > 0) && x < 0 && y > 0 && (indexRB0 < 0) && (indexRB1 > 0))
    {
        return 2;
    }
    if((indexRA0 < 0) && (indexRA1 < 0) && x < 0 && y < 0 && (indexRB0 < 0) && (indexRB1 < 0))
    {
        return 3;
    }
    if((indexRA0 > 0) && (indexRA1 < 0) && x > 0 && y < 0 && (indexRB0 >= 0) && (indexRB1 < 0))
    {
        return 4;
    }
    return 0;
}

uint8_t computeABC(int16_t x, int16_t y, int16_t indexRA0, int16_t indexRA1, int16_t indexRB0, int16_t indexRB1)
{
    uint8_t quadrant;
    int16_t tX,tY;
    double dA,dB,dC;
    tX = x - centreX;
    tY = centreY - y;
    if( tX == 0)
        tX = 1;
    quadrant = compute4Quadrant(tX, tY, indexRA0, indexRA1, indexRB0, indexRB1);
    if(quadrant == 0)
        return 0;
    dA = ((double)tY/tX);
    dB = ((double)indexRA0/indexRA1);
    dC = ((double)indexRB0/indexRB1);
    if(dA > dB && dA < dC)
        return 1;
    if(dA < dB && dA > dC)
        return 1;
    else
        return 0;
}

void displayInit(void)
{
    OledInit();
}

void displayClean(void)
{
}

void displayOff(void)
{
//    lcdBLKSet();
}

void displayOn(void)
{
//    lcdBLKClr();
}

void displayReverse(void)
{
}

void dispRam2S(uint16_t x, uint16_t y,uint16_t xEnd,uint16_t yEnd, uint16_t sizeByte)
{
    //lcdPositionSet(x,y,xEnd,yEnd);
    //lcdWritBusXB(dispRam,sizeByte*2);
}

void dispRam2SData(uint16_t x, uint16_t y, uint16_t xEnd, uint16_t yEnd, unsigned char * dataRam, uint16_t sizeByte)
{
    //lcdPositionSet(x,y,xEnd,yEnd);
    //lcdWritBusXB(dataRam,sizeByte*2);
}

uint16_t findHzIndex(uint16_t hzCode)
{
    uint16_t i,a,b,re = 0;
    uint16_t fdHzCode;
//    uint8_t * tmpData;
    a=0;
    b=_HZ_NUM_;
    i=b/2;
    while(a != b)
    {
        if((a+1) == b)
        {
            if(i == a)
            {
                i=b;
                a=b;
            }
            else
            {
                i=a;
                b=a;
            }
        }
        fdHzCode = (uint16_t)((hzIndex[i][0])<<8) + (hzIndex[i][1]);
        if(fdHzCode == hzCode)
        {
            re = i;
            break;
        }
        else
        {
            if(fdHzCode > hzCode)
            {
                b = i;
            }
            else
            {
                a = i;
            }
            i = (a+b)/2;
        }
    }
    return re;
}

//---------------------------------------------------o
//    画点    |   X坐    |    Y坐    |    16Bit      |
//    函数    |   标点   |    标点   |    颜色       |
void drawPoint(uint16_t x, uint16_t y, uint16_t color)
{
    lcdPoint( x,y,color);
    //lcdPositionSet(x,y,x,y);   //设置光标位置
    //lcdWritBus16(color);
}

//------------------------------------------------------------------------------o
//    画线   |    X1坐   |    Y1坐    |    X2坐    |    Y2坐    |    16Bit      |
//    函数   |    标点   |    标点    |    标点    |    标点    |    颜色       |
void drawLine(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color)
{
    uint16_t t;
    int16_t xerr=0,yerr=0,delta_x,delta_y,distance;
    int16_t incx,incy,uRow,uCol;
    delta_x=x2-x1;                      //计算坐标增量
    delta_y=y2-y1;
    uRow=x1;                            //画线起点坐标
    uCol=y1;
    if(delta_x>0)
        incx=1;                         //设置单步方向
    else if (delta_x==0)
        incx=0;                         //垂直线
    else
    {
        incx=-1;
        delta_x=-delta_x;
    }
    if(delta_y>0)
        incy=1;
    else if(delta_y==0)
        incy=0;                         //水平线
    else
    {
        incy=-1;
        delta_y=-delta_y;
    }
    if(delta_x>delta_y)
        distance=delta_x;               //选取基本增量坐标轴
    else
        distance=delta_y;
    for(t=0; t < (uint16_t)(distance+1); t++)
    {
        drawPoint(uRow,uCol,color); //画点
        xerr+=delta_x;
        yerr+=delta_y;
        if(xerr>distance)
        {
            xerr-=distance;
            uRow+=incx;
        }
        if(yerr>distance)
        {
            yerr-=distance;
            uCol+=incy;
        }
    }
}

//-------------------------------------------------------------------------------o
//    画方形      |   X1坐   |    Y1坐   |   X方向   |   Y方向   |     16Bit     |
//    线宽1       |   标点   |    标点   |   宽度    |   宽度    |     颜色      |
void drawRectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color)
{
    drawLine(x,y,x+w,y,color);
    drawLine(x,y,x,y+h,color);
    drawLine(x,y+h,x+w,y+h,color);
    drawLine(x+w,y,x+w,y+h,color);
}

//-------------------------------------------------------------------------------o
//    画方形      |   X1坐   |    Y1坐   |   X方向   |   Y方向   |     16Bit     |
//    全填充      |   标点   |    标点   |   宽度    |   宽度    |     颜色      |
void fillRectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color)
{
    uint8_t cTmp[2];
    uint16_t i,j;   //row,remain;
    cTmp[0] = (uint8_t)(color>>8 & 0xff);
    cTmp[1] = (uint8_t)(color & 0xff);
    //lcdPositionSet(x,y,x+w-1,y+h-1);//设置显示范围
    for(i=0; i<LCD_W; i++)
    {
        dispRam[i*2] = cTmp[0];
        dispRam[i*2+1] = cTmp[1];
    }
//    row = w*h/LCD_W;
    for(j = 0; j< h; j++)
    {
//        for(i = 0; i< w; i++)
//        {
        //lcdWritBusXB( (unsigned char *)dispRam, w*2 );
//        }
    }
    /*
    remain = (w*h)%LCD_W;
    lcdCSClr();
    for(i=0; i<row; i++)
    {
        HAL_SPI_Transmit(&lcdSpiHandle, (uint8_t *)dispRam, LCD_W*2, 0);
    }
    HAL_SPI_Transmit(&lcdSpiHandle, (uint8_t *)dispRam, remain*2, 0);
    lcdCSSet();
    */
}

//--------------------------------------------------------------------------------------------o
//    画方形          |   X1坐   |    Y1坐   |   X方向   |   Y方向   |    X==Y   |    16Bit      |
//    线宽z点         |   标点   |    标点   |   宽度    |   宽度    |    宽度Z  |    颜色       |
void drawRectangleEdge(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t z, uint16_t color)
{
    if((z >(w/2)) || (z>(h/2)))
        return ;
    fillRectangle(x, y, z, h, color);
    fillRectangle(x+z, y, w-z-z, z, color);
    fillRectangle(x+z, y+h-z, w-z-z, z, color);
    fillRectangle(x+w-z, y, z, h, color);
}

uint16_t analyseInRegion( uint16_t a, uint16_t b, uint16_t rPixcel, uint8_t w, uint16_t ax, uint16_t ay, uint16_t color)
{
    uint16_t i,j,tmp,maxZ,minZ;
    uint16_t re,aa,bb;
    uint16_t xBeg,xEnd;
    int16_t tBx,tBy,tEx,tEy;
    minZ = rPixcel-w;
    maxZ = rPixcel;
    re = LCD_W;
    for(i = 0; i < LCD_H*2; i++)
    {
        dispRam[i*2] = (unsigned char )(color>>8);
        dispRam[i*2+1] = (unsigned char )(color&0x00ff);
    }
    setreOutAreaVal();
    for(i= ax; i< LCD_W; i++)
    {
        aa = (centreX > i) ? (centreX-i) : (i-centreX);
        bb = (centreY > ay) ? (centreY-ay) : (ay-centreY);
        if(reValInArea(minZ*minZ,maxZ*maxZ,aa*aa+bb*bb))
        {
            re = i;
            break;
        }
    }
    if( re == LCD_W)
        return re;
    else
    {
        clrreOutAreaVal();
        xBeg = re;
        re = LCD_W;
        for(i= xBeg; i< LCD_W; i++)
        {
            aa = (centreX > i) ? (centreX-i) : (i-centreX);
            bb = (centreY > ay) ? (centreY-ay) : (ay-centreY);
            if(reValInArea(minZ*minZ,maxZ*maxZ,aa*aa+bb*bb))
            {
                re = i;
                break;
            }
        }
        xEnd = re;

    }
    if(( xBeg > xEnd) || (xEnd > LCD_W))
        return LCD_W;
    tmp = computeA2BRegion( a, b);
    if(tmp == 0)
        return LCD_W;
    for(re = xEnd,i = xBeg; i < (uint16_t)xEnd; i++)
    {
        for(j = 0; j< _MAX_RANK_; j++)
        {
            if((indexRank[j][0] == 0) && (indexRank[j][1] == 0))
                break;
            tBx = cSinCosList[indexRank[j][0]][0];
            tBy = cSinCosList[indexRank[j][0]][1];
            tEx = cSinCosList[indexRank[j][1]][0];
            tEy = cSinCosList[indexRank[j][1]][1];
            tmp = computeABC( i, ay, tBx, tBy, tEx, tEy);
            if((ay == 60) && (i == 125))
                aa = 0;
            if(tmp == 0)
                continue;
            else
            {
                re = i;
                i = xEnd;
                break;
            }
        }
    }
    xBeg = re;
    if(re >= xEnd)
        return xEnd;
    else
    {
        for(re = xEnd,i= xBeg; i< (uint16_t)xEnd; i++)
        {
            for(j = 0; j< _MAX_RANK_; j++)
            {
                if((indexRank[j][0] == 0) && (indexRank[j][1] == 0))
                    break;
                tBx = cSinCosList[indexRank[j][0]][0];
                tBy = cSinCosList[indexRank[j][0]][1];
                tEx = cSinCosList[indexRank[j][1]][0];
                tEy = cSinCosList[indexRank[j][1]][1];
                tmp = computeABC( i, ay, tBx, tBy, tEx, tEy);
                if((ay == 37) && (i > 125))
                    aa = 0;
                if(tmp == 1)
                    continue;
                else
                {
                    re = i;
                    i = xEnd;
                    break;
                }
            }
        }
    }
    if(re >= LCD_W)
        return LCD_W;
    else
    {
        xEnd = re;
        dispRam2S(xBeg,ay,xEnd,ay,xEnd-xBeg+1);
    }
    return re;
}

int reOutArea(uint16_t a,uint16_t b,uint16_t c)
{
    if(c < a || c > b)
        return 1;
    else
        return 0;
}

int16_t findInCircle(uint16_t ox, uint16_t oy, uint16_t rPixcel, uint8_t w, uint16_t findx, uint16_t findy)
{
    uint16_t i,re,maxZ,minZ;
    uint16_t aa,bb;
    minZ = rPixcel-w;
    maxZ = rPixcel;
    re = LCD_W-1;
    for(i= findx; i< LCD_W; i++)
    {
        aa = (ox > i) ? (ox-i) : (i-ox);
        bb = (oy > findy) ? (oy-findy) : (findy-oy);
        if(reValInArea(minZ*minZ,maxZ*maxZ,aa*aa+bb*bb))
        {
            re = i;
            break;
        }
    }
    return re;
}

//-----------------------------------------------------------------------------------o
//    画环形   |  圆心坐   |   圆心坐   |     半径长      |  圆环的  |     16Bit     |
//    线宽w点  |  标点X    |   标点Y    |     r像素       |   宽度   |     颜色      |
void drawCircle(uint16_t ox, uint16_t oy, uint16_t rPixcel, uint8_t w, uint16_t color)
{
    uint16_t ix,iy;
    int16_t flagA,flagB;
    if(w<1 || w>99 || rPixcel<w)
        return;
    for(iy = 0; iy < LCD_H; iy++)
    {
        dispRam[iy*2] = (unsigned char )(color>>8);
        dispRam[iy*2+1] = (unsigned char )(color&0x00ff);
    }
    for(iy = 0; iy < LCD_H; iy++)
    {
        flagA = 0;
        flagB = 0;
        for(ix = 0; ix < LCD_W; ix++)
        {
            setreOutAreaVal();
            flagA = findInCircle(ox, oy, rPixcel, w, ix, iy);
//            flagA = findInCircleBegin(ox, oy, rPixcel, w, ix, iy);
            if(flagA == (LCD_W-1))
                continue;
            ix =flagA;
            clrreOutAreaVal();
            flagB = findInCircle(ox, oy, rPixcel, w, ix, iy);
//            flagB = findInCircleEnd(ox, oy, rPixcel, w, ix, iy);
            ix =flagB;
            dispRam2S(flagA,iy,flagB,iy,flagB-flagA+1);
        }
    }
}

//---------扇形的圆心是常量，const uint16_t centreX,centreY;-----------------------------o
//    画扇形         |  起始角  |   终止角  |     半径长      |  扇形的  |     16Bit     |
//    线宽w点        |  [1,360] |  [1,360]  |     r像素       |   宽度   |     颜色      |
void drawCircleSector(uint16_t a, uint16_t b, uint16_t rPixcel, uint8_t w, uint16_t color)
{
    uint16_t ix,iy;
    int16_t flagC;
    if(w<1 || w>99 || rPixcel<w)
        return;
    for(iy = 0; iy < LCD_H; iy++)
    {
        for(ix = 0; ix < LCD_W; ix++)
        {
            if(iy == 61)
                flagC = 0;
            flagC = analyseInRegion( a, b, rPixcel, w, ix, iy, color);
            if(flagC >= LCD_W )
            {
                break;
            }
            else
            {
                ix = flagC;
            }
            flagC = analyseInRegion( a, b, rPixcel, w, ix, iy, color);
            if(flagC < LCD_W)
                continue;
            ix = flagC;
        }
    }
}

//--------------------------------------------------------------------------------o
//    画16色  |  左上坐  |   左上坐  |  图像    |   图像    |      16Bit颜色      |
//    彩图    |  标点X   |   标点Y   |  宽度    |   高度    |      数据指针       |
void drawImage(uint16_t x, uint16_t y,uint16_t w, uint16_t h, unsigned char * data)
{
    //lcdPositionSet(x,y,x+w-1,y+h-1);
    //lcdWritBusXB(data, w*h*2);
    /*
        lcdCSClr();
        HAL_SPI_Transmit(&lcdSpiHandle, (uint8_t *)data, w*h*2, 0);
        lcdCSSet();
    */
}

//---------------------------------------------------------------------------------------o
//    画16色         |  左上坐  |   左上坐  |  图像    |   图像    |      16Bit颜色      |
//    彩图           |  标点X   |   标点Y   |  宽度    |   高度    |      数据指针       |
void drawImageHyaline(uint16_t x, uint16_t y,uint16_t w, uint16_t h, unsigned char * data)
{
    uint16_t j,k;
    unsigned char bkCol[2];
    unsigned char * ramTmp;
    ramTmp = data;
    bkCol[0] = (bkSetColVal>>8) & 0xff;
    bkCol[1] = bkSetColVal & 0xff;
    //lcdPositionSet(x,y,x+w-1,y+h-1);
    for(j = 0; j < h ; j++)
    {
        for(k = 0; k < w ; k++)
        {
            dispRam[k*2] = *ramTmp++;
            dispRam[k*2+1] = *ramTmp++;
            if( (dispRam[k*2] == 0xff) && (dispRam[k*2+1] == 0xff))
            {
                dispRam[k*2] = bkCol[0];
                dispRam[k*2+1] = bkCol[1];
            }
        }
//        dispRam2S(x,y+j,x+w-1,y+j, w*2);
//        HAL_SPI_Transmit(&lcdSpiHandle, (uint8_t *)dispRam, w*2, 0);
        //lcdWritBusXB((unsigned char *)dispRam, w*2);
    }
//    lcdCSSet();
}

//----------------------------------------------------------------------------------------------o
//    画16色                |  左上坐  |   左上坐  |  图像    |   图像    |      16Bit颜色      |
//    彩图                  |  标点X   |   标点Y   |  宽度    |   高度    |      数据指针       |
void drawImageHyalineOctagon(uint16_t x, uint16_t y,uint16_t w, uint16_t h, unsigned char * data)
{
    /*
    uint16_t j,k;
    unsigned char bkCol[2];
    unsigned char * ramTmp;
    uint16_t oct,octTmp;
    if(x > LCD_W || y > LCD_H)
        return;
    (w <= h)? (oct = w) : (oct = h);
    oct *= 19;
    oct /= 64;
    if( (oct >= (w/2)) || (oct >= (h/2)) )
        return;
    if((x+w)> LCD_W )
        w = (LCD_W - x);
    if((y+h)> LCD_H )
        h = (LCD_H - y);
    ramTmp = data;
    bkCol[0] = (bkSetColVal>>8) & 0xff;
    bkCol[1] = bkSetColVal & 0xff;
    for(j = 0; j < h ; j++)
    {
        for(k = 0; k < w ; k++)
        {
            dispRam[k*2] = *ramTmp++;
            dispRam[k*2+1] = *ramTmp++;
            if( (dispRam[k*2] == 0xff) && (dispRam[k*2+1] == 0xff))
            {
                dispRam[k*2] = bkCol[0];
                dispRam[k*2+1] = bkCol[1];
            }
        }
        if( (j < oct) || (( h-j+1) < oct) )
        {
            if(j < oct)
                octTmp = oct - j;
            else
                octTmp = oct - (h-j+1);
            //lcdPositionSet(x+octTmp,y+j,x+w-octTmp,y+j);
            //lcdWritBusXB((unsigned char *)(dispRam+ (octTmp*2)), (w-octTmp-octTmp)*2);
            ///#
                        lcdCSClr();
                        HAL_SPI_Transmit(&lcdSpiHandle, (uint8_t *)(dispRam+ (octTmp*2)), (w-octTmp-octTmp)*2, 0);
                        lcdCSSet();
            //#/
        }
        else
        {
    //            dispRam2S(x,y+j,x+w,y+j,w*8*2);
            //lcdPositionSet(x,y+j,x+w,y+j);
            //lcdWritBusXB((unsigned char *)dispRam, w*2);
            //#
                        lcdCSClr();
                        HAL_SPI_Transmit(&lcdSpiHandle, (uint8_t *)dispRam, w*2, 0);
                        lcdCSSet();
            //#/
        }
    }
    */
}

//----------------------------------------------------------------------------------------------------------------------------o
//    画单色            |  左上坐  |   左上坐  |  图像    |   图像    |      1Bit颜色       |     16Bit前    |     16Bit背    |
//    图填充            |  标点X   |   标点Y   |  宽度    |   高度    |      数据指针       |     景颜色     |     景颜色     |
void drawImageBlackwhite(uint16_t x, uint16_t y,uint16_t w, uint16_t h, unsigned char * data, uint16_t tColor, uint16_t bColor)
{
    uint16_t i,j,k;
    const unsigned char BITM[8]= {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};
    uint8_t tCols[2],bCols[2];
    tCols[0] = (uint8_t)(tColor >>8)& 0xFF;
    tCols[1] = (uint8_t)(tColor & 0xFF);
    bCols[0] = (uint8_t)(bColor >>8)& 0xFF;
    bCols[1] = (uint8_t)(bColor & 0xFF);
    //lcdPositionSet(x,y,x+w-1,y+h-1);
//    lcdCSClr();
    for(k=0; k < h; k++)
    {
        for(j=0; j < w; j++)
        {
            for(i=0; i < 8; i++)
            {
                if((*(unsigned char *)(data + (w>>3)*k + j) & BITM[i]) != 0)
                {
                    dispRam[j*16+i*2] = tCols[0];
                    dispRam[j*16+i*2+1] = tCols[1];
                }
                else
                {
                    dispRam[j*16+i*2] = bCols[0];
                    dispRam[j*16+i*2+1] = bCols[1];
                }
            }
        }
//        HAL_SPI_Transmit(&lcdSpiHandle, (uint8_t *)dispRam, w*2, 0);
        //lcdWritBusXB( (unsigned char *)dispRam, w*2);
    }
//    lcdCSSet();
}

//-----------------------------------------------------------------------------------------------------------------------------------o
//    画单色                   |  左上坐  |   左上坐  |  图像    |   图像    |      1Bit颜色       |     16Bit前    |     16Bit背    |
//    图填充                   |  标点X   |   标点Y   |  宽度    |   高度    |      数据指针       |     景颜色     |     景颜色     |
void drawImageBlackwhiteOctagon(uint16_t x, uint16_t y,uint16_t w, uint16_t h, unsigned char * data, uint16_t tColor, uint16_t bColor)
{
    /*
    uint16_t i,j,k;
    const unsigned char BITM[8]= {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};
    uint8_t tCols[2],bCols[2];
    uint16_t oct,octTmp;
    if(x > LCD_W || y > LCD_H)
        return;
    (w <= h)? (oct = w) : (oct = h);
    oct *= 19;
    oct /= 64;
    if( (oct >= (w/2)) || (oct >= (h/2)) )
        return;
    if((x+w)> LCD_W )
        w = (LCD_W - x);
    if((y+h)> LCD_H )
        h = (LCD_H - y);
    tCols[0] = (uint8_t)(tColor >>8)& 0xFF;
    tCols[1] = (uint8_t)(tColor & 0xFF);
    bCols[0] = (uint8_t)(bColor >>8)& 0xFF;
    bCols[1] = (uint8_t)(bColor & 0xFF);
    octTmp = oct;
    //	octB = w - oct;
    //    lcdPositionSet(x,y,x+w-1,y+h-1);
    //    lcdCSClr();
    for(k=0; k < h; k++)
    {
        for(j=0; j < w; j++)
        {
            for(i=0; i < 8; i++)
            {
                if((*(unsigned char *)(data + (w>>3)*k + j) & BITM[i]) != 0)
                {
                    dispRam[j*16+i*2] = tCols[0];
                    dispRam[j*16+i*2+1] = tCols[1];
                }
                else
                {
                    dispRam[j*16+i*2] = bCols[0];
                    dispRam[j*16+i*2+1] = bCols[1];
                }
            }
        }
        if( (k < oct) || (( h-k+1) < oct) )
        {
            if(k < oct)
                octTmp = oct - k;
            else
                octTmp = oct - (h-k+1);
            //lcdPositionSet(x+octTmp,y+k,x+w-octTmp,y+k);
            //lcdWritBusXB( (unsigned char *)(dispRam+ (octTmp*2)), (w-octTmp-octTmp)*2 );
            /#
                        lcdCSClr();
                        HAL_SPI_Transmit(&lcdSpiHandle, (uint8_t *)(dispRam+ (octTmp*2)), (w-octTmp-octTmp)*2, 0);
                        lcdCSSet();
            #/
        }
        else
        {
    //			dispRam2S(x,y+k,x+w,y+k,w*8*2);
            //lcdPositionSet(x,y+k,x+w,y+k);
            //lcdWritBusXB( (unsigned char *)dispRam, w*2 );
            /#
                        lcdCSClr();
                        HAL_SPI_Transmit(&lcdSpiHandle, (uint8_t *)dispRam, w*2, 0);
                        lcdCSSet();
            #/
        }
        //dispRam2S(x,y+k,x+w,y+k,w*8*2);
    }
    */
//    lcdCSSet();
}

//----------------------------------------------------------------------------------------------------------o
//   显示单       |      字体       |   左上坐  |   左上坐  |    文字编   |     16Bit前    |     16Bit背    |
//   个半字       |      高度       |   标点X   |   标点Y   |    码数据   |     景颜色     |     景颜色     |
void drawAsciiChar(uint16_t fontSize, uint16_t x, uint16_t y, uint8_t data, uint16_t tColor, uint16_t bColor)
{
    /*
    const unsigned char BITM[8]= {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};
    uint16_t i,j;
    unsigned char * libIndex;
    uint8_t libA,libB;
    uint32_t libStep,libSize;
    uint8_t tCols[2],bCols[2];
    tCols[0] = (uint8_t)(tColor >>8)& 0xFF;
    tCols[1] = (uint8_t)(tColor & 0xFF);
    bCols[0] = (uint8_t)(bColor >>8)& 0xFF;
    bCols[1] = (uint8_t)(bColor & 0xFF);
    switch(fontSize)
    {
        case 16:
            libIndex = (unsigned char *)&ascii8X16[0][0];
            libA = 8;
            libB = 16;
            libStep = 16*16>>4;
            libSize = 16*16;
            break;
        case 24:
            libIndex = (unsigned char *)&ascii12X24[0][0];
            libA = 12;
            libB = 24;
            libStep = 24*24>>4;
            libSize = 24*24;
            break;
        case 32:
            libIndex = (unsigned char *)&ascii16X32[0][0];
            libA = 16;
            libB = 32;
            libStep = 32*32>>4;
            libSize = 32*32;
            break;
        default :
            return ;
    //            break;
    }
    if((x+libA) > LCD_W)
        y += libB;
    if((y+libB) > LCD_H)
    {
        y = 0;
        x += libA;
        if(x > LCD_W)
            x = 0;
    }
    for(j=0; j < libStep; j++)
    {
        for(i=0; i < 8; i++)
        {
            if((*(unsigned char *)(libIndex + libStep*(data-32) + j ) & BITM[i]) != 0)
            {
                dispRam[j*16+i*2] = tCols[0];
                dispRam[j*16+i*2+1] = tCols[1];
            }
            else
            {
                dispRam[j*16+i*2] = bCols[0];
                dispRam[j*16+i*2+1] = bCols[1];
            }
        }
    }
    */
    //lcdPositionSet(x,y,x+libA-1,y+libB-1);
    //lcdWritBusXB((unsigned char *)dispRam,libSize);
}

//---------------------------------------------------------------------------------------------------------------o
//    显示单        |      字体       |   左上坐  |   左上坐  |     文字编     |     16Bit前    |     16Bit背    |
//    个汉字        |      高度       |   标点X   |   标点Y   |     码数据     |     景颜色     |     景颜色     |
void drawChineseChar(uint16_t fontSize, uint16_t x, uint16_t y, uint16_t hzCode, uint16_t tColor, uint16_t bColor)
{
    /*
    int8_t j;
    const unsigned char cTBit[]= {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};
    uint16_t i = 0,index;
    uint8_t tCols[2],bCols[2];
    unsigned char * libIndex;
    uint8_t libB;      //libA,
    uint32_t libStep,libSize;
    tCols[0] = (uint8_t)(tColor >>8)& 0xFF;
    tCols[1] = (uint8_t)(tColor & 0xFF);
    bCols[0] = (uint8_t)(bColor >>8)& 0xFF;
    bCols[1] = (uint8_t)(bColor & 0xFF);
    index = findHzIndex(hzCode);
    switch(fontSize)
    {
        case 16:
            libIndex = (unsigned char *)&hz16Data[0][0];
    //            libA = 8;
            libB = 16;
            libStep = 16*16>>3;
            libSize = 16*16*2;
            break;
        case 24:
            libIndex = (unsigned char *)&hz24Data[0][0];
    //            libA = 12;
            libB = 24;
            libStep = 24*24>>3;
            libSize = 24*24*2;
            break;
        case 32:
            libIndex = (unsigned char *)&hz32Data[0][0];
    //            libA = 16;
            libB = 32;
            libStep = 32*32>>3;
            libSize = 32*32*2;
            break;
        default :
            return ;
    //            break;
    }
    for(i = 0; i< libStep; i++)
    {
        for(j = 0 ; j<8; j++)
        {
            if( (*(unsigned char *)(libIndex + libStep*index + i) & cTBit[j]) !=0)
            {
                dispRam[i*16+j*2] = tCols[0];
                dispRam[i*16+j*2+1] = tCols[1];
            }
            else
            {
                dispRam[i*16+j*2] = bCols[0];
                dispRam[i*16+j*2+1] = bCols[1];
            }
        }
    }
    */
//    dispRam2S(x, y, x+libB-1, y+libB-1,libSize);
    //lcdPositionSet(x,y,x+libB-1,y+libB-1);
    //lcdWritBusXB((unsigned char *)dispRam,libSize);
}

//---------------------------------------------------------------------------------------------------o
//  显示     |     字体       |   左上坐  |   左上坐  |   文字编   |     16Bit前    |     16Bit背    |
//  文字     |     高度       |   标点X   |   标点Y   |   码指针   |     景颜色     |     景颜色     |
void drawText(uint8_t fontSize, uint16_t x, uint16_t y, char * data, uint16_t tColor, uint16_t bColor)
{
    uint16_t xt,yt,hzCode;
    uint8_t *tmpData;
    uint8_t libA,libB;
//    uint32_t libStep,libSize;
//    uint8_t tCols[2],bCols[2];
    xt = x;
    yt = y;
    tmpData = (uint8_t *)data;
//    tCols[0] = (uint8_t)(tColor >>8)& 0xFF;
//    tCols[1] = (uint8_t)(tColor & 0xFF);
//    bCols[0] = (uint8_t)(bColor >>8)& 0xFF;
//    bCols[1] = (uint8_t)(bColor & 0xFF);
    switch(fontSize)
    {
        case 16:
//            libIndex = &ascii8X16[0][0];
            libA = 8;
            libB = 16;
//            libStep = 16*16>>4;
//            libSize = 16*16;
            break;
        case 24:
//            libIndex = &ascii12X24[0][0];
            libA = 12;
            libB = 24;
//            libStep = 24*24>>4;
//            libSize = 24*24;
            break;
        case 32:
//            libIndex = &ascii16X32[0][0];
            libA = 16;
            libB = 32;
//            libStep = 32*32>>4;
//            libSize = 32*32;
            break;
        default :
            return ;
//            break;
    }
    while(*tmpData != 0)
    {
        if(*tmpData < 0x80)
        {
#ifdef  __AUTO_RETURN_DISPLAY_
            if((xt + libA ) > LCD_W)
            {
                xt = x;
                yt += libB;
                if(yt > LCD_H)
                    yt = y;
            }
#endif
            drawAsciiChar(fontSize, xt, yt, *tmpData++, tColor, bColor);
            xt += libA;
        }
        else
        {
#ifdef  __AUTO_RETURN_DISPLAY_
            if((xt + libB) > LCD_W)
            {
                xt = x;
                yt += libB;
                if(yt > LCD_H)
                    yt = y;
            }
#endif
            hzCode = ((*tmpData)<<8) + (*(tmpData+1));
            tmpData += 2;
            drawChineseChar(fontSize, xt, yt, hzCode, tColor, bColor);
            xt += libB;
        }
    }
}

//---------------------------------------------------------------------------------------------------o
// 显示有符  |     字体       |   左上坐  |   左上坐  |   8bit     |     16Bit前    |     16Bit背    |
// 号8bit数  |     高度       |   标点X   |   标点Y   |   数据     |     景颜色     |     景颜色     |
void drawInt8(uint8_t fontSize, uint16_t x, uint16_t y,int8_t data,uint16_t tColor, uint16_t bColor)
{
    char str[5]= {"    "};
    uint8_t num;
    if(data < 0)
    {
        str[0]='-';
        num = (-1)*data;
    }
    else
        num = data;
    if((num/100) >0)
    {
        str[1] = '0'+(num/100);
        str[2] = '0'+(num/10%10);
        str[3] = '0'+(num%10);
    }
    else if((num/10) > 0)
    {
        str[1] = '0'+(num/10);
        str[2] = '0'+(num%10);
    }
    else
        str[1] = '0'+num;
    drawText(fontSize, x, y, str,tColor, bColor);
}

//------------------------------------------------------------------------------------------------------o
// 显示无符    |     字体       |   左上坐  |   左上坐  |   16bit     |     16Bit前    |     16Bit背    |
// 号16bit数   |     高度       |   标点X   |   标点Y   |   数据      |     景颜色     |     景颜色     |
void drawUint16(uint8_t fontSize, uint16_t x, uint16_t y,uint16_t data, uint16_t tColor, uint16_t bColor)
{
    char str[7]= {"     "};
    uint16_t num;
    num = data;
    str[5] = 0;
    if((num/10000) >0)
    {
        str[0] = '0'+(num/10000);
        str[1] = '0'+(num/1000%10);
        str[2] = '0'+(num/100%10);
        str[3] = '0'+(num/10%10);
        str[4] = '0'+(num%10);
    }
    else if((num/1000) > 0)
    {
        str[1] = '0'+(num/1000%10);
        str[2] = '0'+(num/100%10);
        str[3] = '0'+(num/10%10);
        str[4] = '0'+(num%10);
    }
    else if((num/100) > 0)
    {
        str[2] = '0'+(num/100%10);
        str[3] = '0'+(num/10%10);
        str[4] = '0'+(num%10);
    }
    else if((num/10) > 0)
    {
        str[3] = '0'+(num/10%10);
        str[4] = '0'+(num%10);
    }
    else
        str[4] = '0'+num;
    drawText(fontSize, x, y, str,tColor, bColor);
}


