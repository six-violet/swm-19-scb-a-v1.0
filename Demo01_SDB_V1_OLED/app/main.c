/*  杭州六堇科技有限公司
 *  2023-03-27
 *  GPIO Demo
 *  By Dawoo.Zheng
 *
 */

#include "main.h"
//#include "drvMcu.h"
#include "bmp.h"

void delay_ms(uint32_t t)
{
    uint16_t i,j;
    for(i=0; i<t; i++)
        for(j=0; j<1400; j++)
            ;
}

int main(void)
{
//    uint8_t i;
    SystemInit();

    delay_ms(10);
    BSPInit();

    OledColorTurn(0);//
    OledDisplayTurn(0);//
    OledRefresh();
    OledShowPicture(0,0,128,8,BMP1);

    while(1==1)
    {
        OledShowPicture(0,0,128,8,BMP1);
        GPIO_SetBit(GPIOC, PIN6);
        delay_ms(1000);

        OledShowPicture(0,0,128,8,BMP2);
        GPIO_ClrBit(GPIOC, PIN6);
        delay_ms(1000);

        OledShowString(0, 2,"1234567890abcdefghij",12);
        OledShowString(0,14,"klmnopqrstuvwxyz!@#$",12);
        OledShowString(0,26,"ABCDEFGHIJKLMNOPQRST",12);
        OledShowString(0,38,"UVWXYZ%^&*()_+-=~`[]",12);
        OledShowString(0,50,"{}:;',><?/|\\\"",12);
        OledRefresh();
        GPIO_SetBit(GPIOC, PIN6);
        delay_ms(1000);

        OledClear();
        OledShowString(0, 0,"0123456789!@#$%^",16);
        OledShowString(0,16,"ABCDEFGHIJKLMNOP",16);
        OledShowString(0,32,"QRSTUVWXYZ_abcde",16);
        OledShowString(0,48,"fghijklmnopqrstu",16);
        OledRefresh();
        GPIO_ClrBit(GPIOC, PIN6);
        delay_ms(1000);

        OledClear();
        OledShowString(0, 0,"0123456789",24);
        OledShowString(0,24,"ABCDEFGHIJ",24);
        OledRefresh();
        GPIO_SetBit(GPIOC, PIN6);
        delay_ms(1000);

        OledShowPicture(0,0,128,8,BMP1);
        OledColorTurn(1);
//        OledDisplayTurn(1);
        GPIO_ClrBit(GPIOC, PIN6);
        delay_ms(1000);
        OledColorTurn(0);
//        OledDisplayTurn(0);
    }
}


